```text
"illuminate/support": "^8.0",
"illuminate/container": "^8.0",
"illuminate/redis": "^8.0",
```

Инфраструктура и запуск - Phalcon

Приложение, автозагрузка зависимостей и т.д. - Laravel

Реализован паттерн Action Domain Responder (ADR). Отличие (упрощение), для всех ответов api, используется один ответчик
