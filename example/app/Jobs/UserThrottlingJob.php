<?php
declare(strict_types=1);

namespace Lg\App\Jobs;

use Decmedia\Kernel\Contracts\Foundation\Application;
use Decmedia\Kernel\Exception\ModelException;
use Decmedia\Kernel\Foundation\Bus\Dispatchable;
use Phalcon\Di\Injectable;
use Lg\App\Models\UserThrottling as ThrottlingLog;

/**
 * Class UserThrottlingJob
 * @package Yt\App\Jobs
 */
final class UserThrottlingJob extends Injectable
{
    use Dispatchable;

    /** @var string $hash */
    private $hash;

    /** @var int|null $userID */
    private $userID;

    public function __construct(string $hash, int $userID = null)
    {
        $this->hash = $hash;
        $this->userID = $userID;
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws ModelException
     */
    public function handle(Application $app)
    {
        if ($app->runningUnitTests()) {
            return;
        }

        $throttlingLog = new ThrottlingLog();
        $result = $throttlingLog
            ->set('userID', $this->userID)
            ->set('hash', $this->hash)
            ->set('attempted', time())
            ->save();

        if (false === $result) {
            throw new ModelException($throttlingLog->getModelMessages());
        }

        $attempts = ThrottlingLog::count([
            'hash = :hash: AND attempted >= :attempted:' . ($this->userID ? " AND userID = :userID:" : ''),
            'bind' => array_merge(
                [
                    'hash' => $this->hash,
                    'attempted' => time() - 3600 * 6
                ],
                $this->userID ? ['userID' => $this->userID] : []
            )
        ]);

        if ($this->userID !== 9) {
            switch ($attempts) {
                case 1:
                case 2:
                    // no delay
                    break;
                case 3:
                case 4:
                    sleep(2);
                    break;
                default:
                    sleep(5);
                    break;
            }
        }
    }
}
