<?php
declare(strict_types=1);

namespace Lg\App\Actions\User\Event;

use DateTimeImmutable;
use Lg\App\Models\User;

/**
 * Class UserLoginEvent
 * @package Yt\Event
 */
trait UserEvent
{
    /** @var DateTimeImmutable  */
    private $occurredOn;

    /** @var User */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->occurredOn = new DateTimeImmutable();
    }

    public function user(): User
    {
        return $this->user;
    }

    public function occurredOn(): DateTimeImmutable
    {
        return $this->occurredOn;
    }
}
