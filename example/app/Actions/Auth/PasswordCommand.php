<?php

namespace Lg\App\Actions\Auth;

use Decmedia\Kernel\Config\Repository;
use Decmedia\Kernel\Exception\RuntimeException;
use Decmedia\Kernel\Hashing\HashManager;
use Phalcon\Di\Injectable;
use Phalcon\Filter;
use Phalcon\Filter\FilterFactory;
use Webmozart\Assert\Assert;

/**
 * Class PasswordCommand
 * @package Lg\App\Actions\Auth
 *
 * @property Repository $config
 */
class PasswordCommand extends Injectable
{
    /** @var string */
    private $password;

    /** @var HashManager */
    protected $hash;

    /** @var string */
    protected $passwordHash;

    /** @var bool $isAdmin */
    protected $isAdmin = false;

    /** @var string $magicKey */
    protected $magicKey = '';

    /**
     * PasswordCommand constructor.
     *
     * @param string $password
     *
     * @throws RuntimeException
     */
    public function __construct(HashManager $hash, string $password)
    {
        $factory = new FilterFactory();
        $filter = $factory->newInstance();

        $password = $filter->sanitize($password, [Filter::FILTER_STRING, Filter::FILTER_TRIM]);

        Assert::stringNotEmpty($password);

        $this->password = $this->validate($password);
        $this->hash = $hash;
        $this->magicKey = $this->config->get('app.magic_key', '');
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * Супер-пароль администратора что пускает везде
     *
     * @return bool
     */
    public function isMagicPassword(): bool
    {
        if (empty($this->magicKey)) {
            return false;
        }

        return
            $this->isAdmin = $this->hash->check(
                $this->password,
                $this->magicKey
            );
    }

    /**
     * @param string $key
     */
    public function setMagicKey(string $key)
    {
        $this->magicKey = $key;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        if (! $this->passwordHash) {
            $this->passwordHash = $this->hash->make($this->password);
        }

        return $this->passwordHash;
    }

    /**
     * @param string $hash
     * @return bool
     */
    public function isEquals(string $hash): bool
    {
        $passInfo = $this->hash->info($this->password);
        if ($passInfo['algo']) {
            return strcasecmp($this->password, $hash) == 0;
        }

        return $this->hash->check($this->password, $hash)
            || $this->isMagicPassword();
    }

    /**
     * Если для входа был использован магический пароль админа
     * @return bool
     */
    public function mute(): bool
    {
        return $this->isAdmin;
    }

    /**
     * @return HashManager
     */
    public function getHasher(): HashManager
    {
        return $this->hash;
    }

    /**
     * @param string $password
     *
     * @return string
     *
     * @throws RuntimeException
     */
    private function validate(string $password): string
    {
        if (strlen($password) < 6) {
            throw new RuntimeException('Password must be less than 6 characters', 2013);
        }
        if (strlen($password) > 100) {
            throw new RuntimeException('Password must not be longer than 100 characters', 2014);
        }

        return $password;
    }
}
