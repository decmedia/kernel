<?php
declare(strict_types=1);

namespace Lg\App\Actions\Auth;

use function Decmedia\Kernel\app;
use function Decmedia\Kernel\tap;

/**
 * Class AuthClient
 * @package Lg\App\Actions\Auth
 */
final class AuthClient extends \Phalcon\Di\Injectable
{
    public function dispatch($command)
    {
        return tap(
            (app(get_class($command)))($command),
            function ($handler) {
                $handler->releaseEvents();
            }
        );
    }
}
