<?php
declare(strict_types=1);

namespace Lg\App\Actions\Auth\Event;

use Decmedia\Kernel\Support\Contracts\Event\ApplicationEvent;
use Lg\App\Actions\User\Event\UserEvent;

/**
 * Class UserLoginEvent
 * @package Yt\Event
 */
class AuthLoginUserEvent implements ApplicationEvent
{
    use UserEvent;
}
