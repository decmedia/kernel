<?php
declare(strict_types=1);

namespace Lg\App\Actions\Auth\Event;

use Decmedia\Kernel\Support\Contracts\Event\ApplicationEvent;
use Lg\App\Actions\User\Event\UserEvent;

/**
 * Class UserLogoutEvent
 * @package Yt\Event
 */
class AuthFailedBlockedUserEvent implements ApplicationEvent
{
    use UserEvent;
}
