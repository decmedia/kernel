<?php
declare(strict_types=1);

namespace Lg\App\Actions\Auth\Event;

use DateTimeImmutable;
use Decmedia\Kernel\Support\Contracts\Event\ApplicationEvent;
use Lg\App\Actions\User\Event\UserEvent;
use Lg\App\Models\User;

/**
 * Class UserLogoutEvent
 * @package Yt\Event
 */
class AuthFailedLoginUserEvent implements ApplicationEvent
{
    use UserEvent;

    /** @var string $clientIp */
    public $clientIp;

    public function __construct(User $user, string $clientIp)
    {
        $this->user = $user;
        $this->clientIp = $clientIp;
        $this->occurredOn = new DateTimeImmutable();
    }
}
