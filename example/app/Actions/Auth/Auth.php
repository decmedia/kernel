<?php

namespace Lg\App\Actions\Auth;

use Decmedia\Kernel\Support\Facades\Facade;

/**
 * @method static AuthClient dispatch($command)
 *
 * @see AuthClient
 */
class Auth extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'auth';
    }
}
