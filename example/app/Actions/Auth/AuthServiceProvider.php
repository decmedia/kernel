<?php
declare(strict_types=1);

namespace Lg\App\Actions\Auth;

use Decmedia\Kernel\Support\ServiceProvider;

use Lg\App\Actions\Auth\Login\LoginCommand;
use Lg\App\Actions\Auth\Login\LoginHandler;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     * @return void
     */
    public function register()
    {
        $this->registerAuthenticator();
        $this->registerHandlers();
    }

    /**
     * Register the authenticator services.
     * @return void
     */
    protected function registerAuthenticator()
    {
        $this->app->singleton('auth', function ($app) {
            // Once the authentication service has actually been requested by the developer
            // we will set a variable in the application indicating such. This helps us
            // know that we need to set any queued cookies in the after event later.
            $app['auth.loaded'] = true;

            return new AuthClient();
        });
    }

    protected function registerHandlers()
    {
        $this->app->singleton(LoginCommand::class, LoginHandler::class);
    }
}
