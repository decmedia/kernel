<?php
declare(strict_types=1);

namespace Lg\App\Actions\Auth\Login;

use Decmedia\Kernel\Support\Facades\Date;
use Illuminate\Contracts\Container\BindingResolutionException;
use Phalcon\Di\Injectable;
use Webmozart\Assert\Assert;
use Decmedia\Kernel\Event\EventTrait;
use Decmedia\Kernel\Exception\Exception;
use Decmedia\Kernel\Exception\ModelException;
use Decmedia\Kernel\Exception\RuntimeException;
use Decmedia\Kernel\Traits\QueryTrait;
use Lg\App\Actions\Auth\Event\AuthFailedBlockedUserEvent;
use Lg\App\Actions\Auth\Event\AuthFailedLoginUserEvent;
use Lg\App\Actions\Auth\Event\AuthLoginUserEvent;
use Lg\App\Jobs\UserThrottlingJob;
use Lg\App\Models\User;
use function Decmedia\Kernel\app;
use function Decmedia\Kernel\event;
use function Decmedia\Kernel\tap;
use function Decmedia\Kernel\getError;

/**
 * Class LogIn
 * @package Yt\Api\Controllers\Auth
 *
 * @property \Decmedia\Kernel\Http\Request  $request
 * @property \Decmedia\Kernel\Contracts\Cache\Store  $cache
 * @property \Decmedia\Kernel\Contracts\Config\Repository $config
 */
class LoginHandler extends Injectable
{
    use QueryTrait;
    use EventTrait;

    /** @var User|null */
    protected $user = null;

    /** @var string $password */
    protected $password;

    /**
     * @param LoginCommand $command
     *
     * @return LoginHandler
     *
     * @throws BindingResolutionException
     * @throws Exception
     * @throws RuntimeException
     */
    public function __invoke(LoginCommand $command)
    {
        return $this->attempt($command);
    }

    /**
     * @param LoginCommand $command
     *
     * @return LoginHandler
     *
     * @throws BindingResolutionException
     * @throws Exception
     * @throws RuntimeException
     */
    public function handle(LoginCommand $command)
    {
        return $this->attempt($command);
    }

    /**
     * @param LoginCommand $command
     *
     * @return LoginHandler
     *
     * @throws BindingResolutionException
     * @throws Exception
     * @throws RuntimeException
     */
    public function attempt(LoginCommand $command)
    {
        $this->user = tap($this->checkPerson($command), function (User $user) use ($command) {
            if (false === $command->getPassword()->mute()) {
                $this->registerEvent(new AuthLoginUserEvent($user));
            }
        });

        return $this;
    }

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->user;
    }

    /**
     * @param LoginCommand $command
     * @return User
     *
     * @throws BindingResolutionException
     * @throws Exception
     * @throws RuntimeException
     */
    private function checkPerson(LoginCommand $command): User
    {
        $parameters = [
            'login' => $command->getUsername()
        ];
        $result = $this->getRecords($this->config, $this->cache, User::class, $parameters);
        if (count($parameters) > 0 && 0 === count($result)) {
            throw new RuntimeException('User not found', 2007);
        }

        $user = $result[0];

        if ($user->enabled === 'no' || $user->block === 'yes') {
            // If the user account is locked, we fire an event so that the user
            // can be notified of any suspicious attempts to access your account from
            // unidentified user. The developer can listen to this event as needed.
            $this->fireBlockedEvent($user);

            throw new RuntimeException('Account is blocked', 2009);
        }

        if (false === $command->getPassword()->isEquals($user->password)) {
            // If the authentication attempt fails we will fire an event so that the user
            // may be notified of any suspicious attempts to access their account from
            // an unrecognized user. A developer may listen to this event as needed.
            $this->fireFailedEvent($user, $this->request->getClientIp());

            UserThrottlingJob::dispatch($this->request->getClientIp(), $user->userID);

            throw new RuntimeException('Password not correct', 2010);
        }

        return $user;
    }

    /**
     * Fire the failed authentication attempt event with the given arguments.
     *
     * @param User $user
     * @param string $clientIp
     * @return void
     * @throws BindingResolutionException
     */
    protected function fireFailedEvent($user, string $clientIp)
    {
        event(new AuthFailedLoginUserEvent($user, $clientIp));
    }

    /**
     * Fire a blocked account event
     *
     * @param $user
     * @throws BindingResolutionException
     */
    protected function fireBlockedEvent($user)
    {
        event(new AuthFailedBlockedUserEvent($user));
    }
}
