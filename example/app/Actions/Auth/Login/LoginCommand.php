<?php
declare(strict_types=1);

namespace Lg\App\Actions\Auth\Login;

use Phalcon\Filter;
use Phalcon\Filter\FilterFactory;
use Webmozart\Assert\Assert;
use Lg\App\Actions\Auth\PasswordCommand;
use function Decmedia\Kernel\app;

class LoginCommand
{
    /** @var string */
    private $username;

    /** @var PasswordCommand */
    private $password;

    /** @var int */
    private $code2fa;

    /**
     * LoginCommand constructor.
     *
     * @param string $username
     * @param string $password
     * @param int $code2fa
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(
        string $username,
        string $password,
        int $code2fa = 0
    ) {
        $factory = new FilterFactory();
        $filter = $factory->newInstance();

        Assert::stringNotEmpty($username);
        Assert::stringNotEmpty($password);
        Assert::numeric($code2fa);

        $this->username = $filter->sanitize($username, [Filter::FILTER_STRING, Filter::FILTER_TRIM]);
        $this->code2fa = $filter->sanitize($code2fa, Filter::FILTER_INT);
        $this->password = app(PasswordCommand::class, ['password' => $password]);
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return PasswordCommand
     */
    public function getPassword(): PasswordCommand
    {
        return $this->password;
    }

    /**
     * @return int
     */
    public function getCode2fa(): int
    {
        return (int)$this->code2fa ?? 0;
    }
}
