<?php

namespace Lg\App\Actions;

/**
 * Class BaseQueryFilter
 * @package Lg\App\Actions
 *
 * $builder = new BaseQueryFilter(User::class);
 * $users = $builder
 *      ->apply($request)
 *      ->orderBy('id desc')
 *      ->getQuery()
 *      ->execute();
 */
final class BaseQueryFilter extends QueryFilter
{

}
