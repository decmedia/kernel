<?php

namespace Lg\App\Console;

use Decmedia\Kernel\Console\Scheduling\Schedule;
use Decmedia\Kernel\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * The application's global providers stack.
     * @var string[]
     */
    protected $infrastructureProvider = [
        \Decmedia\Kernel\Providers\Config\ConfigProvider::class,
        \Decmedia\Kernel\Providers\LoggerProvider::class,
        \Decmedia\Kernel\Providers\Database\DatabaseProvider::class,
        \Decmedia\Kernel\Providers\LoggerDbProvider::class,
        \Decmedia\Kernel\Providers\EventProvider::class,
        \Decmedia\Kernel\Providers\Cache\CacheProvider::class,
        \Decmedia\Kernel\Providers\PipelineProvider::class,
        \Decmedia\Kernel\Providers\ResolverProvider::class,
        \Decmedia\Kernel\Providers\View\ViewSimpleProvider::class,
        \Decmedia\Kernel\Providers\LocaleProvider::class,

        /*
        |--------------------------------------------------------------------------
        | Model Caching
        |--------------------------------------------------------------------------
        |
        | https://docs.phalcon.io/4.0/en/db-models-cache
        |
        */
        // \Decmedia\Kernel\Providers\ModelsCacheProvider::class,
        // \Decmedia\Kernel\Providers\ModelsMetadataProvider::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Decmedia\Kernel\Console\Scheduling\Schedule $schedule
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function schedule(Schedule $schedule)
    {
        /*$productService = new ProductService();
        $productService->updateProductCostByFix();*/
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     * @throws \ReflectionException
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        /*
        |--------------------------------------------------------------------------
        | Console Routes
        |--------------------------------------------------------------------------
        |
        | This file is where you may define all of your Closure based console
        | commands. Each Closure is bound to a command instance allowing a
        | simple approach to interacting with each command's IO methods.
        |
        */

        //...
        // Artisan::command('build {project}', function ($project) {});
    }
}
