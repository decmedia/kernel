<?php
declare(strict_types=1);

namespace Lg\App\Console\Commands;

use Decmedia\Kernel\Console\Command;
use Decmedia\Kernel\Support\Facades\Date;
use Lg\App\Models\UserToken;

/**
 * Class ClearUserToken
 * @package Lg\App\Jobs
 */
final class ClearUserToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:clear-expired-tokens';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleans up expired tokens that were issued to users';

    /**
     * Очистка просроченных токенов авторизации
     */
    public function handle()
    {
        UserToken::find([
            'conditions' => 'date_expires < :current_time:',
            'bind' => [
                'current_time' => Date::now()->getTimestamp()
            ]
        ])->delete();
    }
}
