<?php

namespace Lg\App\Mail;

use Decmedia\Kernel\Bus\Queueable;
use Decmedia\Kernel\Config\Repository;
use Decmedia\Kernel\Contracts\Queue\ShouldQueue;
use Decmedia\Kernel\Notification\Mail\Mailable;

/**
 * Class EmailVerification
 * @package Yt\App\Actions\Mail
 *
 * @property Repository $config
 */
class PasswordRecovery extends Mailable implements ShouldQueue
{
    use Queueable;

    /** @var string $email */
    protected $email;

    /** @var string $code */
    protected $code;

    /**
     * EmailVerification constructor.
     * @param string $email
     * @param $confirmCode
     */
    public function __construct(string $email, $confirmCode)
    {
        $this->email = $email;
        $this->code = $confirmCode;
    }

    /**
     * Build the message.
     *
     * @return $this
     * @throws \Exception
     */
    public function build()
    {
        return $this
            ->view('mail/password_recovery')
            ->with([
                'contact' => $this->email,
                'code' => $this->code
            ])
            ;
    }
}
