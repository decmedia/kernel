<?php
namespace Lg\App\Http\Controllers;

use Decmedia\Kernel\Config\Repository;
use Decmedia\Kernel\Http\JsonResponse;
use Decmedia\Kernel\Http\Request;
use Decmedia\Kernel\Http\Response;
use Phalcon\Filter;

/**
 * Class ExampleController
 * @package Lg\App\Http\Controllers
 *
 * @property Repository $config;
 * @property Request $request;
 * @property Response|JsonResponse $response;
 */
class ExampleController extends \Phalcon\Mvc\Controller
{
    /**
     * @throws \Decmedia\Kernel\Exception\ModelException
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function indexAction()
    {
        $username = $this->request->get('username', Filter::FILTER_STRING);
        $password = $this->request->get('password', Filter::FILTER_TRIM);

        if (!empty($username) && !empty($password)) {
            $this
                ->response
                ->setPayloadSuccess([
                    'field1' => 'val1',
                    'field2' => 'val2'
                ])
            ;
        } else {
            $this
                ->response
                ->setPayloadError(
                    $this->response->getHttpCodeDescription($this->response::BAD_REQUEST),
                    $this->response::BAD_REQUEST
                )
                ->setStatusCode($this->response::BAD_REQUEST)
            ;
        }
    }
}
