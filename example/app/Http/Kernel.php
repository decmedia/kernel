<?php

namespace Lg\App\Http;

use Decmedia\Kernel\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global providers stack.
     * @var string[]
     */
    protected $infrastructureProvider = [
        \Decmedia\Kernel\Providers\RequestProvider::class,
        \Decmedia\Kernel\Providers\Config\ConfigProvider::class,
        \Decmedia\Kernel\Providers\LoggerProvider::class,
        \Decmedia\Kernel\Providers\Database\DatabaseProvider::class,
        \Decmedia\Kernel\Providers\LoggerDbProvider::class,
        \Decmedia\Kernel\Providers\ResponseProvider::class,
        \Decmedia\Kernel\Providers\EventProvider::class,
        \Decmedia\Kernel\Providers\Cache\CacheProvider::class,
        \Decmedia\Kernel\Providers\PipelineProvider::class,
        \Decmedia\Kernel\Providers\ResolverProvider::class,
        \Decmedia\Kernel\Providers\View\ViewSimpleProvider::class,
        \Decmedia\Kernel\Providers\LocaleProvider::class,
        \Lg\App\Providers\HttpRouterProvider::class,

        /*
        |--------------------------------------------------------------------------
        | Model Caching
        |--------------------------------------------------------------------------
        |
        | https://docs.phalcon.io/4.0/en/db-models-cache
        |
        */
        // \Decmedia\Kernel\Providers\ModelsCacheProvider::class,
        // \Decmedia\Kernel\Providers\ModelsMetadataProvider::class,
    ];
}
