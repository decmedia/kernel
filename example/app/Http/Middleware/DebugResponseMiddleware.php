<?php
declare(strict_types=1);

namespace Lg\App\Http\Middleware;

use Decmedia\Kernel\Http\Response;
use Decmedia\Kernel\Support\Facades\Log;
use Decmedia\Kernel\Traits\ResponseTrait;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;

/**
 * Class ResponseMiddleware
 *
 * @property Response $response
 */
class DebugResponseMiddleware implements MiddlewareInterface
{
    use ResponseTrait;

    /**
     * Call me
     *
     * @param Micro $api
     *
     * @return bool
     */
    public function call(Micro $api)
    {
        Log::info(
            print_r($_COOKIE, true)
        );

        return true;
    }
}
