<?php
declare(strict_types=1);

namespace Lg\App\Http\Middleware;

use Decmedia\Kernel\Http\Middleware\AclMiddleware as FoundationAcl;
use Decmedia\Kernel\Traits\QueryTrait;
use Decmedia\Kernel\Traits\ResponseTrait;
use Decmedia\Kernel\Http\Request;
use Decmedia\Kernel\Traits\TokenTrait;
use Decmedia\Kernel\Exception\Exception;
use Lg\App\Constants\Role;
use Lg\App\Models\User;
use Phalcon\Mvc\Micro;

/**
 * Class AclMiddleware
 * @package Lg\App\Http\Middleware
 */
class AclMiddleware extends FoundationAcl
{
    use ResponseTrait;
    use QueryTrait;
    use TokenTrait;

    /** @var \Decmedia\Kernel\Contracts\Cache\Store $cache */
    private $cache;

    /** @var \Decmedia\Kernel\Log\LogManager $logger */
    private $logger;

    /** @var \Decmedia\Kernel\Contracts\Config\Repository $config */
    private $config;

    /** @var \Decmedia\Kernel\Http\Request $request */
    private $request;

    /** @var \Decmedia\Kernel\Http\JsonResponse $response */
    private $response;

    /**
     * @param Micro $api
     * @return bool|void
     */
    public function call(Micro $api)
    {
        $this->logger   = $api->getService('logger');
        $this->cache    = $api->getService('cache');
        $this->config   = $api->getService('config');
        $this->request  = $api->getService('request');
        $this->response = $api->getService('response');

        $this->role     = $this->role();

        if (false === parent::call($api)) {
            $this->halt(
                $api,
                $this->response::FORBIDDEN,
                $this->response->getHttpCodeDescription($this->response::FORBIDDEN)
            );

            return false;
        }

        return true;
    }

    /**
     * Получить роль и вернуть ее строкой, в данном случае по токену JWT
     * @return string
     */
    public function role(): string
    {
        $role = Role::GUEST()->getValue();

        if (true === $this->isValidCheck($this->request)) {
            // This is where we will find if the user exists based on
            // the token passed using Bearer Authentication
            try {
                $token = $this->getToken($this->request->getBearerTokenFromHeader());


                // Проверить токен на валидность (в базе или генераций нового, в зависимости от выбранного способа
                // хранения), и определить нужную роль
                // ---------
                $user = $this->getUserByToken($this->config, $this->cache, $token);
                if (false !== $user) {
                    if (Role::isValid($user->get('role'))) {
                        $role = $user->role;

                        Auth::setUser($user);
                    }
                }
                // ---------

            } catch (Exception $e) {
                $this->response
                    ->setPayloadError($e->getMessage(), $e->getCode())
                ;
            }
        }

        return $role;
    }

    /**
     * Массив контроллеров и методов, для каждой роли
     * @return string[]
     */
    public function allowedComponents(): array
    {
        return [
            Role::GUEST()->getValue() => [
                "implements" => null,
                "methods" => [
                    'Lg\App\Http\Controllers\DevController' => ['indexAction', '...'],
                    DevController::class => ['indexAction', '...'],
                ]
            ],
            Role::USER()->getValue() => [
                "implements" => 'guest',
                "methods" => []
            ],
            Role::ADMIN()->getValue() => [
                "implements" => 'user',
                "methods" => []
            ]
        ];
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    protected function isValidCheck(Request $request): bool
    {
        return (
            true !== $request->isLoginPage() &&
            true !== $request->isEmptyBearerToken()
        );
    }
}
