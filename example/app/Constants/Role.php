<?php
declare(strict_types=1);

namespace Lg\App\Constants;

use MyCLabs\Enum\Enum;

/**
 * Enums вместо использования констант.
 * Полезно там, где значение может быть передано функции или классу
 *
 * function setAction(Role $role)
 * function getAction(): Role
 * Role::isValid('myRole')
 *
 * @method __construct() The constructor checks that the value exist in the enum
 * @method __toString() You can echo $myValue, it will display the enum value (value of the constant)
 * @method getValue() Returns the current value of the enum
 * @method getKey() Returns the key of the current value on Enum
 * @method equals()
 * @method static toArray() method Returns all possible values as an array (constant name in key, constant value in value)
 * @method static keys() Returns the names (keys) of all constants in the Enum class
 * @method static values() Returns instances of the Enum class of all Enum constants (constant name in key, Enum instance in value)
 * @method static isValid() Check if tested value is valid on enum set
 * @method static isValidKey() Check if tested key is valid on enum set
 * @method static search()
 *
 * @see https://github.com/myclabs/php-enum
 *
 * @method static Role GUEST()
 * @method static Role USER()
 * @method static Role ADMIN()
 */
class Role extends Enum
{
    private const GUEST    = 'guest';
    private const USER     = 'user';
    private const ADMIN    = 'admin';
}
