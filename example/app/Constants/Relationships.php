<?php
declare(strict_types=1);

namespace Lg\App\Constants;

class Relationships
{
    const USER_2FA  = 'user_2fa';
    const USER      = 'user';
}
