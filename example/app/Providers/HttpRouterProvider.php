<?php
declare(strict_types=1);

namespace Lg\App\Providers;

use Lg\App\Http\Controllers\ExampleController;
use Lg\App\Http\Controllers\AuthenticatorController;
use Lg\App\Http\Controllers\BalanceController;
use Lg\App\Http\Controllers\CategoryController;
use Lg\App\Http\Controllers\ContractsController;
use Lg\App\Http\Controllers\DataController;
use Lg\App\Http\Controllers\DeliveryController;
use Lg\App\Http\Controllers\DevController;
use Lg\App\Http\Controllers\FileController;
use Lg\App\Http\Controllers\IndexController;
use Lg\App\Http\Controllers\LogsController;
use Lg\App\Http\Controllers\OrderController;
use Lg\App\Http\Controllers\ProductsController;
use Lg\App\Http\Controllers\RenderController;
use Lg\App\Http\Controllers\StorageController;
use Lg\App\Http\Controllers\TestsController;
use Lg\App\Http\Controllers\TransferController;
use Lg\App\Http\Controllers\UserController;
use Lg\App\Http\Controllers\VerificationController;
use Lg\App\Http\Middleware\AclMiddleware;

use Decmedia\Kernel\Http\Middleware\PreventRequestsDuringMaintenanceMiddleware;
use Decmedia\Kernel\Providers\RouterProvider;
use Decmedia\Kernel\Http\Middleware\NotFoundMiddleware;
use Decmedia\Kernel\Http\Middleware\RequestMiddleware;
use Decmedia\Kernel\Http\Middleware\ResponseMiddleware;
use Decmedia\Kernel\Http\Middleware\CORSMiddleware;
use Decmedia\Kernel\Http\Middleware\RateLimitMiddleware;
use Lg\App\Http\Middleware\DebugResponseMiddleware;

/**
 * Class HttpRouterProvider
 * @package Yt\Api\Providers
 */
class HttpRouterProvider extends RouterProvider
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [

        /*
        |--------------------------------------------------------------------------
        | Rate Limit
        |--------------------------------------------------------------------------
        |
        | General purpose rate limiter that can be used to limit the rate at which operations.
        | Restriction by ip address
        |
        */
        // RateLimitMiddleware::class   => 'before',

        PreventRequestsDuringMaintenanceMiddleware::class   => 'before',
        CORSMiddleware::class                               => 'before',
        NotFoundMiddleware::class                           => 'before',
        AclMiddleware::class                                => 'before',
        RequestMiddleware::class                            => 'before',
        ResponseMiddleware::class                           => 'after',
    ];

    /**
     * API Routes
     *
     * Here is where you can register API routes for your application. These
     * routes are loaded by the RouteServiceProvider within a group which
     * is assigned the "api" middleware group.
     *
     * @var string[]
     */
    protected $routes = [
        // Class, Route, Method, Handler, ActionMethod
        [ExampleController::class,         '/api/v1/auth',                     'post', '/',        'indexAction'],
        [ExampleController::class,         '/api/v1/auth/check',               'get',  '/',        'checkAction'],
    ];
}
