<?php
declare(strict_types=1);

namespace Lg\App\Providers;

use Decmedia\Kernel\Queue\Events\JobFailed;
use Decmedia\Kernel\Foundation\Support\Provider\EventServiceProvider as ServiceProvider;
use Decmedia\Kernel\Queue\Events\JobProcessed;
use Decmedia\Kernel\Queue\Events\JobProcessing;
use Decmedia\Kernel\Support\Facades\Queue;

class EventServiceProvider extends ServiceProvider
{
    /*
    |--------------------------------------------------------------------------
    | The event listener mappings for the application
    |--------------------------------------------------------------------------
    | Registered::class => [SendEmailVerificationNotification::class, ...], ...
    |
    */
    protected $listen = [
        // ...
    ];

    /*
    |--------------------------------------------------------------------------
    | Register any events for your application
    |--------------------------------------------------------------------------
    |
    | Встроенные Phalcon события:
    | https://docs.phalcon.io/4.0/ru-ru/events#list-of-events
    |
    | Прикрепите своих слушателей событий что необходимы вышей системе
    | $eventsManager->attach(
    |    'custom:custom',
    |    function (Event $event, $connection) {
    |       // ...handler
    |       // return 'first response';
    |    }
    | );
    |
    | $this->attach(
    |    'custom:custom',
    |    function (Event $event, $connection) {
    |       // ...handler
    |       // return 'first response';
    |    }
    | );
    |
    | $this->fire('custom:custom', null);
    | print_r($eventsManager->getResponses());
    | [
    |   0 => 'first response',
    |   1 => 'second response',
    | ]
    |
    | --------------------------
    |
    | Подписки
    | По имени класса. В слушателе будет вызван метод handle, если он отсутствует, то __invoke
    | По интерфейсу, который будет реализовать класс испускаемого события
    |
    | listen / dispatch - подписки уровня приложения
    | attach / fire - подписки уровня framework (phalcon)
    |
    | $this->listen(CustomEventClass::class, new CustomEventHandler(...$constructorParams));
    | $this->listen(CustomEventClass::class, CustomEventHandler::class);
    | $this->listen(CustomEventInterface::class, CustomEventHandler::class);
    | $this->listen(CustomEventClass::class, Closure);
    |
    | Если имя события совпадает с параметром в closure, то можно короче:
    | Event::listen(function(MyEvent $event) {...})
    |
    | Вызов события
    | Классы команд, реализующие Yt\CommandBus\Command запускаются на выполнение с CommandBus
    | обработчик будет найден самостоятельно
    |
    | Event::dispatch($command)
    | Event::dispatch(new TestCommand(...$constructorParams))
    |
    */
    public function boot()
    {
        parent::boot();

        // Event::
        // ...
    }
}
