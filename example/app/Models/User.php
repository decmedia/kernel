<?php
namespace Lg\App\Models;

use Lg\App\Constants\Relationships;
use Phalcon\Filter;

class User extends AbstractModel
{
    public $userID;
    public $login;
    public $password;
    public $email;
    public $name;

    public function initialize()
    {
        $this->setSource($this->prefix() . 'user');

        $this->hasOne(
            'userID',
            User2fa::class,
            'userID',
            [
                'alias'    => Relationships::USER_2FA,
                'reusable' => true,
            ]
        );

        parent::initialize();
    }

    /**
     * Model filters
     *
     * @return array<string,string>
     */
    public function getModelFilters(): array
    {
        return [
            'userID'        => Filter::FILTER_ABSINT,
            'login'         => Filter::FILTER_STRING,
            'password'      => Filter::FILTER_STRING,
            'email'         => Filter::FILTER_EMAIL,
            'name'          => [Filter::FILTER_STRING, Filter::FILTER_TRIM],
        ];
    }
}
