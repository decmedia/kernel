
# Maintenance Mode
Перевести приложение в режим обслуживания. Все запросы будут прерываться, очередь и задания в cron остановлены. 
```text
php cli down
```
Вы также можете указать retry параметр для down команды, который будет установлен как значение заголовка HTTP:Retry-After
```text
php cli down --retry=60
```
##### Bypassing Maintenance Mode
Обход блокировки в режиме обслуживания
```text
php cli down --secret="1630542a-246b-4b66-afa1-dd72a4c43515"
```
Затем добавьте секрет к параметрам запроса
GET, POST, etc.

##### Pre-Rendering The Maintenance Mode View
Если работы, проводимые в режиме обслуживания, заключаются в обновлении/установке пакетов composer, то страница сообщения о режиме, может не отобразится. Предварительно отрендерить ее можно командой
```text
php cli down --render="errors::503"
```
##### Redirecting Maintenance Mode Requests
При необходимости, можно направить всех гостей на определенный URL
```text
php cli down --redirect=/
```
##### Disabling Maintenance Mode
Чтобы отключить режим обслуживания, используйте upкоманду:

```text
php cli up
```

# Console
```text
# показать список всех команд
php cli list

# для всех команд доступна справка по ее опциям. Для вывода
# пишем команду и передаем флаг -h. Например:
php cli make:controller -h

# help
php cli -h

# Создать контроллер
php cli make:controller -h
php cli make:controller TestController
php cli make:controller Path/TestController
Доступны несколько опций, позволяющих создать разные типы контроллеров:
php cli make:controller -h
```
##### Создание своих команд
Доступно создание собственных команд, что будут выполняться в консоли
```text
php cli make:command TestCommand
```
Команда будет создана по пути /app/Console/Commands и загружена автоматически, при инициализации консольного приложения (будут добавлены все файлы из этой папки, а не только созданные с помощью консольной команды создания).
```text
php cli command:TestCommand
```
Системные команды описаны в ```/src/Foundation/Providers/ArtisanServiceProvider.php```
# Config
```php
$config->get('app.name');
$config->get(['path.key', 'other']);
$config->has('path.key');
$config->set('path.key', 'val');

// вызов оттуда, где DI недоступен
\Decmedia\Kernel\config($key, $default = null);
\Decmedia\Kernel\app('config')->get('app.name');
```

# Container
https://laravel.com/docs/8.x/container

Для доступа к любому сервису контейнера
```php
use \Decmedia\Kernel\Support\Facades\Cache;
use \Decmedia\Kernel\app;

$di->getShared('cache')
Cache::get('key')
app('cache')
$app->bind('cache')
```

# Mail
Поддерживаются почти все функции Laravel Mail
https://laravel.com/docs/8.x/mail

(+) дополнительно:
1. Автоматический рендер markdown шаблонов
2. Автоматический рендер .mjml шаблонов
3. Автоматическая замена внешних css стилей на inline

#### Создать письмо
```text
php cli make:mail NameMailClassByCamelCase
```
По пути ```app/Mail/``` будет создан файл ```NameMailClassByCamelCase.php``` с содержимым
```php
<?php

namespace Lg\App\Mail;

use Decmedia\Kernel\Bus\Queueable;
use Decmedia\Kernel\Contracts\Queue\ShouldQueue;
use Decmedia\Kernel\Notification\Mail\Mailable;
use Decmedia\Kernel\Queue\SerializesModels;

/**
 * Class NameMailClassByCamelCase
 * @package Decmedia\Kernel\App\Mail
 *
 * @link https://laravel.com/docs/8.x/mail
 */
class NameMailClassByCamelCase extends Mailable
{
    // use Queueable;
    // use SerializesModels;

    /**
     * Create a new message instance
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message
     *
     * @return $this
     */
    public function build()
    {
        // $this->with([...payload]);
        // $this->attach('/root/path/to/file');
        // $this->attachFromStorage('file/from/disk/storage');
        // $this->attachFromStorageDisk('s3', 'file/from/disk/storage');
        // $this->attachData($this->pdf, 'name.pdf', ['mime' => 'application/pdf']);
        // $this->disableCssInliner();
        // $this->addCssRaw('.well { color: red }');
        // $this->addCssFile('/resource/view/mail/style.css');

        return $this->view('mail/base');
    }
}
```
##### Использование
Письмо конфигурируется в методе build класса письма
```php
/**
 * Build the message
 *
 * @return $this
*/
public function build()
{
    $this->view('mail/base.phtml'); // Volt render engine
    $this->view('mail/markdown.md'); // Markdown parse
    $this->view('mail/templateName'); // Auto detect render engine

    // Отключить преобразование
    $this->disableCssInliner();

    // добавить стили что должны быть применены к письму
    $this->addCssRaw('.well { color: red }');

    // добавить стили из файла по его абсолютному пути
    $this->addCssFile('/resource/view/mail/style.css');

    // добавить стили из файла, с помощью Filesystem (Storage::)
    $this->addCssFileFromStorage('mail/style.css');

    $this->...
}
```
или после создания
```php
$mail = new NameMailClassByCamelCase($myParam1, $myParam2);
$mail
    ->view('mail/templateName')
    ->disableCssInliner()
; 
```
Если для отправки или конфигурирования письма, будут использоваться функции очереди (```->onConnection(), onQueue, allOnConnection, allOnQueue, delay, through, chain```), то в классе письма необходимо раскомментировать trait
```php
use Queueable;
```  
Если письмо всегда и автоматически должно помещаться в очередь по умолчанию при отправке, то класс письма должен реализовывать интерфейс ```ShouldQueue```
```php
class NameMailClassByCamelCase extends Mailable implements ShouldQueue
{
    ...
```

Возможно использование сразу нескольких провайдеров отправки. Драфвер должен быть сконфигурирован в файле настроек 
```php
Mail::mailer('postmark')
        ->to($request->user())
        ->send(new NameMailClassByCamelCase);
```

##### Отправка
Какой провайдер использовать для отправки, определяется в файле конфигурации ```app/config/mail.php```

Для отладки, если в файле конфигурации определена секция ```to``` то все письма будут отправляться на указанный там адрес, не зависимо от кода вызова
```php
// С использование фасада \Decmedia\Kernel\Support\Facades\Mail
Mail::raw(
    'Text to e-mail', 
    function($message) {
        $message->from('example@example.org', 'Subject');
        $message->to('example@example.com');
    }
);

Mail::
    to('example@example.com')
    ->cc($moreUsers)
    ->bcc($evenMoreUsers)
    ->send(new NameMailClassByCamelCase);

// Принудительное помещение письма в очередь
Mail::
    to('example@example.com')
    ->queue(new NameMailClassByCamelCase);
```
```php
// Задержка отправки письма
// С использованием Carbon
$when = now()->addMinutes(10);

// С использованием Carbon и фасада \Decmedia\Kernel\Support\Facades\Date
$when = Date::now()->addYear();

// Также можно использовать PHP DateTime class и Unix Timestamp
Mail::
    to('example@example.com')
    ->later($when, new NameMailClassByCamelCase);
```
```php
// Указание получателя моделью пользователя
Mail::to(\Phalcon\Mvc\Model $user)->send(
    new NameMailClassByCamelCase($order)
);

// С указанием локали что нужно использовать
Mail::to(\Phalcon\Mvc\Model $user)->locale('es')->send(
    new NameMailClassByCamelCase($order)
);

// Если модель имплементируется от HasLocalePreference и содержит метод preferredLocale(), 
// то локаль будет получена для каждого пользователя автоматически, из базы, этим методом. Указывать ```->locale('es')``` при этом не нужно
Mail::to(\Phalcon\Mvc\Model $user)->send(
    new NameMailClassByCamelCase($order)
);
```
Несколько получателей
```php
Mail::to(['email1@mail.com', 'email2@mail.com'])->send($mail);

// Через коллекции
$users = new \Illuminate\Support\Collection(array(
    ['email' => 'example1@example.com', 'name' => 'Example1'],
    ['email' => 'example2@example.com', 'name' => 'Example2']
));
Mail::to($users)->send($mail);
```

```php
// и отправлять письмо уже так:
$mail = (new NameMailClassByCamelCase())->onQueue('emails');
Mail::to('test@text.com')->queue($mail);

// worker для этой очереди, будет запущен так:
php cli queue:work --queue=emails
```

Вывод рендера письма в браузер (или буфер)
```php
echo (new NameMailClassByCamelCase)->render();
```

#### Рендер
Рендер всех представлений и писем в частности, определяется в ```app/Http/Kernel.php```
```php
protected $infrastructureProvider = [
    // ...
    \Decmedia\Kernel\Providers\View\ViewSimpleProvider::class;
]
```
Рендер на основе ```Phalcon\Mvc\View\Simple```

Поддерживает markdown (.md), volt (.phtml) и MJML (.mjml) шаблоны представлений. 

Путь представления: 
```text
$view->render('путь к шаблону относительно папки resource') 
-> на основе расширения файла выбирается какой рендер для него будет запущен
-> рендер выбранным
-> файл передается volt компилятору phalcon. Замена тегов, компиляция
```

##### MJML
https://mjml.io/

MailJet Markup Language помогает использовать HTML-подобную разметку для создания адаптивных шаблонов электронной почты.

Для создания разметки письма используется online редактор 
(https://mjml.io/try-it-live) 
или приложение (https://appimage.github.io/mjml-app/)

Поддержка почтовыми клиентами: https://mjml.io/compatibility/mj-column#attribute-vertical-align 

При рендере, если шаблон был написан с "сырыми" тегами mjml, он должен быть скомпилирован в HTML сообщения ел. почты.
Что будет обрабатывать конвертацию, определяет ```/backend/config/view.php```. 
Доступно два варианта: запрос к API mjml.io и локальным node.js клиентом (см. package.json в /backend директории)

Для использования локального (на сервере, в том числе), необходимо в корне бека проекта, выполнить
```text
npm install mjml
```
Если mjml клиент-конвертер был установлен в другом месте, то в конфигурации сервисов ```app/config/service.php``` для `mjml` нужно указать `binary_file` - путь к нему

Скомпилированный шаблон будет закеширован, если в `.env` `APP_ENV="production"`, и рендерится каждый раз, для `APP_ENV="development"`

--------
Если использовать представления не планируется вообще, необходимо использовать заглушку:
```php
\Decmedia\Kernel\Providers\View\ViewCapProvider::class;
```





# Files
Удобная обертка для типовых файловых операций.
```php
\Decmedia\Kernel\Support\Facades\File;

File::allFiles(string $directory, bool $hidden = false): \Symfony\Component\Finder\SplFileInfo[];
File::files(string $directory, bool $hidden = false): \Symfony\Component\Finder\SplFileInfo[];
File::directories(string $directory): array;
File::glob(string $pattern, int $flags = 0): array;
File::cleanDirectory(string $directory): bool;
File::copy(string $path, string $target): bool;
File::copyDirectory(string $directory, string $destination, int|null $options = null): bool;
File::delete(string|array $paths): bool;
File::deleteDirectories(string $directory): bool;
File::deleteDirectory(string $directory, bool $preserve = false): bool;
File::exists(string $path): bool;
File::isDirectory(string $directory): bool;
File::isFile(string $file): bool;
File::isReadable(string $path): bool;
File::isWritable(string $path): bool;
File::makeDirectory(string $path, int $mode = 0755, bool $recursive = false, bool $force = false): bool;
File::move(string $path, string $target): bool;
File::moveDirectory(string $from, string $to, bool $overwrite = false): bool;
File::append(string $path, string $data): int;
File::lastModified(string $path): int;
File::prepend(string $path, string $data): int;
File::size(string $path): int;
File::put(string $path, string $contents, bool $lock = false): int|bool;
File::chmod(string $path, int|null $mode = null): mixed;
File::getRequire(string $path): mixed;
File::requireOnce(string $file): mixed;
File::basename(string $path): string;
File::dirname(string $path): string;
File::extension(string $path): string;
File::get(string $path, bool $lock = false): string;
File::hash(string $path): string;
File::name(string $path): string;
File::sharedGet(string $path): string;
File::type(string $path): string;
File::mimeType(string $path): string|false;
File::ensureDirectoryExists(string $path, int $mode = 0755, bool $recursive = true): void;
File::link(string $target, string $link): void;
File::relativeLink(string $target, string $link): void;
File::replace(string $path, string $content): void;
```





# File Storage
Абстрактное файловое хранилище для различных драйверов с единым API.
Работает на базе Flysystem (https://github.com/thephpleague/flysystem)

```php
\Decmedia\Kernel\Support\Facades\Storage;

Storage::put('file.txt', 'Contents');

// С указанием драйвера
// Supported Drivers: "local", "ftp", "sftp", "s3", "memory"
Storage::disk('local')->put('file.txt', 'Contents');
```
```php
$contents = Storage::get('file.jpg');
$exists = Storage::disk('s3')->exists('file.jpg');
$missing = Storage::disk('s3')->missing('file.jpg');
$size = Storage::size('file.jpg');
```
```php
Storage::copy('old/file.jpg', 'new/file.jpg');
Storage::move('old/file.jpg', 'new/file.jpg');
```
```php
Storage::delete('file.jpg');
Storage::delete(['file.jpg', 'file2.jpg']);
Storage::disk('s3')->delete('folder_path/file_name.jpg');
```
```php
$files = Storage::files($directory);
$files = Storage::allFiles($directory);
```
```php
$directories = Storage::directories($directory);
// Recursive
$directories = Storage::allDirectories($directory);
```






# Date и время
https://carbon.nesbot.com/docs/





# Translate
https://docs.phalcon.io/4.0/ru-ru/translate

Переводы в виде массивов берутся из папки:
```
app/storage/message/ru.php
```

Обслуживает все ```LocaleProvider.php```. Доступен в DI как ```locale```

```
<?php echo $locale->_('hi-name', ['name' => 'Mike']); ?>
<p>{{ locale._('hi-name', ['name' => 'Mike']) }}</p>
```
```php
$locale->_('hi-name', ['name' => 'Mike']);
```

##### Использование
```php
// app/storage/message/en.php
$messages = [
    'hi'      => 'Hello',
    'bye'     => 'Good Bye',
    'hi-name' => 'Hello %name%',
    'song'    => 'This song is %song%',
];

$this->locale->_('hi-name', ['name' => 'Mike']);
```
```
<p>{{ locale._('hi-name', ['name' => 'Mike']) }}</p>
```




# Logging
Использование с фасадом
```php
Log::emergency($message);
Log::alert($message);
Log::critical($message);
Log::error($message);
Log::warning($message);
Log::notice($message);
Log::info($message);
Log::debug($message);
```
Передача контекста (будет помещен в лог) для полноты отладочных данных
```php
Log::info('User failed to login.', ['id' => $user->id]);
```
Запись в специфичный (отличный от того что используется по умолчанию) канал
```php
Log::channel('slack')->info('Something happened!');
Log::stack(['single', 'slack'])->info('Something happened!');
```
Как настроить свое форматирование и каналы, можно прочитать тут: https://laravel.com/docs/8.x/logging




# Cache
Одновременно можно использовать один провайдер. Определяется в конфигурации `config/cache.php`
```php
use \Decmedia\Kernel\Support\Facades\Cache;
use \Decmedia\Kernel\app;

Cache::get('key');
Cache::get('key', 'default');
Cache::put('bar', 'baz', 600); // 10 Minutes
Cache::put('key', 'value', now()->addMinutes(10));

Cache::putMany(array $values, $ttl = null)
Cache::many(array $keys)
```
```php
if (Cache::has('key')) {
    //
}
```
```php
Cache::increment('key');
Cache::increment('key', $amount);
Cache::decrement('key');
Cache::decrement('key', $amount);
```
```php
// Storing Items Forever
Cache::forever('key', 'value');
```
```php
// Removing Items From The Cache
Cache::forget('key');
Cache::put('key', 'value', 0);
Cache::put('key', 'value', -5);
Cache::flush();
```

##### Cache Helper
```php
use \Decmedia\Kernel\cache;

$value = cache('key');

cache(['key' => 'value'], $seconds);

cache(['key' => 'value'], now()->addMinutes(10));
```





# Facades
Фасады предоставляют «статический» интерфейс для классов, 
доступных в сервисном контейнере приложения, 
обеспечивая преимущество краткого выразительного синтаксиса при сохранении
большей тестируемости и гибкости, чем традиционные статические методы.

##### Применение
```php
use \Decmedia\Kernel\Support\Facades\Log;

Log::alert('Payload info...');
```

##### Создать свой фасад
В любом месте проекта (как правило app/Actions/...) необходимо создать класс фасада
```php
<?php

namespace Lg\App\Actions\MyCustomService;

use Decmedia\Kernel\Support\Facades\Facade;

/**
 * @method static void dispatch($command)
 */
class Auth extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'serviceName';
    }
}
```
В методе `getFacadeAccessor` которого, указать имя службы, что было использовано при добавлении сервиса/класса в Container

Чтобы IDE значала с чем можно работать, в блок документации фасада, добавьте описания всех методов что будут через него доступны с тегом `@method`
 
Сервисы рекомендуется объявлять в виде отдельных классов, как например `/example/app/Auth/AuthServiceProvider.php`
Для того чтобы зарегистрировать сервис в приложении, его необходимо указать в секции `providers` конфигурации `config/app.php`  

Простые сервисы, например из одной функции, можно указать в специально для этого существующий `app/Providers/AppServiceProvider.php` в метод `register`

Если добавить алиас созданного фасада (как и любого другого класса) в секцию `aliases` конфигурации `config/app.php`, то он будет доступен по заданному в корне пространства имен. Например
```php
'aliases' => [
    'SmallName' => Lg\App\Actions\ServiceName::class,
],
```
то его можно будет использовать в любом месте кода:
```php
\SmallName::myMethod();
```



# Pipeline
Для последовательного выполнения команд. Данные передаются 
от одного обработчика, к другому, последовательно. 

Объявленные в обработчиках классы, будут переданы Container для поиска их там.
Если использовать Container нет необходимости, то вместо реального контейнера
приложения. можно передать пустой

Объекты передаются по ссылке, поэтому, если вы передадите ее, нет необходимости сохранять результат в переменной. Это также верно для Eloquent Models, Collections и всего, что есть в экземплярах.

В каждом обработчике, если он объявлен как класс, будет вызываться метод handle.
Изменить имя, можно указав ->via('имя метода'). Вы должны гарантировать его наличие во всех обработчиках 

Пример объявления трубы
```php
$pipeline = new Pipeline(new Container);
// or
$pipeline = new Pipeline($this->container);
```

Пример передачи обработчиков массивом и переопределения имени вызываемого метода на handle 
```php
$pipes = [
    StrToLowerAction::class,
    SetUserNameAction::class
];
$result = $pipeline
    ->send($data)
    ->through($pipes)
    ->via('handle') // Здесь может быть любое название метода, вы должны гарантировать его наличие во всех обработчиках
    ->then(function ($changedData) {
        return $changedData;
    });
```

Пример на анонимной функции
```php
$result = app(Pipeline::class)
    ->send(’this should be correctly formatted’)
    ->through(
        function ($passable, $next) {
          return $next(ucfirst($passable));
        },
        AddPunctuation::class,
        new RemoveDoubleSpacing(),
        InvokableClassToRemoveDuplicatedWords::class
     );

```

Пример обаботки статьи перед сохранением 
```php
$article->body = $pipeline->send($request->body)
    ->through([
        Paragraphize::class,
        RemoveDuplicates::class,
        AddPunctutation::class,
        EncodeToHtml::class,
    ])->thenReturn();
                    
$article->save();


$bonusValue = $pipeline
    ->send($user)
    ->through([
        CheckForRightReceive::class,
        CheckCvalification::class,
        LeaderLevelRule::class,
        CalcTeamBunus::class,
        CalcLeaderTeamBunus::class,
        // ...
        // и так 
        new RemoveDoubleBonus(),
        // и замыкания
        function ($passable, $next) {
          return $next(ucfirst($passable));
        },
    ])

    // очередь трубы ленивая, и запустится когда где-то внизу, 
    // может даже в другом методе, не вызовем 
    // then (для передачи даннвх дальше) или thenReturn (для return)
    ->thenReturn()
;
                    
$article->save();
```
Пример построения запроса с пользовательскими фильтрами
```php
// вместо этого:
$builder = Product::query();

    if ($request->has('foo')) {
        $builder->where('foo', $request->foo);
    }

    if ($request->has('bar')) {
        $builder->where('bar', '!=', $request->bar);
    }

    if ($request->has('baz')) {
        $builder->whereNotIn('baz', $request->bar);
    }

    return $builder->get();

// это
$pipes = [
    FilterFoo::class,
    FilterBar::class,
    FilterBaz::class,
];

$builder = Product::query();

$results = app(Pipeline::class)
    ->send($builder)
    ->through($pipes)
    ->then(
        fn ($builder) => $builder->get()
    );



class FilterFoo implements Pipe
{
    public function handle($builder, Closure $next)
    {
        if (request()->has('foo')) {
            $builder->where('foo', request()->foo);
        }
        
        return  $next($builder);
    }
}
```

# Queue
Создать класс задачи:
```text
php cli make:job NameCustomJob
```
Класс задачи, будет создан по пути /backend/app/Jobs/NameCustomJob.php
Если класс реализует интерфейс ShouldQueue (```Class NameCustomJob implements ShouldQueue```), то задача может быть помещена в очередь. 
Запуск задачи:
```php
# Если задача реализует интерфейс ShouldQueue, то она будет помещена в очередь. 
# На процесс выполнения текущего скрипта, влияния не оказывает.
# Если интерфейс не реализует, то задача будет выполнена немедленно в текущем процессе.
dispatch(new NameCustomJob());

# Запуск задачи любого типа немедленно: 
dispatch_now(new NameCustomJob());

# Вместо отправки класса задания в очередь вы также можете отправить Closure. 
# Это подходит для быстрых и простых задач, которые необходимо выполнять вне текущего цикла запроса. 
# При отправке замыканий в очередь содержимое кода замыкания подписывается криптографически, поэтому его нельзя изменить при передаче 
dispatch(function () {
     sleep(3);
     echo '!!!!!!!!!!!!';
});

# Добавление задачи в очередь, независимо от ее типа 
Queue::push(new NameCustomJob());
Queue::pushOn('emails', new InvoiceEmail());

# Помещение задания в определенную очередь (high, low). Имя и кол-во, может быть любым. В последствии при запуске рабочего, мы определим приоритет
dispatch((new NameCustomJob)->onQueue('high'));

# Аргументы, переданные dispatch методу, будут переданы конструктору задания:
NameCustomJob::dispatch($payload);
(new NameCustomJob(...$params))->dispatch();
Bus::dispatch(new NameCustomJob());

# Добавить задание в очередь, если условие выполнено и нет
NameCustomJob::dispatchIf($boolean === true, ...$params);
NameCustomJob::dispatchUnless($boolean === false, ...$params);

# Отложенная отправка
# У службы очередей Amazon SQS максимальное время задержки составляет 15 минут.
NameCustomJob::dispatch(...$params)->delay(now()->addMinutes(1));
# Временем в проекте управляет Carbon
# @see https://carbon.nesbot.com/docs/
# @see https://github.com/briannesbitt/Carbon/blob/master/src/Carbon/Factory.php

Queue::later(60, new InvoiceEmail());
Queue::laterOn('emails', 60, new InvoiceEmail(...$params));

# Добавить сразу несколько заданий в очередь
Queue::bulk([
    new InvoiceEmail(),
    new NameCustomJob(),
    // ...
]);
# Добавить сразу несколько заданий в конкретную очередь
Queue::bulk([
    new InvoiceEmail(),
    new NameCustomJob(),
    // ...
], $data ?? null, 'emails');

# Если в класс задачи добавить метод shouldQueue, то он будет вызываться каждый раз при добавлении этой задачи в очередь.
# На основе его ответа, задача либо будет принята. либо отклонена
public function shouldQueue($event)
{
    return $bool;
}
```

##### Пакетирование заданий
Функция пакетной обработки заданий позволяет выполнять пакет заданий, а затем выполнять некоторые действия, когда пакет заданий завершился.
Для того чтобы класс задания сделать исполняемым в пакете, необходимо добавить в него трейты 
```php
use Yt\Bus\Batchable;
use Yt\Bus\Queueable;
use Yt\Contracts\Queue\ShouldQueue;
use Yt\Foundation\Bus\Dispatchable;
use Yt\Queue\InteractsWithQueue;
use Yt\Queue\SerializesModels;

class NameCustomJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->batch()->cancelled()) {
            // Detected cancelled batch...

            return;
        }

        // Batched job executing...
    }
}
```
Чтобы выполнить партию работ, следует использовать batch метод Bus фасада.
Все задания из пакета, будут добавлены в очередь и выполнены параллельно. Если какое-то задание из пакета завершится с ошибкой, оно будет помещено в таблицу заданий что закончились с ошибками (определяется в config/queue.php) и может быть вызвано вновь  
```php
use Yt\Bus\Batch;
use Yt\Support\Facades\Bus;
use Throwable;

$batch = Bus::batch([
    new ProcessPodcast(Podcast::find(1)),
    new ProcessPodcast(Podcast::find(2)),
    new ProcessPodcast(Podcast::find(3)),
    new ProcessPodcast(Podcast::find(4)),
    new ProcessPodcast(Podcast::find(5)),
])->then(function (Batch $batch) {
    // All jobs completed successfully...
})->catch(function (Batch $batch, Throwable $e) {
    // First batch job failure detected...
})->finally(function (Batch $batch) {
    // The batch has finished executing...
})
->dispatch();

# Задать имя пакета (лучше задавать)
->name('Import Contacts')

# Задать конкретную очередь или/и драйвер
->onConnection('redis')->onQueue('podcasts')

return $batch->id;
```
Иногда может быть полезно добавить дополнительные задания в пакет из пакетного задания. Этот шаблон может быть полезен, когда вам нужно выполнить пакетную обработку тысяч заданий, выполнение которых может занять слишком много времени во время веб-запроса. Таким образом, вместо этого вы можете захотеть отправить начальный пакет заданий «загрузчик», которые насыщают пакет дополнительными заданиями:
```php
$batch = Bus::batch([
    new LoadImportBatch,
    new LoadImportBatch,
    new LoadImportBatch,
])->then(function (Batch $batch) {
    // All jobs completed successfully...
})->name('Import Contacts')->dispatch();
```
```php
/**
 * Execute the job.
 *
 * @return void
 */
public function handle()
{
    if ($this->batch()->cancelled()) {
        return;
    }

    $this->batch()->add(new ProcessPodcast);
    // ...
    $this->batch()->add(\Illuminate\Support\Collection::times(1000, function () {
        return new ImportContacts;
    }));
}
```
Вы можете добавлять задания в пакет только из задания, которое принадлежит к тому же пакету.

Получение информации о пакете заданий
```php
$batch = Bus::findBatch('91cf82a6-baac-4d55-90b4-53aabc90d63d');
// The UUID of the batch...
$batch->id;

// The name of the batch (if applicable)...
$batch->name;

// The number of jobs assigned to the batch...
$batch->totalJobs;

// The number of jobs that have not been processed by the queue...
$batch->pendingJobs;

// The number of jobs that have failed...
$batch->failedJobs;

// The number of jobs that have been processed thus far...
$batch->processedJobs();

// The completion percentage of the batch (0-100)...
$batch->progress();

// Indicates if the batch has finished executing...
$batch->finished();

// Cancel the execution of the batch...
$batch->cancel();

// Indicates if the batch has been cancelled...
$batch->cancelled();
```
```php
// Отмена пакета
$batch->cancel();
```
Когда задание в пакете терпит неудачу, весь пакет автоматически помечается как «отмененный». При желании вы можете отключить это поведение, чтобы при сбое задания пакет не отмечался автоматически как отмененный. Это может быть выполнено путем вызова ```allowFailures``` метода при отправке пакета:
```php
$batch = Bus::batch([
    // ...
])->then(function (Batch $batch) {
    // All jobs completed successfully...
})->allowFailures()->dispatch();
```
Повторная попытка выполнения неудачных пакетных заданий
```text
php cli queue:retry-batch 32dbc76c-4f82-4749-b610-a639fe0099b5
```

##### Цепочка заданий 
Позволяет указать список заданий в очереди, которые должны выполняться последовательно после успешного выполнения основного задания. Если одно задание в последовательности выйдет из строя, остальные задания не будут запущены.
```php
Bus::chain([
    new ProcessPodcast,
    new OptimizePodcast,
    function () use ($logger) {
        Podcast::update(...);
        $logger->info('...');
        app('logger')->info('...');
    },
])->dispatch();

# Обработка ошибки выполнения цепочки заданий
->catch(function (Throwable $e) {
     // A job within the chain has failed...
})

# Помещение заданий из цепочки в конкретную очередь или/и драйвер
->onConnection('redis')->onQueue('podcasts')
```

##### Управление очередью
```php
# Просмотр кол-ва заданий в очереди по умолчанию
Queue::size();

# Просмотр кол-ва заданий в очереди с переданным именем
Queue::size('queueName');
```

Запуск воркера на обработку очереди по умолчанию
```text
php cli queue:work
```
Запуск воркера на обработку очереди с заданным драйвером:
(Конфигурация драйверов описывается в config/queue.php)
Возможна одновременная работа сразу нескольких драйверов
```text
php cli queue:work redis
```
Запуск воркера на обработку очереди с заданным именем:
```text
php cli queue:work --queue=emails
```
Поручить работнику обрабатывать только одно задание из очереди:
```text
php cli queue:work --once
```
Эта опция может использоваться, чтобы проинструктировать работника обработать заданное количество заданий и затем выйти. Эта опция может быть полезна в сочетании с Supervisor, чтобы ваши рабочие автоматически перезапускались после обработки заданного количества заданий
```text
php cli queue:work --max-jobs=1000
```
Эта опция может использоваться, чтобы дать рабочему указание обработать все задания и затем корректно завершить работу
```text
php cli queue:work --stop-when-empty
```
Эта опция может использоваться, чтобы проинструктировать работника обрабатывать задания в течение заданного количества секунд, а затем выйти. Эта опция может быть полезна в сочетании с Supervisor, чтобы ваши рабочие автоматически перезапускались после обработки заданий в течение определенного периода времени
```text
php cli queue:work --max-time=3600
```
Выполнять задачи в очереди по указанному приоритету
```text
php cli queue:work --queue=high,default
```
Поскольку обработчики очереди - это долговечные процессы, они не будут принимать изменения в вашем коде без перезапуска. 
Эта команда проинструктирует всех работников очереди аккуратно «умереть» после завершения обработки своего текущего задания, чтобы существующие задания не были потеряны. Поскольку работники очереди умрут при выполнении команды, вы должны запустить диспетчер процессов, например Supervisor, для автоматического перезапуска.
Команду удобно использовать совместно CI
```text
php cli queue:restart
```
Иногда процесс дочерней очереди может "зависнуть" по разным причинам. Опция удаляет замороженные процессы, превысившие , что указанный срок:

Эначение ```--timeout``` должно быть на несколько секунд меньше, чем установленное ```retry_after``` в файле конфигурации очереди, в секции соответствующего драйвера. (например: ```config('queue.connections.beanstalkd.retry_after')```

Это гарантирует, что рабочий, обрабатывающий заданное задание, всегда будет убит перед повторной попыткой задания. Если ваш ```--timeout``` параметр длиннее ```retry_after``` значения конфигурации, ваши задания могут быть обработаны дважды.
```text
php cli queue:work --timeout=60
```
Когда задания доступны в очереди, работник будет продолжать обрабатывать задания без задержки между ними. Однако этот sleep параметр определяет, как долго (в секундах) рабочий будет «спать», если новых рабочих мест нет. Во время сна рабочий не будет обрабатывать никаких новых заданий - задания будут обработаны после того, как воркер снова проснется.
Default value: 3 sec
```text
php cli queue:work --sleep=3
```

##### Работа с неудавшейся работой
Если задание завершится с ошибкой, то оно может быть автоматически перезапущено, указанное кол-во раз:
```text
php cli queue:work --tries=3
```
Кроме того, вы можете указать, сколько секунд надо ждать перед повторной попыткой выполнения задания, которое не удалось, с помощью этой --backoff опции. По умолчанию задание немедленно повторяется:
```text
php cli queue:work --tries=3 --backoff=3
```

Если вы хотите настроить задержку повторной попытки неудачного задания для каждого задания, вы можете сделать это, определив backoff свойство в классе заданий в очереди:
```php
/**
 * The number of seconds to wait before retrying the job.
 *
 * @var int
 */
public $backoff = 3;

# или, если требуются вычисления, так:
/**
* Calculate the number of seconds to wait before retrying the job.
*
* @return int
*/
public function backoff()
{
    return 3;
}

# или, вы можете настроить «экспоненциальную» отсрочку, вернув из backoff
# метода массив значений отсрочки. В этом примере задержка повтора будет составлять 1 секунду для первой попытки, 
# 5 секунд для второй попытки и 10 секунд для третьей попытки:
/**
* Calculate the number of seconds to wait before retrying the job.
*
* @return array
*/
public function backoff()
{
  return [1, 5, 10];
}
```

Для обработки ошибки в классе задания, можно определить в нем метод failed. Например, это может быть полезно для выполнения очистки ресурсов и т.п. 
Это идеальное место для отправки оповещений пользователям или отмены любых действий, выполненных заданием. Throwable Исключение, которое вызвало задание на провал будет передана failed метода:
```php
/**
 * Handle a job failure.
 *
 * @param  \Throwable  $exception
 * @return void
 */
public function failed(Throwable $exception)
{
    // Send user notification of failure, etc...
}
```

События неудачных заданий

Все задания испускают события, до и после выполнения, а также в случае ошибки. Прослушивать их удобно в файле 
```backend/app/Api/Providers/EventServiceProvider.php```

```php
public function boot()
    {
        parent::boot();

        // Event::
        // ...


        Queue::failing(function (JobFailed $event) {
            // $event->connectionName
            // $event->job
            // $event->exception
        });

        Queue::before(function (JobProcessing $event) {
            // $event->connectionName
            // $event->job
            // $event->job->payload()
        });

        Queue::after(function (JobProcessed $event) {
            // $event->connectionName
            // $event->job
            // $event->job->payload()
        });
    }
```

##### Работа с заданиями что закончились с ошибкой
Чтобы просмотреть все неудачные задания, которые были вставлены в failed_jobs таблицу базы данных, вы можете использовать команду
```text
php cli queue:failed
```
Повторить неудачное задание с известным идентификатором (```id``` из таблицы команды ```queue:failed```)
```text
php cli queue:retry 5
```
При необходимости вы можете передать команде несколько идентификаторов или диапазон идентификаторов (при использовании числовых идентификаторов), или указать выполнить все
```text
php cli queue:retry 5 6 7 8 9 10
php cli queue:retry --range=5-10
php cli queue:retry all
```
Для удаления неудавшегося задания из таблицы базы данных, укажите его номер. Или удалите все
```text
php cli queue:forget 5
php cli queue:flush
```
Очистить очередь
```text
php cli queue:flush
```

# Task Scheduling
Добавление плановых задач не в крон, а в код проекта. Реализованы все возможности описанные тут:
https://laravel.com/docs/8.x/scheduling

На сервере, добавить в крон
```text
* * * * * cd /path-to-your-project && php cli schedule:run >> /dev/null 2>&1
```
Запуск для локальной отладки

This command will run in the foreground and invoke the scheduler every minute until you exit the command:
```text
php cli schedule:work
```

Задачи писать в ```backend/app/Console/Kernel.php```

# OAuth 1, 2
Создать файл конфигурации сервисов, поддержка которых планируется
```config/services.php```
```php
return [
    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect' => env('GOOGLE_REDIRECT_URI')
    ],
    // ...
];
```
```php
use \Decmedia\Kernel\Providers\OAuth\Facades\Socialite;

# Получить ссылку перехода для авторизации приложения
Socialite::driver('google')->redirect();

# Получить данные пользователя на обратном редиректе
Socialite::driver('google')->user();

# Задать адрес для редиректа
->redirectUrl('https://example.com')

# Не использовать сессию, как хранилище
->stateless()
```

# Response
Обработчика ответа два: json api и text/html

Нужный выбирается автоматически, на основе заголовка запроса ```X-Requested-With: XMLHttpRequest```   

Ответы json api имеют след. структуру:
```php
$this->response

// Произошла ошибка
{
  "jsonapi": {
    "version": "1.0"
  },
  "errors": [
    "404 (Not Found)"
  ],
  "code": 1002,
  "meta": {
    "timestamp": "2020-11-14T09:38:37+00:00",
    "hash": "45033b7d2b0d7717c93616985a73ea353dc0a350"
  }
}
// errors - текстовое сообщение о случившемся
// code - код ошибки, для использования на фронте 
```

```php
// Успешный ответ
{
  "jsonapi": {
    "version": "1.0"
  },
  "data": [
    "key": "value"
  ],
  "meta": {
    "timestamp": "2020-11-14T09:38:37+00:00",
    "hash": "45033b7d2b0d7717c93616985a73ea353dc0a350"
  }
}
```

Принудительно вернуть шаблон html из контроллера
```php
// с продолжением работы 
echo $this->view->render('path/to/template');

// или (с продолжением)
$this->getDI()->set('response', new \Decmedia\Kernel\Http\Response());
return $this->view->render('path/to/template');

// остановить адскую машину
$this
->response
->setContent($this->view->render('path/to/template'))
->setStatusCode(200)
->send()
;
exit;
```

# Errors
Ошибки, вызванные переданными входными данными, что должны быть возвращены пользователю
- определяются в ```config/error.php```
```php
throw new Decmedia\Kernel\RuntimeException('Message for developer', 2002);

// Сообщение (расшифровка) ошибки, для возврата на frontend
$this->config->get('errors.2002')
config('errors')[2002]
```


# Url Signer

```php
$signer = new \Decmedia\Kernel\Http\UrlSigner(
    $this->config->get('app.key'),
    $this->config->get('app.url') . "/api/*?xyz=true"
);

$signer->buildUrl([
    'controller' => MyCustomController::class,
    'action' => 'myCustomAction',
    'params' => [
         ...
    ]
]);
```
Маршрут будет перехвачен NotFoundMiddleware и передан указанному в параметрах контроллеру и методу
