<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Errors
    |--------------------------------------------------------------------------
    |
    | RuntimeException
    | Ошибки приложения, возникающие на основе переданных параметров.
    | Информация о них потребуется представлению, для информирования frontend
    |
    */

    2002 => 'User not found',
    2003 => 'User blocked',
    2004 => 'Enter 2fa code',
    2005 => '2fa code not correct',
    2011 => 'User [login] exist',
    2012 => 'User [contact] exist',
    2013 => 'Password must be less than 6 characters',
    2014 => 'Password must not be longer than 100 characters',
    2015 => 'Fin Password not correct',
    2016 => 'Invalid link provided, please request a link again.',
];
