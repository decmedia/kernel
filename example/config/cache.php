<?php

use Illuminate\Support\Str;
use function Decmedia\Kernel\env;
use function Decmedia\Kernel\app_path;
use function Decmedia\Kernel\resource_path;
use function Decmedia\Kernel\storage_path;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Cache Store
    |--------------------------------------------------------------------------
    |
    | This option controls the default cache connection that gets used while
    | using this caching library. This connection is used when another is
    | not explicitly specified when executing a given caching function.
    | See: https://docs.phalcon.io/4.0/en/cache
    |
    | Supported: "apcu", "stream", "libmemcached", "redis", "memory"
    |
    */

    'default' => env('CACHE_DRIVER', 'stream'),

    'lifetime' => env('CACHE_LIFETIME', 86400),

    /*
    |--------------------------------------------------------------------------
    | Providers Options
    |--------------------------------------------------------------------------
    |
    | Create a new instance of the adapter
    | Example:
    | 'servers' => [
    |     [
    |         'host' => 'localhost',
    |         'port' => 11211,
    |         'weight' => 1,
    |     ]
    | ],
    | 'host' => '127.0.0.1',
    | 'port' => 6379,
    | 'index' => 0,
    | 'persistent' => false,
    | 'auth' => '',
    | 'socket' => '',
    | 'defaultSerializer' => 'Php',
    | 'lifetime' => 3600,
    | 'serializer' => null,
    | 'prefix' => 'phalcon',
    | 'storageDir' => ''
    |
    */

    'options' => [

        'libmemcached' => [
            'servers'  => [
                0 => [
                    'host'   => env('DATA_API_MEMCACHED_HOST', '127.0.0.1'),
                    'port'   => env('DATA_API_MEMCACHED_PORT', 11211),
                    'weight' => env('DATA_API_MEMCACHED_WEIGHT', 100),
                ],
            ],
            'client'   => [
                \Memcached::OPT_PREFIX_KEY => 'yt-api-',

                # default options
                # \Memcached::OPT_CONNECT_TIMEOUT => 10,
                # \Memcached::OPT_DISTRIBUTION => \Memcached::DISTRIBUTION_CONSISTENT,
                # \Memcached::OPT_SERVER_FAILURE_LIMIT => 2,
                # \Memcached::OPT_REMOVE_FAILED_SERVERS => true,
                # \Memcached::OPT_RETRY_TIMEOUT => 1,
            ],

            # default options
            # 'defaultSerializer'   => 'Php',
            # 'lifetime'            => 3600,
            # 'serializer'          => null,
            # 'prefix'              => 'ph-memc-',
            # 'persistentId'        => 'ph-mcid-',
            # 'saslAuthData'        => [
            #   'user'  => '',
            #   'pass'  => ''
            # ]
        ],

        'memory' => [
            # 'defaultSerializer'   => 'Php',
            # 'lifetime'            => 3600,
            # 'serializer'          => null,
            # 'prefix'              => 'ph-memo-',
        ],

        'redis' => [
            'host'                  => env('DATA_API_REDIS_HOST', '127.0.0.1'),
            'port'                  => env('DATA_API_REDIS_PORT', 6379),
            'index'                 => 0,
            'persistent'            => false,

            # default options
            # 'defaultSerializer'   => 'Php',
            # 'lifetime'            => 3600,
            # 'serializer'          => null,
            # 'prefix'              => 'ph-reds-',
            # 'auth'                => ,
            # 'socket'              => ,
        ],

        'stream' => [
            'storageDir'            => env('CACHE_STORAGE_DIR', storage_path('cache')),

            # default options
            'defaultSerializer'     => 'Php',
            'lifetime'              => 3600,
            # 'serializer'          => new Phalcon\Storage\Serializer\Json(),
            # 'prefix'              => 'phstrm-',
        ],

        'apcu' => [
            'defaultSerializer'     => 'Php',
            'lifetime'              => 3600,
            'serializer'            => null,
            'prefix'                => 'ph-apcu-',
        ],
    ],

    'metadata' => [
        'dev'  => [
            'adapter' => 'Memory',
            'options' => [],
        ],
        'prod' => [
            'adapter' => 'Files',
            'options' => [
                'metaDataDir' => app_path('storage/cache/metadata/'),
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Cache Key Prefix
    |--------------------------------------------------------------------------
    |
    | When utilizing a RAM based store such as APC or Memcached, there might
    | be other applications utilizing the same cache. So, we'll specify a
    | value to get prefixed to all our keys so we can avoid collisions.
    |
    */

    'prefix' => env('CACHE_PREFIX', Str::slug(env('APP_NAME', 'yt'), '_').'_cache'),
];
