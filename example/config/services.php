<?php

use function Decmedia\Kernel\app_path;

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect' => env('GOOGLE_REDIRECT_URI')
    ],

    /*
    |--------------------------------------------------------------------------
    | Framework that makes responsive email
    |--------------------------------------------------------------------------
    |
    | MJML is a markup language designed to reduce the pain of coding a responsive email.
    | Its semantic syntax makes it easy and straightforward and its rich standard components
    | library speeds up your development time and lightens your email codebase.
    | MJML’s open-source engine generates high quality responsive
    | HTML compliant with best practices.
    |
    | Get key: https://mjml.io/api
    |
    */

    'mjml' => [
        'client_id' => env('MJML_CLIENT_ID'),
        'client_secret' => env('MJML_CLIENT_SECRET'),
        'client_public' => env('MJML_CLIENT_PUBLIC'),
        'binary_file' => env('MJML_BINARY_FILE', realpath(app_path('../node_modules/.bin/mjml'))),
    ],
];
