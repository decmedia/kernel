<?php

use function Decmedia\Kernel\env;

return [

    /*
    |--------------------------------------------------------------------------
    | Tokens setting
    |--------------------------------------------------------------------------
    |
    | Настройки по умолчанию, для параметров выдачи токенов авторизации для API запросов
    |
    */

    // Returns the default audience for the tokens
    'token_audience'    => env('TOKEN_AUDIENCE', 'http://localhost'),

    // Returns the time drift i.e. token will be valid not before
    'token_not_before'  => env('TOKEN_NOT_BEFORE', 1),

    // Returns the expiry time for the token
    'token_expiration'  => env('TOKEN_EXPIRATION', 86400),
];
