<?php

use function Decmedia\Kernel\env;
use function Decmedia\Kernel\resource_path;
use function Decmedia\Kernel\storage_path;

return [

    /*
    |--------------------------------------------------------------------------
    | View Storage Paths
    |--------------------------------------------------------------------------
    |
    | Most templating systems load templates from disk. Here you may specify
    | an array of paths that should be checked for your views. Of course
    | the usual Laravel view path has already been registered for you.
    |
    */

    'path' => env(
        'VIEW_COMPILED_PATH',
        resource_path('view')
    ),

    /*
    |--------------------------------------------------------------------------
    | Compiled View Path
    |--------------------------------------------------------------------------
    |
    | This option determines where all the compiled Blade templates will be
    | stored for your application. Typically, this is within the storage
    | directory. However, as usual, you are free to change this value.
    |
    */

    'compiled' => env(
        'VIEW_COMPILED_PATH',
        storage_path('cache/compiled-templates/')
    ),

    /*
    |--------------------------------------------------------------------------
    | Framework that makes responsive email
    |--------------------------------------------------------------------------
    |
    | MJML is a markup language designed to reduce the pain of coding a responsive email.
    | Its semantic syntax makes it easy and straightforward and its rich standard components
    | library speeds up your development time and lightens your email codebase.
    | MJML’s open-source engine generates high quality responsive
    | HTML compliant with best practices.
    |
    | Get key: https://mjml.io/api
    | Supported: "binary", "api"
    |
    */

    'mjml' => env('VIEW_MJML_RENDER', 'binary'),
];
