<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new application instance
| which serves as the "glue" for all the components.
|
*/

$app = new Decmedia\Kernel\Foundation\Application(
    $_ENV['APP_BASE_PATH'] ?? dirname(__DIR__)
);

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
    Decmedia\Kernel\Contracts\Http\Kernel::class,
    Lg\App\Http\Kernel::class
);

$app->singleton(
    Decmedia\Kernel\Contracts\Console\Kernel::class,
    Lg\App\Console\Kernel::class
);

$app->singleton(
    Decmedia\Kernel\Contracts\Debug\ExceptionHandler::class,
    Decmedia\Kernel\Foundation\Exceptions\Handler::class
);

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
