<?php
declare(strict_types=1);

use Phalcon\Loader;

$loader     = new Loader();
$namespaces = [
    // 'Yt'        => app_path('/src'),
    'Yt\Cli'    => __DIR__.'/../console',
    'Lg\App'    => __DIR__.'/../app',
    'Yt\Tests'  => __DIR__.'/../tests',
];

$loader->registerNamespaces($namespaces);
$loader->register();

/**
 * Composer Autoloader
 */

require __DIR__.'/../vendor/autoload.php';

