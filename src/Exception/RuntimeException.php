<?php
declare(strict_types=1);

/**
 * RuntimeException is an exception that might also occur in code that is written and configured 'perfectly'.
 * Such an exception may be caused by input from the end user, or an error in (the communication with) an external
 * system. When a RuntimeException is thrown, it should not require a fix in the code.
 * If the exception ends up uncaught however, we should add some code to handle it.
 * This may mean logging the error, using a fallback strategy, reporting the error to the user,
 * or a combination thereof.
 */

namespace Decmedia\Kernel\Exception;

class RuntimeException extends Exception
{
}
