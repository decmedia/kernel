<?php

namespace Decmedia\Kernel\Notification\Mail;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use GuzzleHttp\Client as HttpClient;
use Swift_Transport;
use Webmozart\Assert\Assert;
use Decmedia\Kernel\Contracts\Config\Repository;
use Decmedia\Kernel\Contracts\Foundation\Application;
use Decmedia\Kernel\Notification\Mail\Transport\ArrayTransport;
use Decmedia\Kernel\Notification\Mail\Transport\LogTransport;
use Decmedia\Kernel\Notification\Mail\Transport\SesTransport;
use Decmedia\Kernel\Contracts\Mail\Factory as FactoryContract;
use Decmedia\Kernel\Notification\Mail\Transport\MailgunTransport;
use Decmedia\Kernel\Support\Manager\Manager;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Swift_DependencyContainer;
use Swift_Mailer;
use Swift_SendmailTransport as SendmailTransport;
use Swift_SmtpTransport as SmtpTransport;

// use Postmark\ThrowExceptionOnFailurePlugin;
// use Postmark\Transport as PostmarkTransport;

/**
 * Class MailManager
 * @package Decmedia\Kernel\Notification\Mail
 *
 * @property Application $application
 */
class MailManager extends Manager implements FactoryContract
{

    /**
     * The array of resolved mailers.
     *
     * @var array
     */
    protected $mailers = [];

    /**
     * @param string|null $name
     * @return mixed
     */
    public function mailer($name = null)
    {
        $name = $name ?: $this->getDefaultDriver();

        return $this->mailers[$name] = $this->get($name);
    }

    /**
     * Get the default mail driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        // Here we will check if the "driver" key exists and if it does we will use
        // that as the default driver in order to provide support for old styles
        // of the Laravel mail configuration file for backwards compatibility.
        return $this->config->get('mail.driver') ??
            $this->config->get('mail.default');
    }

    /**
     * Attempt to get the mailer from the local cache.
     *
     * @param string $name
     * @return Mailer
     */
    protected function get(string $name)
    {
        return $this->mailers[$name] ?? $this->resolve($name);
    }

    /**
     * Resolve the given mailer.
     *
     * @param string $name
     * @return Mailer
     *
     * @throws InvalidArgumentException
     */
    protected function resolve(string $name)
    {
        $sectionConfiguration = $this->getConfig($name);

        Assert::notNull($sectionConfiguration, "Mailer [{$name}] is not defined.");

        // Once we have created the mailer instance we will set a container instance
        // on the mailer. This allows us to resolve mailer classes via containers
        // for maximum testability on said classes instead of passing Closures.
        $mailer = new Mailer(
            $name,
            $this->container->getShared('view'),
            $this->createSwiftMailer($sectionConfiguration),
            $this->container->getShared('events')
        );

        //TODO: queue
        if ($this->application->bound('queue')) {
            $mailer->setQueue($this->application['queue']);
        }

        // Next we will set all of the global addresses on this mailer, which allows
        // for easy unification of all "from" addresses as well as easy debugging
        // of sent messages since these will be sent to a single email address.
        foreach (['from', 'reply_to', 'to', 'return_path'] as $type) {
            $this->setGlobalAddress($mailer, $sectionConfiguration, $type);
        }

        return $mailer;
    }

    /**
     * Set a global address on the mailer by type.
     *
     * @param Mailer $mailer
     * @param array $config
     * @param string $type
     * @return void
     */
    protected function setGlobalAddress(Mailer $mailer, array $config, string $type)
    {
        $address = Arr::get($config, $type, $this->application['config']['mail.'.$type]);

        if (is_array($address) && isset($address['address'])) {
            $mailer->{'always'.Str::studly($type)}($address['address'], $address['name']);
        }
    }

    /**
     * Get the mail connection configuration.
     *
     * @param  string  $name
     * @return array
     */
    protected function getConfig(string $name)
    {
        // Here we will check if the "driver" key exists and if it does we will use the
        // entire mail configuration file as the "driver" config in order to provide "BC"
        return $this->config->get('mail.driver')
            ? $this->config->get('mail')
            : $this->config->get("mail.mailers.{$name}");
    }

    /**
     * Create the SwiftMailer instance for the given configuration.
     *
     * @param array $config
     * @return Swift_Mailer
     */
    protected function createSwiftMailer($config)
    {
        if ($config['domain'] ?? false) {
            Swift_DependencyContainer::getInstance()
                ->register('mime.idgenerator.idright')
                ->asValue($config['domain']);
        }

        return new Swift_Mailer($this->createTransport($config));
    }

    /**
     * Create a new transport instance.
     *
     * @param array $config
     * @return Swift_Transport
     */
    public function createTransport($config)
    {
        // Here we will check if the "transport" key exists and if it doesn't we will
        // assume an application is still using the legacy mail configuration file
        // format and use the "mail.driver" configuration option instead for BC.
        $transport = $config['transport'] ?? $this->application['config']['mail.driver'];

        if (isset($this->customCreators[$transport])) {
            return call_user_func($this->customCreators[$transport], $config);
        }

        Assert::notFalse(
            trim($transport) === '' || method_exists($this, $method = 'create'.ucfirst($transport).'Transport'),
            sprintf("Unsupported mail transport [%s].", $transport)
        );

        return $this->{$method}($config);
    }

    /**
     * Create an instance of the Mail Swift Transport driver.
     *
     * @return SendmailTransport
     */
    protected function createMailTransport()
    {
        return new SendmailTransport;
    }

    /**
     * Create an instance of the Mailgun Swift Transport driver.
     *
     * @param array $config
     * @return MailgunTransport
     */
    protected function createMailgunTransport($config)
    {
        if (!$config['secret']) {
            $config = $this->config->get('mailers.mailgun', []);
        }

        return new MailgunTransport(
            $this->guzzle($config),
            $config['secret'],
            $config['domain'],
            $config['endpoint'] ?? null
        );
    }

    /**
     * Create an instance of the Log Swift Transport driver.
     *
     * @param \Decmedia\Kernel\Contracts\Config\Repository $config
     * @return \Decmedia\Kernel\Notification\Mail\Transport\LogTransport
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function createLogTransport()
    {
        // $logger = $this->getDI()->getShared('logger');
        $logger = $this->application->make('logger');

        return new LogTransport($logger);
    }

    /**
     * Get a fresh Guzzle HTTP client instance.
     *
     * @param  array $config
     * @return HttpClient
     */
    protected function guzzle($config)
    {
        return new HttpClient(Arr::add(
            $config['guzzle'] ?? [],
            'connect_timeout',
            60
        ));
    }

    /**
     * Dynamically call the default driver instance.
     *
     * @param string $method
     * @param array $parameters
     * @return mixed
     */
    public function __call(string $method, array $parameters)
    {
        return $this->mailer()->$method(...$parameters);
    }
}
