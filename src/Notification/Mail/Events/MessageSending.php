<?php

namespace Decmedia\Kernel\Notification\Mail\Events;

use Swift_Message;

class MessageSending
{
    /**
     * The Swift message instance.
     *
     * @var Swift_Message
     */
    public $message;

    /**
     * The message data.
     *
     * @var array
     */
    public $data;

    /**
     * Create a new event instance.
     *
     * @param Swift_Message $message
     * @param array $data
     */
    public function __construct(Swift_Message $message, $data = [])
    {
        $this->data = $data;
        $this->message = $message;
    }
}
