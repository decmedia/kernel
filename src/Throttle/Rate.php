<?php

declare(strict_types=1);

namespace Decmedia\Kernel\Throttle;

use Webmozart\Assert\Assert;

class Rate
{
    /** @var int */
    protected $operations;

    /** @var int */
    protected $interval;

    /**
     * Rate constructor.
     * @param int $operations
     * @param int $interval
     */
    final protected function __construct(int $operations, int $interval)
    {
        Assert::true($operations > 0, 'Quota must be greater than zero');
        Assert::true($interval > 0, 'Seconds interval must be greater than zero');

        $this->operations = $operations;
        $this->interval = $interval;
    }

    /**
     * @param int $operations
     * @return static
     */
    public static function perSecond(int $operations)
    {
        return new static($operations, 1);
    }

    /**
     * @param int $operations
     * @return static
     */
    public static function perMinute(int $operations)
    {
        return new static($operations, 60);
    }

    /**
     * @param int $operations
     * @return static
     */
    public static function perHour(int $operations)
    {
        return new static($operations, 3600);
    }

    /**
     * @param int $operations
     * @return static
     */
    public static function perDay(int $operations)
    {
        return new static($operations, 86400);
    }

    /**
     * @param int $operations
     * @param int $interval
     * @return static
     */
    public static function custom(int $operations, int $interval)
    {
        return new static($operations, $interval);
    }

    /**
     * @return int
     */
    public function getOperations(): int
    {
        return $this->operations;
    }

    /**
     * @return int
     */
    public function getInterval(): int
    {
        return $this->interval;
    }
}
