<?php

declare(strict_types=1);

namespace Decmedia\Kernel\Throttle;

use \Predis\ClientInterface;
use Decmedia\Kernel\Throttle\Exception\LimitExceeded;
use function ceil;
use function max;
use function time;

final class PredisRateLimiter implements RateLimiter, SilentRateLimiter
{
    /** @var ClientInterface */
    private $predis;

    /** @var string */
    private $keyPrefix;

    /**
     * PredisRateLimiter constructor.
     * @param ClientInterface $predis
     * @param string $keyPrefix
     */
    public function __construct(ClientInterface $predis, string $keyPrefix = '')
    {
        $this->predis = $predis;
        $this->keyPrefix = $keyPrefix;
    }

    /**
     * @param string $identifier
     * @param Rate $rate
     */
    public function limit(string $identifier, Rate $rate): void
    {
        $key = $this->key($identifier, $rate->getInterval());

        $current = $this->getCurrent($key);

        if ($current >= $rate->getOperations()) {
            throw LimitExceeded::for($identifier, $rate);
        }

        $this->updateCounter($key, $rate->getInterval());
    }

    /**
     * @param string $identifier
     * @param Rate $rate
     * @return Status
     */
    public function limitSilently(string $identifier, Rate $rate): Status
    {
        $key = $this->key($identifier, $rate->getInterval());

        $current = $this->getCurrent($key);

        if ($current <= $rate->getOperations()) {
            $current = $this->updateCounter($key, $rate->getInterval());
        }

        return Status::from(
            $identifier,
            $current,
            $rate->getOperations(),
            time() + $this->ttl($key)
        );
    }

    /**
     * @param string $identifier
     * @param int $interval
     * @return string
     */
    private function key(string $identifier, int $interval): string
    {
        return "{$this->keyPrefix}{$identifier}:$interval";
    }

    /**
     * @param string $key
     * @return int
     */
    private function getCurrent(string $key): int
    {
        return (int) $this->predis->get($key);
    }

    /**
     * @param string $key
     * @param int $interval
     * @return int
     */
    private function updateCounter(string $key, int $interval): int
    {
        $current = $this->predis->incr($key);

        if ($current === 1) {
            $this->predis->expire($key, $interval);
        }

        return $current;
    }

    /**
     * @param string $key
     * @return int
     */
    private function ttl(string $key): int
    {
        return max((int) ceil($this->predis->pttl($key) / 1000), 0);
    }
}
