<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Throttle;

use Redis;
use Decmedia\Kernel\Support\Manager\Manager;

class AdapterManager extends Manager
{
    /**
     * @return mixed|string|null
     */
    public function getDefaultDriver()
    {
        return $this->config->get('cache.default', 'redis');
    }

    /**
     * @return RedisRateLimiter
     */
    public function createRedisDriver()
    {
        $redis = new Redis();
        $redis->connect('127.0.0.1');

        return new RedisRateLimiter($redis);
    }
}
