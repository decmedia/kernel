<?php

declare(strict_types=1);

namespace Decmedia\Kernel\Throttle;

interface SilentRateLimiter
{
    public function limitSilently(string $identifier, Rate $rate): Status;
}
