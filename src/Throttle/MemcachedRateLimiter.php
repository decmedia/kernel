<?php

declare(strict_types=1);

namespace Decmedia\Kernel\Throttle;

use Memcached;
use Decmedia\Kernel\Throttle\Exception\CannotUseRateLimiter;
use Decmedia\Kernel\Throttle\Exception\LimitExceeded;
use function max;
use function sprintf;
use function time;

final class MemcachedRateLimiter implements RateLimiter, SilentRateLimiter
{
    private const MEMCACHED_SECONDS_LIMIT = 2592000; // 30 days in seconds

    /** @var Memcached */
    private $memcached;

    /** @var string */
    private $keyPrefix;

    /**
     * MemcachedRateLimiter constructor.
     * @param Memcached $memcached
     * @param string $keyPrefix
     */
    public function __construct(Memcached $memcached, string $keyPrefix = '')
    {
        // @see https://www.php.net/manual/en/memcached.increment.php#111187
        if ($memcached->getOption(Memcached::OPT_BINARY_PROTOCOL) !== 1) {
            throw new CannotUseRateLimiter('Memcached "OPT_BINARY_PROTOCOL" option should be set to "true".');
        }

        $this->memcached = $memcached;
        $this->keyPrefix = $keyPrefix;
    }

    /**
     * @param string $identifier
     * @param Rate $rate
     */
    public function limit(string $identifier, Rate $rate): void
    {
        $limitKey = $this->limitKey($identifier, $rate->getInterval());

        $current = $this->getCurrent($limitKey);
        if ($current >= $rate->getOperations()) {
            throw LimitExceeded::for($identifier, $rate);
        }

        $this->updateCounter($limitKey, $rate->getInterval());
    }

    /**
     * @param string $identifier
     * @param Rate $rate
     * @return Status
     */
    public function limitSilently(string $identifier, Rate $rate): Status
    {
        $interval = $rate->getInterval();
        $limitKey = $this->limitKey($identifier, $interval);
        $timeKey = $this->timeKey($identifier, $interval);

        $current = $this->getCurrent($limitKey);
        if ($current <= $rate->getOperations()) {
            $current = $this->updateCounterAndTime($limitKey, $timeKey, $interval);
        }

        return Status::from(
            $identifier,
            $current,
            $rate->getOperations(),
            time() + max(0, $interval - $this->getElapsedTime($timeKey))
        );
    }

    /**
     * @param string $identifier
     * @param int $interval
     * @return string
     */
    private function limitKey(string $identifier, int $interval): string
    {
        return sprintf('%s%s:%d', $this->keyPrefix, $identifier, $interval);
    }

    /**
     * @param string $identifier
     * @param int $interval
     * @return string
     */
    private function timeKey(string $identifier, int $interval): string
    {
        return sprintf('%s%s:%d:time', $this->keyPrefix, $identifier, $interval);
    }

    /**
     * @param string $limitKey
     * @return int
     */
    private function getCurrent(string $limitKey): int
    {
        return (int) $this->memcached->get($limitKey);
    }

    /**
     * @param string $limitKey
     * @param string $timeKey
     * @param int $interval
     * @return int
     */
    private function updateCounterAndTime(string $limitKey, string $timeKey, int $interval): int
    {
        $current = $this->updateCounter($limitKey, $interval);

        if ($current === 1) {
            $this->memcached->add($timeKey, time(), $this->intervalToMemcachedTime($interval));
        }

        return $current;
    }

    /**
     * @param string $limitKey
     * @param int $interval
     * @return int
     */
    private function updateCounter(string $limitKey, int $interval): int
    {
        $current = $this->memcached->increment($limitKey, 1, 1, $this->intervalToMemcachedTime($interval));

        return $current === false ? 1 : $current;
    }

    /**
     * @param string $timeKey
     * @return int
     */
    private function getElapsedTime(string $timeKey): int
    {
        return time() - (int) $this->memcached->get($timeKey);
    }

    /**
     * Interval to Memcached expiration time.
     *
     * @see https://www.php.net/manual/en/memcached.expiration.php
     *
     * @param int $interval
     * @return int
     */
    private function intervalToMemcachedTime(int $interval): int
    {
        return $interval <= self::MEMCACHED_SECONDS_LIMIT ? $interval : time() + $interval;
    }
}
