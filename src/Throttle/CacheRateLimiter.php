<?php

declare(strict_types=1);

namespace Decmedia\Kernel\Throttle;

use Phalcon\Cache\Adapter\AdapterInterface;
use Decmedia\Kernel\Contracts\Cache\Store;
use Decmedia\Kernel\Throttle\Exception\LimitExceeded;
use function max;
use function time;

final class CacheRateLimiter implements RateLimiter, SilentRateLimiter
{
    /** @var AdapterInterface */
    private $cache;

    /** @var string */
    private $keyPrefix;

    /**
     * RedisRateLimiter constructor.
     * @param Store $cache
     * @param string $keyPrefix
     */
    public function __construct(Store $cache, string $keyPrefix = '')
    {
        $this->cache = $cache->getAdapter();
        $this->keyPrefix = $keyPrefix;
    }

    /**
     * @param string $identifier
     * @param Rate $rate
     */
    public function limit(string $identifier, Rate $rate): void
    {
        $key = $this->key($identifier, $rate->getInterval());

        $current = $this->getCurrent($key, $rate->getInterval());

        if ($current >= $rate->getOperations()) {
            throw LimitExceeded::for($identifier, $rate);
        }

        $this->updateCounter($key);
    }

    /**
     * @param string $identifier
     * @param Rate $rate
     * @return Status
     */
    public function limitSilently(string $identifier, Rate $rate): Status
    {
        $key = $this->key($identifier, $rate->getInterval());

        $current = $this->getCurrent($key, $rate->getInterval());

        if ($current <= $rate->getOperations()) {
            $current = $this->updateCounter($key);
        }

        return Status::from(
            $identifier,
            $current,
            $rate->getOperations(),
            time() + $this->ttl($key)
        );
    }

    /**
     * @param string $identifier
     * @param int $interval
     * @return string
     */
    private function key(string $identifier, int $interval): string
    {
        return "{$this->keyPrefix}{$identifier}:$interval";
    }

    /**
     * @param string $key
     * @param int $interval
     * @return int
     */
    private function getCurrent(string $key, int $interval): int
    {
        $current = $this->cache->get($key);

        if (!$current) {
            $current = [1, time() + $interval];
            $this->cache->set($key, $current, $interval);
        }

        return (int) $current[0];
    }

    /**
     * @param string $key
     * @return int
     */
    private function updateCounter(string $key): int
    {
        $current = $this->cache->get($key);

        list($attempts, $ttl) = $current;
        $this->cache->set($key, [++$attempts, $ttl], $ttl - time());

        return (int) $attempts;
    }

    /**
     * @param string $key
     * @return int
     */
    private function ttl(string $key): int
    {
        $current = $this->cache->get($key);

        list($attempts, $ttl) = $current;

        return max((int) ($ttl - time()), 0);
    }
}
