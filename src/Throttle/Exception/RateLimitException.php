<?php

declare(strict_types=1);

namespace Decmedia\Kernel\Throttle\Exception;

use Throwable;

interface RateLimitException extends Throwable
{
}
