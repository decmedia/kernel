<?php

declare(strict_types=1);

namespace Decmedia\Kernel\Throttle\Exception;

use RuntimeException;

final class CannotUseRateLimiter extends RuntimeException implements RateLimitException
{
}
