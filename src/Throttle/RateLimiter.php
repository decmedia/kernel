<?php

declare(strict_types=1);

namespace Decmedia\Kernel\Throttle;

use Decmedia\Kernel\Throttle\Exception\LimitExceeded;

interface RateLimiter
{
    /**
     * @param string $identifier
     * @param Rate $rate
     * @throws LimitExceeded
     */
    public function limit(string $identifier, Rate $rate): void;
}
