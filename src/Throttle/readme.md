General purpose rate limiter that can be used to limit the rate at which certain operation can be performed. Default implementation uses Redis as backend.

https://github.com/nikolaposa/rate-limit

###Usage
#####Offensive rate limiting

```php
$rateLimiter = new CacheRateLimiter(Phalcon\Cache $cache);
$rateLimiter->limit('Request ip or name operation', Rate::perMinute(100));
```

```php
use RateLimit\Exception\LimitExceeded;
use RateLimit\Rate;
use RateLimit\RedisRateLimiter;
use Redis;

$rateLimiter = new RedisRateLimiter(new Redis());

$apiKey = 'abc123';

try {
    $rateLimiter->limit($apiKey, Rate::perMinute(100));
    
    //on success
} catch (LimitExceeded $exception) {
   //on limit exceeded
}
```

#####Silent rate limiting

```php
use RateLimit\Rate;
use RateLimit\RedisRateLimiter;
use Redis;

$rateLimiter = new RedisRateLimiter(new Redis());

$ipAddress = '192.168.1.2';
$status = $rateLimiter->limitSilently($ipAddress, Rate::perMinute(100));

echo $status->getRemainingAttempts(); //99
```
