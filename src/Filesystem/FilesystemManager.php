<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Filesystem;

use League\Flysystem\AwsS3V3\AwsS3V3Adapter;
use League\Flysystem\Ftp\FtpAdapter;
use League\Flysystem\Ftp\FtpConnectionOptions;
use League\Flysystem\InMemory\InMemoryFilesystemAdapter;
use Phalcon\Helper\Arr;
use Aws\S3\S3Client;
use Aws\Credentials\Credentials;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemAdapter;
use League\Flysystem\Local\LocalFilesystemAdapter;
use League\Flysystem\PhpseclibV2\SftpAdapter;
use League\Flysystem\PhpseclibV2\SftpConnectionProvider;
use Decmedia\Kernel\Support\Manager\Manager;
use Decmedia\Kernel\Filesystem\FilesystemAdapter as YtFilesystemAdapter;

class FilesystemManager extends Manager
{
    /**
     * The array of resolved filesystem drivers.
     *
     * @var array
     */
    protected $disks = [];

    /**
     * @return string
     */
    public function getDefaultDriver()
    {
        return $this->config->get('filesystems.default', 'local');
    }

    /**
     * @return string
     */
    public function getDefaultCloudDriver()
    {
        return $this->config->get('filesystems.cloud', 's3');
    }

    /**
     * Get a filesystem instance.
     *
     * @param  string|null  $name
     * @return Filesystem
     */
    public function drive($name = null)
    {
        return $this->disk($name);
    }

    /**
     * Get a filesystem instance.
     *
     * @param  string|null  $name
     * @return Filesystem
     */
    public function disk($name = null)
    {
        $name = $name ?: $this->getDefaultDriver();

        return $this->disks[$name] = $this->driver($name);
    }

    /**
     * Get a default cloud filesystem instance.
     *
     * @return \Illuminate\Contracts\Filesystem\Filesystem
     */
    public function cloud()
    {
        $name = $this->getDefaultCloudDriver();

        return $this->disks[$name] = $this->driver($name);
    }

    /**
     * Create an instance of the local driver.
     *
     * @link https://flysystem.thephpleague.com/v2/docs/adapter/local/
     * @return YtFilesystemAdapter
     */
    public function createLocalDriver()
    {
        $config = $this->config->get('filesystems.disks.local', []);

        $links = ($config['links'] ?? null) === 'skip'
            ? LocalFilesystemAdapter::SKIP_LINKS
            : LocalFilesystemAdapter::DISALLOW_LINKS;

        $adapter = new LocalFilesystemAdapter(
            $config['root'],
            null,
            LOCK_EX,
            $links
        );

        return $this->adapt($this->createFlysystem($adapter, $config));
    }

    /**
     * Create an instance of the s3 driver.
     *
     * @link https://flysystem.thephpleague.com/v2/docs/adapter/aws-s3-v3/
     * @return YtFilesystemAdapter
     */
    public function createS3Driver()
    {
        $config = $this->config->get('filesystems.disks.s3');
        $config = count($config) > 0 ? $config->toArray() : [];

        $credentials = new Credentials($config['key'], $config['secret']);
        $client = new S3Client([
            'version'     => 'latest',
            'region'      => $config['region'],
            'credentials' => $credentials
        ]);

        $adapter = new AwsS3V3Adapter(
            $client,
            $config['bucket']
        );
        return $this->adapt($this->createFlysystem($adapter, $config));
    }

    /**
     * Create an instance of the memory driver.
     * This adapter keeps the filesystem completely in memory.
     * This is useful when you need a filesystem, but don’t want it persisted.
     *
     * @link https://flysystem.thephpleague.com/v2/docs/adapter/in-memory/
     * @return YtFilesystemAdapter
     */
    public function createMemoryDriver()
    {
        $adapter = new InMemoryFilesystemAdapter();
        return $this->adapt($this->createFlysystem($adapter, []));
    }

    /**
     * Create an instance of the sftp driver.
     *
     * @link https://flysystem.thephpleague.com/v2/docs/adapter/sftp/
     * @return YtFilesystemAdapter
     */
    public function createSftpDriver()
    {
        $config = $this->config->get('filesystems.disks.sftp');
        $config = count($config) > 0 ? $config->toArray() : [];

        $configSftp = Arr::whiteList(
            $config,
            ['host', 'username', 'password', 'privateKey', 'passphrase', 'port']
        );

        $connection = new SftpConnectionProvider(...$configSftp);

        $adapter = new SftpAdapter(
            $connection,
            $config['root'] ?? '/', // root path (required)
            null
        );

        return $this->adapt($this->createFlysystem($adapter, $config));
    }

    /**
     * Create an instance of the ftp driver.
     *
     * @link https://flysystem.thephpleague.com/v2/docs/adapter/ftp/
     * @return YtFilesystemAdapter
     */
    public function createFtpDriver()
    {
        $config = $this->config->get('filesystems.disks.ftp');
        $config = count($config) > 0 ? $config->toArray() : [];

        $configFtp = Arr::whiteList(
            $config,
            ['host', 'username', 'password', 'ssl', 'timeout', 'port', 'utf8', 'root', 'passive', 'transferMode',
                'systemType', 'ignorePassiveAddress', 'timestampsOnUnixListingsEnabled', 'recurseManually']
        );

        $adapter = new FtpAdapter(
            FtpConnectionOptions::fromArray($configFtp)
        );

        return $this->adapt($this->createFlysystem($adapter, $config));
    }

    /**
     * Dynamically call the default driver instance.
     *
     * @param string $method
     * @param array $parameters
     * @return mixed
     */
    public function __call(string $method, array $parameters)
    {
        return $this->disk()->$method(...$parameters);
    }

    /**
     * Create a Flysystem instance with the given adapter.
     *
     * @param FilesystemAdapter $adapter
     * @param array $config
     * @return Filesystem
     */
    protected function createFlysystem(FilesystemAdapter $adapter, array $config)
    {
        return new Filesystem($adapter, count($config) > 0 ? $config : []);
    }

    /**
     * Adapt the filesystem implementation.
     * @param Filesystem $adapter
     * @return YtFilesystemAdapter
     */
    protected function adapt(Filesystem $adapter)
    {
        return new YtFilesystemAdapter($adapter);
    }
}
