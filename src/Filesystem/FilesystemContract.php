<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Filesystem;

use League\Flysystem\FilesystemOperator;

abstract class FilesystemContract implements FilesystemOperator
{

}
