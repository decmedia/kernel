<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Filesystem;

use BadMethodCallException;
use Illuminate\Support\Str;
use League\Flysystem\DirectoryListing;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use League\Flysystem\UnableToDeleteFile;
use Decmedia\Kernel\Http\File;
use Psr\Http\Message\StreamInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Decmedia\Kernel\Http\UploadedFile;

/**
 * @mixin \League\Flysystem\FilesystemAdapter
 */
class FilesystemAdapter extends FilesystemContract
{
    /**
     * The Flysystem filesystem implementation.
     *
     * @var \League\Flysystem\FilesystemAdapter
     */
    protected $driver;

    /**
     * Create a new filesystem adapter instance.
     *
     * @param Filesystem $driver
     */
    public function __construct(Filesystem $driver)
    {
        $this->driver = $driver;
    }

    /**
     * Pass dynamic methods call onto Flysystem.
     *
     * @param string $method
     * @param array $parameters
     * @return mixed
     *
     * @throws BadMethodCallException
     */
    public function __call(string $method, array $parameters)
    {
        return call_user_func_array([$this->driver, $method], $parameters);
    }

    /**
     * Write the contents of a file.
     *
     * @param string $path
     * @param string|resource $contents
     * @param mixed $options
     *
     * @return bool
     *
     * @throws FilesystemException
     */
    public function put(string $path, $contents, $options = [])
    {
        $options = is_string($options)
            ? ['visibility' => $options]
            : (array) $options;

        // If the given contents is actually a file or uploaded file instance than we will
        // automatically store the file using a stream. This provides a convenient path
        // for the developer to store streams without managing them manually in code.
        if ($contents instanceof File ||
            $contents instanceof UploadedFile) {
            return $this->putFile($path, $contents, $options);
        }

        if ($contents instanceof StreamInterface) {
            $this->driver->writeStream($path, $contents->detach(), $options);
            return true;
        }

        is_resource($contents)
            ? $this->driver->writeStream($path, $contents, $options)
            : $this->driver->write($path, $contents, $options);

        return true;
    }

    /**
     * Store the uploaded file on the disk.
     *
     * @param string $path
     * @param File|string $file
     * @param mixed $options
     * @return string|false
     * @throws FilesystemException
     */
    public function putFile(string $path, $file, $options = [])
    {
        $file = is_string($file) ? new File($file) : $file;

        return $this->putFileAs($path, $file, $file->hashName(), $options);
    }

    /**
     * Store the uploaded file on the disk with a given name.
     *
     * @param string $path
     * @param File|string $file
     * @param string $name
     * @param mixed $options
     * @return string|false
     * @throws FilesystemException
     */
    public function putFileAs(string $path, $file, string $name, $options = [])
    {
        $stream = fopen(is_string($file) ? $file : $file->getRealPath(), 'r');

        // Next, we will format the path of the file and store the file using a stream since
        // they provide better performance than alternatives. Once we write the file this
        // stream will get closed automatically by us so the developer doesn't have to.
        $result = $this->put(
            $path = trim($path.'/'.$name, '/'),
            $stream,
            $options
        );

        if (is_resource($stream)) {
            fclose($stream);
        }

        return $result ? $path : false;
    }

    /**
     * Prepend to a file.
     *
     * @param string $path
     * @param string $data
     * @param string $separator
     * @return bool
     * @throws FilesystemException
     */
    public function prepend(string $path, string $data, $separator = PHP_EOL)
    {
        if ($this->fileExists($path)) {
            return $this->put($path, $data.$separator.$this->driver->read($path));
        }

        return $this->put($path, $data);
    }

    /**
     * Append to a file.
     *
     * @param string $path
     * @param string $data
     * @param string $separator
     * @return bool
     * @throws FilesystemException
     */
    public function append(string $path, string $data, $separator = PHP_EOL)
    {
        if ($this->fileExists($path)) {
            return $this->put($path, $this->driver->read($path).$separator.$data);
        }

        return $this->put($path, $data);
    }

    /**
     * Create a streamed response for a given file.
     *
     * @param string $path
     * @param string|null $name
     * @param array|null $headers
     * @param string|null $disposition
     * @return StreamedResponse
     * @throws FilesystemException
     */
    public function response(string $path, $name = null, array $headers = [], $disposition = 'inline')
    {
        $response = new StreamedResponse;

        $filename = $name ?? basename($path);

        $disposition = $response->headers->makeDisposition(
            $disposition,
            $filename,
            $this->fallbackName($filename)
        );

        // TODO - replace to phalcon Stream (https://docs.phalcon.io/4.0/en/response)
        $response->headers->replace($headers + [
                'Content-Type' => $this->mimeType($path),
                'Content-Length' => $this->driver->fileSize($path),
                'Content-Disposition' => $disposition,
            ]);

        $response->setCallback(function () use ($path) {
            $stream = $this->readStream($path);
            fpassthru($stream);
            fclose($stream);
        });

        return $response;
    }

    /**
     * Create a streamed download response for a given file.
     *
     * @param string $path
     * @param string|null $name
     * @param array|null $headers
     * @return StreamedResponse
     * @throws FilesystemException
     */
    public function download(string $path, $name = null, array $headers = [])
    {
        return $this->response($path, $name, $headers, 'attachment');
    }

    /**
     * Convert the string to ASCII characters that are equivalent to the given name.
     *
     * @param string $name
     * @return string
     */
    protected function fallbackName(string $name)
    {
        return str_replace('%', '', Str::ascii($name));
    }

    /**
     * {@inheritdoc}
     */
    public function fileExists(string $path): bool
    {
        return $this->driver->fileExists($path);
    }

    /**
     * {@inheritdoc}
     */
    public function write(string $path, string $contents, array $config = []): void
    {
        $this->driver->write($path, $contents, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function writeStream(string $path, $contents, array $config = []): void
    {
        $this->driver->writeStream($path, $contents, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function read(string $path): string
    {
        return $this->driver->read($path);
    }

    /**
     * {@inheritdoc}
     */
    public function readStream(string $path)
    {
        return $this->driver->readStream($path);
    }

    /**
     * Delete the file at a given path.
     *
     * @param string|array $paths
     *
     * @throws FilesystemException
     * @throws UnableToDeleteFile
     */
    public function delete($paths): void
    {
        $paths = is_array($paths) ? $paths : func_get_args();

        foreach ($paths as $path) {
            $this->driver->delete($path);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function deleteDirectory(string $path): void
    {
        $this->driver->deleteDirectory($path);
    }

    /**
     * {@inheritdoc}
     */
    public function createDirectory(string $path, array $config = []): void
    {
        $this->driver->createDirectory($path, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function setVisibility(string $path, $visibility): void
    {
        $this->driver->setVisibility($path, $visibility);
    }

    /**
     * {@inheritdoc}
     */
    public function visibility(string $path): string
    {
        return $this->driver->visibility($path);
    }

    /**
     * {@inheritdoc}
     */
    public function mimeType(string $path): string
    {
        return $this->driver->mimeType($path);
    }

    /**
     * {@inheritdoc}
     */
    public function lastModified(string $path): int
    {
        return $this->driver->lastModified($path);
    }

    /**
     * {@inheritdoc}
     */
    public function fileSize(string $path): int
    {
        return $this->driver->fileSize($path);
    }

    /**
     * {@inheritdoc}
     */
    public function listContents(string $location, bool $deep = self::LIST_SHALLOW): DirectoryListing
    {
        return $this->driver->listContents($location, $deep);
    }

    /**
     * {@inheritdoc}
     */
    public function move(string $source, string $destination, array $config = []): void
    {
        $this->driver->move($source, $destination, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function copy(string $source, string $destination, array $config = []): void
    {
        $this->driver->copy($source, $destination, $config);
    }
}
