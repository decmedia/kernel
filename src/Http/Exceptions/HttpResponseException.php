<?php

namespace Decmedia\Kernel\Http\Exceptions;

use RuntimeException;
use Decmedia\Kernel\Http\Response;

class HttpResponseException extends RuntimeException
{
    /**
     * The underlying response instance.
     *
     * @var \Symfony\Component\HttpFoundation\Response
     */
    protected $response;

    /**
     * Create a new HTTP response exception instance.
     *
     * @param  \Decmedia\Kernel\Http\Response  $response
     * @return void
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * Get the underlying response instance.
     *
     * @return \Decmedia\Kernel\Http\Response
     */
    public function getResponse()
    {
        return $this->response;
    }
}
