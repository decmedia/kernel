<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Http\Middleware;

use Decmedia\Kernel\Http\Request;
use Decmedia\Kernel\Http\Response;
use Decmedia\Kernel\Traits\QueryTrait;
use Decmedia\Kernel\Traits\ResponseTrait;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;

/**
 * Class AuthenticationMiddleware
 */
class AuthenticationMiddleware implements MiddlewareInterface
{
    use ResponseTrait;
    use QueryTrait;

    /**
     * @param Micro $api
     *
     * @return bool
     */
    public function call(Micro $api)
    {
        /** @var Request $request */
        $request  = $api->getService('request');
        /** @var Response $response */
        $response = $api->getService('response');

        if (true !== $request->isLoginPage() &&
            true === $request->isEmptyBearerToken()) {
            $this->halt(
                $api,
                $response::OK,
                'Invalid Token'
            );

            return false;
        }

        return true;
    }
}
