<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Http\Middleware;

use Phalcon\Helper\Str;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;
use Phalcon\Acl\Adapter\Memory as AclAdapter;
use Phalcon\Acl\Adapter\AbstractAdapter;
use Phalcon\Acl\Component;
use Phalcon\Acl\Enum;
use Phalcon\Acl\Role;

/**
 * Class AclMiddleware
 * @package Decmedia\Kernel\Http\Middleware
 */
abstract class AclMiddleware  implements MiddlewareInterface
{
    /** @var string $role */
    protected $role;

    /** @var string $controller */
    protected $controller;

    /** @var string $action */
    protected $action;

    /**
     * @param Micro $api
     *
     * @return bool
     */
    public function call(Micro $api)
    {
        $handler = $api->getActiveHandler();

        if ($handler instanceof \Closure) {
            return true;
        }

        if (is_string($handler)) {
            $arrHandler = Str::includes("::", $handler)
                ? explode('::', $handler)
                : [$handler, '__invoke'];
        } else {
            $arrHandler = [$handler[0]->getDefinition() ?? '', $handler[1]];
        }

        list($this->controller, $this->action) = $arrHandler;

        $acl = $this->getAcl();
        return $acl->isAllowed($this->role, $this->controller, $this->action);
    }

    /**
     * @return string
     */
    public function getActionPath(): string
    {
        return sprintf('%s : %s', $this->controller, $this->action);
    }

    /**
     * @return AbstractAdapter
     */
    private function getAcl(): AbstractAdapter
    {
        $acl = new AclAdapter();
        $acl->setDefaultAction(Enum::DENY);

        foreach ($this->allowedComponents() as $role => $accessData) {
            $acl->addRole(new Role($role), $accessData['implements']);

            foreach ($accessData['methods'] as $resource => $actions) {
                $acl->addComponent(new Component($resource), $actions);
                foreach ($actions as $action) {
                    $acl->allow($role, $resource, $action);
                }
            }
        }

        return $acl;
    }

    /**
     * @return array
     */
    abstract function allowedComponents(): array;
}
