<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Http\Middleware;

use Phalcon\Events\Event;
use Phalcon\Http\Response;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;
use Decmedia\Kernel\Traits\QueryTrait;
use Decmedia\Kernel\Traits\ResponseTrait;

/**
 * Class RequestMiddleware
 *
 * @property Response $response
 */
class RequestMiddleware implements MiddlewareInterface
{
    use ResponseTrait;
    use QueryTrait;

    /**
     * Receiving a JSON payload and checks it. If the JSON payload is not valid it will stop execution
     *
     * @param Event $event
     * @param Micro $application
     *
     * @returns bool
     * @return bool
     */
    public function beforeExecuteRoute(
        Event $event,
        Micro $application
    ) {

        $serverHeaders = $application->request->getHeaders();
        $contentType = $application->request->getHeader('Content-Type');
        $contentType = $contentType ?? $serverHeaders['CONTENT_TYPE'] ?? 'application/json';

        if (false !== strpos($contentType, 'application/json')) {
            $rawBody = $application->request->getRawBody();

            if (!empty($rawBody)) {
                json_decode($rawBody);
                if (JSON_ERROR_NONE !== json_last_error()) {
                    $this->halt(
                        $application,
                        $application->response::BAD_REQUEST,
                        'Invalid Json Payload'
                    );

                    return false;
                }

                $rawBody = $application->request->getJsonRawBody(true);
                if (!empty($rawBody)) {
                    foreach ($rawBody as $key => $value) {
                        $_REQUEST[$key] = $value;
                    }

                    unset($_REQUEST['_url']);
                }
            }
        }

        return true;
    }

    /**
     * @param Micro $application
     *
     * @returns bool
     * @return bool
     */
    public function call(Micro $application)
    {
        return true;
    }
}
