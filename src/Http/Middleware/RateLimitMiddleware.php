<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Http\Middleware;

use Phalcon\Mvc\Micro;
use Decmedia\Kernel\Contracts\Cache\Store;
use Decmedia\Kernel\Exception\Exception;
use Decmedia\Kernel\Http\Request;
use Decmedia\Kernel\Throttle\CacheRateLimiter;
use Decmedia\Kernel\Throttle\Exception\LimitExceeded;
use Decmedia\Kernel\Throttle\Rate;
use Decmedia\Kernel\Traits\ResponseTrait;

/**
 * Class TokenUserMiddleware
 */
class RateLimitMiddleware extends TokenBase
{
    use ResponseTrait;

    /**
     * @param Micro $api
     * @url https://github.com/nikolaposa/rate-limit
     *
     * @return bool
     * @throws Exception
     */
    public function call(Micro $api)
    {
        /** @var Request $request */
        $request = $api->getSharedService('request');
        $clientIp = $request->getClientIp();

        /** @var Store $cache */
        $cache = $api->getSharedService('cache');

        $rateLimiter = new CacheRateLimiter($cache);

        try {
            $rateLimiter->limit($clientIp, Rate::perMinute(100));
        } catch (LimitExceeded $exception) {
            $this->halt($api, 429, 'Too Many Requests');
            return false;
        }

        return true;
    }
}
