<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon API.
 *
 * (c) Phalcon Team <team@phalcon.io>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Decmedia\Kernel\Http\Middleware;

use Decmedia\Kernel\Config\Repository;
use Decmedia\Kernel\Contracts\Foundation\Application;
use Decmedia\Kernel\Http\Response;
use Decmedia\Kernel\Http\UrlSigner;
use Decmedia\Kernel\Traits\ResponseTrait;
use Illuminate\Support\Str;
use Phalcon\Di\Injectable;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;

/**
 * Class NotFoundMiddleware
 *
 * @property Micro    $application
 * @property Response $response
 * @property Repository $config
 */
class NotFoundMiddleware extends Injectable implements MiddlewareInterface
{
    use ResponseTrait;

    /**
     * Checks if the resource was found
     *
     * @return object|bool
     * @throws \Exception
     */
    public function beforeNotFound()
    {
        if (!empty($this->request->get('xyz')) && $this->dispatcher->getParam('forward') !== true) {
            $signer = new UrlSigner(
                $this->config->get('app.key'),
                $this->config->get('app.url') . "/api/*?xyz=true"
            );
            $signerParams = $signer->parseUrl($this->request->getServer('REQUEST_URI'));

            $params = $signerParams['params'] ? $signerParams['params'] : [];
            $params['forward'] = true;

            $this->dispatcher->setControllerName(Str::beforeLast($signerParams['controller'], 'Controller'));
            $this->dispatcher->setActionName(Str::replaceLast('Action', '', $signerParams['action']));
            $this->dispatcher->setParams($params);

            return $this->dispatcher->dispatch();
        }

        $this->halt(
            $this->application instanceof Application
                ? $this->application->getInfrastructure()
                : $this->application,
            $this->response::NOT_FOUND,
            $this->response->getHttpCodeDescription($this->response::NOT_FOUND)
        );

        return false;
    }

    /**
     * Call me
     *
     * @param Micro $api
     *
     * @return bool
     */
    public function call(Micro $api)
    {
        return true;
    }
}
