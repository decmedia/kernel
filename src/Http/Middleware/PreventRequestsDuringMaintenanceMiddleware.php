<?php

namespace Decmedia\Kernel\Http\Middleware;

use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;
use Decmedia\Kernel\Contracts\Foundation\Application;
use Decmedia\Kernel\Http\JsonResponse;
use Decmedia\Kernel\Http\Request;
use Decmedia\Kernel\Traits\ResponseTrait;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class PreventRequestsDuringMaintenanceMiddleware
 * @package Decmedia\Kernel\Http\Middleware
 *
 * @property Request $request
 * @property JsonResponse $response
 */
class PreventRequestsDuringMaintenanceMiddleware implements MiddlewareInterface
{
    use ResponseTrait;

    public function call(
        Micro $infrastructure
    ) {
        /** @var Application $application */
        $application = $infrastructure->getDI()->getShared('application');

        if ($application->isDownForMaintenance()) {
            $data = json_decode(file_get_contents($application->storagePath().'/framework/down'), true);

            if (isset($data['secret']) && $infrastructure->request->get('secret', '') === $data['secret']) {
                return true;
            }

            if (isset($data['redirect'])) {
                $path = $data['redirect'] === '/'
                    ? $data['redirect']
                    : trim($data['redirect'], '/');

                if ($infrastructure->request->path() !== $path) {
                    return $infrastructure->response->redirect($path, true)->send();
                }
            }

            if (isset($data['template'])) {
                return $infrastructure
                    ->response
                    ->setStatusCode($data['status'] ?? 503)
                    ->setHeader('Retry-After', $data['retry'] ?? 3600)
                    ->setContent($data['template'])
                    ->send()
                    ;
            }

            $application->terminate();

            tap($data['status'] ?? $infrastructure->response::UNAVAILABLE, function ($status) use ($infrastructure) {
                $this->halt(
                    $infrastructure,
                    $status,
                    $infrastructure->response->getHttpCodeDescription($status)
                );
            });

            return false;
        }

        return true;
    }
}
