<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Http\Middleware;

use Phalcon\Di\Injectable;
use Phalcon\Events\Event;
use Phalcon\Http\Request;
use Phalcon\Http\Response;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;

/**
 * CORSMiddleware
 *
 * @property Request  $request
 * @property Response $response
 */
class CORSMiddleware extends Injectable implements MiddlewareInterface
{
    /**
     * @param Event $event
     * @param Micro $application
     *
     * @returns bool
     * @return false
     */
    public function beforeHandleRoute(
        Event $event,
        Micro $application
    ) {

        if ($application->request->getHeader('ORIGIN')) {
            $origin = $application
                ->request
                ->getHeader('ORIGIN')
            ;
        } else {
            $origin = '*';
        }

        $application
            ->response
            ->setHeader(
                'Access-Control-Allow-Origin',
                $origin
            )
            ->setHeader(
                'Access-Control-Allow-Methods',
                'GET,PUT,POST,DELETE,OPTIONS'
            )
            ->setHeader(
                'Access-Control-Allow-Headers',
                'Origin, X-Requested-With, Content-Range, ' .
                'Content-Disposition, Content-Type, Authorization, YRole'
            )
            ->setHeader(
                'Access-Control-Allow-Credentials',
                'true'
            )
            ->setHeader(
                'Vary',
                $origin
            )
        ;


        if ($this->isPreflightRequest($application->request)) {
            $application->response
                ->setPayloadSuccess(['ok'])
                ->send();

            $application->stop();

            return false;
        }
    }

    /**
     * @param Micro $application
     *
     * @return bool
     */
    public function call(Micro $application)
    {
        return true;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function isCorsRequest($request)
    {
        return !empty($request->getHeader('Origin')) && !$this->isSameHost($request);
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function isPreflightRequest($request)
    {
        return $this->isCorsRequest($request)
            && $request->getMethod() === 'OPTIONS'
            && !empty($request->getHeader('Access-Control-Request-Method'));
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function isSameHost($request)
    {
        return $request->getHeader('Origin') === $this->getSchemeAndHttpHost($request);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getSchemeAndHttpHost($request)
    {
        return $request->getScheme() . '://' . $request->getHttpHost();
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getOrigin($request)
    {
        return $request->getHeader('Origin') ? $request->getHeader('Origin') : '*';
    }
}
