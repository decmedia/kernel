<?php

namespace Decmedia\Kernel\Http\OAuth\Facades;

use Decmedia\Kernel\Support\Facades\Facade;
use Decmedia\Kernel\Http\OAuth\Contracts\Factory;

/**
 * @method static \Decmedia\Kernel\Http\OAuth\Contracts\Provider driver(string $driver = null)
 * @see \Decmedia\Kernel\Http\OAuth\SocialiteManager
 */
class Socialite extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return Factory::class;
    }
}

