<?php

namespace Decmedia\Kernel\Http\OAuth\Contracts;

interface Factory
{
    /**
     * Get an OAuth provider implementation.
     *
     * @param  string  $driver
     * @return \Decmedia\Kernel\Http\OAuth\Contracts\Provider
     */
    public function driver($driver = null);
}
