<?php

namespace Decmedia\Kernel\Http\OAuth\One;

use InvalidArgumentException;

class MissingVerifierException extends InvalidArgumentException
{
    //
}
