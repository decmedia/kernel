<?php

namespace Decmedia\Kernel\Http\OAuth\One;

use InvalidArgumentException;

class MissingTemporaryCredentialsException extends InvalidArgumentException
{
    //
}
