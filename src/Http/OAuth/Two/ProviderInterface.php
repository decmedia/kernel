<?php

namespace Decmedia\Kernel\Http\OAuth\Two;

interface ProviderInterface
{
    /**
     * Redirect the user to the authentication page for the provider.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirect();

    /**
     * Get the User instance for the authenticated user.
     *
     * @return \Decmedia\Kernel\Http\OAuth\Two\User
     */
    public function user();
}
