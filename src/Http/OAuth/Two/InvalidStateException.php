<?php

namespace Decmedia\Kernel\Http\OAuth\Two;

use InvalidArgumentException;

class InvalidStateException extends InvalidArgumentException
{
    //
}
