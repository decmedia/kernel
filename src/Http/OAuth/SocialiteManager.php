<?php

namespace Decmedia\Kernel\Http\OAuth;

use Illuminate\Support\Arr;
use Decmedia\Kernel\Support\Manager\Manager;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Decmedia\Kernel\Http\OAuth\One\TwitterProvider;
use Decmedia\Kernel\Http\OAuth\Two\BitbucketProvider;
use Decmedia\Kernel\Http\OAuth\Two\FacebookProvider;
use Decmedia\Kernel\Http\OAuth\Two\GithubProvider;
use Decmedia\Kernel\Http\OAuth\Two\GitlabProvider;
use Decmedia\Kernel\Http\OAuth\Two\GoogleProvider;
use Decmedia\Kernel\Http\OAuth\Two\LinkedInProvider;
use League\OAuth1\Client\Server\Twitter as TwitterServer;

/**
 * Class SocialiteManager
 * @package Decmedia\Kernel\Http\OAuth
 *
 * @property \Decmedia\Kernel\Config\Repository $config
 * @property \Decmedia\Kernel\Contracts\Foundation\Application $application
 */
class SocialiteManager extends Manager implements Contracts\Factory
{
    /**
     * Get a driver instance.
     *
     * @param  string  $driver
     * @return mixed
     */
    public function with($driver)
    {
        return $this->driver($driver);
    }

    /**
     * Create an instance of the specified driver.
     *
     * @return \Decmedia\Kernel\Http\OAuth\Two\AbstractProvider
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function createGithubDriver()
    {
        $config = $this->config->get('services.github');

        return $this->buildProvider(
            GithubProvider::class,
            $config
        );
    }

    /**
     * Create an instance of the specified driver.
     *
     * @return \Decmedia\Kernel\Http\OAuth\Two\AbstractProvider
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function createFacebookDriver()
    {
        $config = $this->config->get('services.facebook');

        return $this->buildProvider(
            FacebookProvider::class,
            $config
        );
    }

    /**
     * Create an instance of the specified driver.
     *
     * @return \Decmedia\Kernel\Http\OAuth\Two\AbstractProvider
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function createGoogleDriver()
    {
        $config = $this->config->get('services.google');

        return $this->buildProvider(
            GoogleProvider::class,
            $config
        );
    }

    /**
     * Create an instance of the specified driver.
     *
     * @return \Decmedia\Kernel\Http\OAuth\Two\AbstractProvider
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function createLinkedinDriver()
    {
        $config = $this->config->get('services.linkedin');

        return $this->buildProvider(
            LinkedInProvider::class,
            $config
        );
    }

    /**
     * Create an instance of the specified driver.
     *
     * @return \Decmedia\Kernel\Http\OAuth\Two\AbstractProvider
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function createBitbucketDriver()
    {
        $config = $this->config->get('services.bitbucket');

        return $this->buildProvider(
            BitbucketProvider::class,
            $config
        );
    }

    /**
     * Create an instance of the specified driver.
     *
     * @return \Decmedia\Kernel\Http\OAuth\Two\AbstractProvider
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function createGitlabDriver()
    {
        $config = $this->config->get('services.gitlab');

        return $this->buildProvider(
            GitlabProvider::class,
            $config
        );
    }

    /**
     * Build an OAuth 2 provider instance.
     *
     * @param string $provider
     * @param array $config
     * @return \Decmedia\Kernel\Http\OAuth\Two\AbstractProvider
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function buildProvider($provider, $config)
    {
        return new $provider(
            $this->application->make('request'),
            $config['client_id'],
            $config['client_secret'],
            $this->formatRedirectUrl($config),
            Arr::get($config, 'guzzle', [])
        );
    }

    /**
     * Create an instance of the specified driver.
     *
     * @return \Decmedia\Kernel\Http\OAuth\One\AbstractProvider
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function createTwitterDriver()
    {
        $config = $this->config->get('services.twitter');

        return new TwitterProvider(
            $this->application->make('request'),
            new TwitterServer($this->formatConfig($config))
        );
    }

    /**
     * Format the server configuration.
     *
     * @param array $config
     * @return array
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function formatConfig(array $config)
    {
        return array_merge([
            'identifier' => $config['client_id'],
            'secret' => $config['client_secret'],
            'callback_uri' => $this->formatRedirectUrl($config),
        ], $config);
    }

    /**
     * Format the callback URL, resolving a relative URI if needed.
     *
     * @param array $config
     * @return string
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function formatRedirectUrl(array $config)
    {
        $redirect = value($config['redirect']);

        return Str::startsWith($redirect, '/')
                    ? $this->application->make('url')->to($redirect)
                    : $redirect;
    }

    /**
     * Get the default driver name.
     *
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    public function getDefaultDriver()
    {
        throw new InvalidArgumentException('No Socialite driver was specified.');
    }
}
