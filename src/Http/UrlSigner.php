<?php
/**
 * Tool to create and parse digitally signed URLs with a number of
 * secured parameters passed.
 *
 * Usage sample (URL builder):
 *   $signer = new HTTP_UrlSigner("my_secret", "http://example.com/img/*");
 *   echo '<img src="' . $signer->buildUrl(array('w' => 10, 'h' => 10)) . '" />';
 *
 * Usage sample (URL parser):
 *   $signer = new HTTP_UrlSigner("my_secret", "http://example.com/img/*");
 *   $params = $signer->parseUrl($_SERVER['REQUEST_URI']);
 *
 * This sample guarantees that we get the same $params content than we
 * packed at the builder stage, and nobody else could create such URLs
 * (if he does not know the secret, of course).
 *
 * Base URL (passed to the constructor) may be relative or absolute.
 * URL to be parsed may also be passed as relative or absolute.
 *
 * @version 1.01
 */

namespace Decmedia\Kernel\Http;

use Exception;
use Phalcon\DI\Injectable;

class UrlSigner extends Injectable
{

    /**
     * Replacements to convert base64 encoded string to safe URL.
     *
     * @var array
     */
    private $sigSafeBase64 = array(array('+', '/', "="), array('_', '~', '-'));

    /**
     * Secret code to make digital signature.
     *
     * @var string
     */
    private $sigSecret;

    /**
     * http://example.com/some/a_*_/path?abc
     * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *
     * @var string
     */
    private $sigMask;

    /**
     * http://example.com/some/a_*_/path?abc
     * ^^^^^^^^^^^^^^^^^^^^^^^^^^
     *
     * @var string
     */
    private $sigMaskPrefixFull = null;

    /**
     * http://example.com/some/a_*_/path?abc
     *                   ^^^^^^^^
     *
     * @var string
     */
    private $sigMaskPrefix = null;

    /**
     * http://example.com/some/a_*_/path?abc
     *                            ^^^^^^^^^^
     *
     * @var string
     */
    private $sigMaskSuffix = null;

    /**
     * Create new ImageResizer object.
     *
     * @param string $secret
     * @param $urlMask
     * @throws Exception
     */
    public function __construct($secret, $urlMask)
    {
        $this->sigSecret = $secret;
        $this->sigMask = $urlMask;
        list ($this->sigMaskPrefixFull, $this->sigMaskPrefix, , $this->sigMaskSuffix) = $this->splitUrl($this->sigMask);
    }

    /**
     * Builds URL with data is token mixed in.
     *
     * @param array $params
     * @return string
     */
    public function buildUrl(array $params)
    {
        $token = http_build_query($params);
        if (function_exists('gzdeflate')) {
            $deflated = gzdeflate($token);
            if ($this->sigStrlen($deflated) < $this->sigStrlen($token)) {
                $token = "z" . $deflated;
            } else {
                $token = "a" . $token;
            }
        } else {
            $token = "a" . $token;
        }
        $token = base64_encode($token);
        $token = str_replace($this->sigSafeBase64[0], $this->sigSafeBase64[1], $token);
        $token = join("/", str_split($token, 80));
        // Add digital signature to the end of the PACKED result. We cannot insert
        // the signature before packing, because else a hacked may create another
        // pack which unpacks to the same result. Add signatures at the beginning,
        // because we need an easy way to explode() them back.
        $token =
            $this->_hash($this->sigSecret . $this->sigMaskPrefixFull . $token . $this->sigMaskSuffix) .
            "/" .
            $this->_hash($this->sigSecret . $this->sigMaskPrefix . $token . $this->sigMaskSuffix) .
            "/" .
            $token;
        return str_replace("*", $token, $this->sigMask);
    }

    /**
     * Parses passed URL and return extracted data items.
     *
     * @param string $url
     * @return array
     * @throws Exception
     */
    public function parseUrl($url)
    {
        list ($prefixFull, $prefix, $token, $suffix) = $this->splitUrl($url);
        @list ($signFull, $sign, $token) = explode("/", $token, 3);
        if ($this->sigMaskPrefixFull === $this->sigMaskPrefix) {
            // Base URL is relative => compare relative signatures only.
            $ok = $this->_hash($this->sigSecret . $prefix . $token . $suffix) === $sign;
        } elseif ($prefixFull !== $prefix) {
            // Checked URL is absolute => compare absolute signatures [we know that base URL is absolute].
            $ok = $this->_hash($this->sigSecret . $prefixFull . $token . $suffix) === $signFull;
        } else {
            // Checked URL is relative [we know that base URL is absolute].
            $ok = $this->_hash($this->sigSecret . $prefix . $token . $suffix) === $sign;
        }
        if (!$ok) {
            throw new Exception("Wrong digital signature");
        }
        $token = str_replace('/', '', $token);
        $token = str_replace($this->sigSafeBase64[1], $this->sigSafeBase64[0], $token);
        $token = @base64_decode($token);
        if (!$token) {
            throw new Exception("Invalid URL token encoding");
        }
        if (@$token[0] == "z") {
            $token = gzinflate($this->sigSubstr($token, 1));
        } else {
            $token = $this->sigSubstr($token, 1);
        }
        $params = null;
        parse_str($token, $params);
        return $params;
    }

    /**
     * @param $url
     * @return mixed
     */
    protected function getUriByUrl($url)
    {
        $m = null;
        if (preg_match('{^(?:\w+:)?//[^/?]+(.*)$}s', $url, $m)) {
            return $m[1];
        }
        return $url;
    }

    /**
     * @param $url
     * @return array
     * @throws Exception
     */
    protected function splitUrl($url)
    {
        if ($this->sigMaskPrefix === null) {
            $m = null;
            if (!preg_match('/^(.*)\*(.*)$/', $this->sigMask, $m)) {
                throw new Exception("URL mask must contain \"*\", {$this->sigMask} given");
            }
            $this->sigMaskPrefixFull = $m[1];
            $this->sigMaskPrefix = $this->getUriByUrl($this->sigMaskPrefixFull);
            $this->sigMaskSuffix = $m[2];
        }
        // ^(http://example.com(/some/a_)) (*) (_/path?abc)$
        if ($this->sigMaskPrefix === $this->sigMaskPrefixFull) {
            $url = $this->getUriByUrl($url);
        }
        $hasProtocol = preg_match('{^(\w+:)?//}s', $url);
        $re = '^(' .
            ($hasProtocol
                ? preg_quote(
                    $this->sigSubstr($this->sigMaskPrefixFull, 0, -$this->sigStrlen($this->sigMaskPrefix)), '/'
                )
                : "") .
            "(" . preg_quote($this->sigMaskPrefix, '/') . ")" .
            ")(.+)(" . preg_quote($this->sigMaskSuffix, '/') . ')$';
        if (!preg_match("/$re/s", $url, $m)) {
            throw new Exception("URL does not match the mask \"{$this->sigMask}\"");
        }
        return array($m[1], $m[2], $m[3], $m[4]);
    }

    /**
     * @param $data
     * @return string
     */
    protected function _hash($data)
    {
        return md5($this->sigSecret . $data);
    }

    /**
     * @param $s
     * @return int
     */
    private function sigStrlen($s)
    {
        return function_exists('mb_orig_strlen')? mb_orig_strlen($s) : strlen($s);
    }

    /**
     * @param $s
     * @param $from
     * @param null $len
     * @return false|string
     */
    private function sigSubstr($s, $from, $len = null)
    {
        if ($len !== null) {
            return function_exists('mb_orig_substr')
                ? mb_orig_substr($s, $from, $len)
                : substr($s, $from, $len);
        } else {
            return function_exists('mb_orig_substr')
                ? mb_orig_substr($s, $from)
                : substr($s, $from);
        }
    }
}
