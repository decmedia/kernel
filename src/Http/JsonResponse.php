<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Http;

use Phalcon\Http\ResponseInterface;
use Decmedia\Kernel\Contracts\Http\Response as ResponseContract;
use function date;
use function json_decode;
use function sha1;

class JsonResponse extends Response implements ResponseContract
{
    /**
     * {@inheritdoc}
     */
    public function send(): ResponseInterface
    {
        $content   = $this->getContent();
        $timestamp = date('c');
        $hash      = sha1($timestamp . $content);
        $eTag      = sha1($content ?? '');

        /** @var array $content */
        $content = !empty($content) ? json_decode($this->getContent(), true) : [];
        $jsonapi = [
            'jsonapi' => [
                'version' => '1.0',
            ],
        ];
        $meta    = [
            'meta' => [
                'timestamp' => $timestamp,
                'hash'      => $hash,
            ]
        ];

        if (!is_array($content)) {
            $this
                ->setStatusCode(200)
                ->setContentType('text/html', 'UTF-8')
            ;
            return parent::send();
        }

        $data = $jsonapi + $content + $meta;
        $this
            ->setHeader('E-Tag', $eTag)
            ->setJsonContent($data);

        return parent::send();
    }

    /**
     * {@inheritdoc}
     */
    public function setPayloadError(string $detail = '', int $appStatusCode = 500): JsonResponse
    {
        if (true !== $this->getDI()->getShared('config')->path('app.devMode')) {
            $detail = 'Exception';
        }
        if (array_key_exists($appStatusCode, $this->codes)) {
            $this->setStatusCode($appStatusCode);
        }
        $this->setJsonContent(['errors' => [$detail], 'code' => $appStatusCode]);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setPayloadSuccess($content = []): JsonResponse
    {
        $data = (true === is_array($content)) ? $content : ['data' => $content];
        $data = (true === isset($data['data'])) ? $data  : ['data' => $data];

        $this->setJsonContent($data);

        return $this;
    }
}
