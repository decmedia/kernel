<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Http;

use Decmedia\Kernel\Contracts\Http\Response as ResponseContract;
use Phalcon\Http\Response as PhResponse;
use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\View\Simple;
use function Decmedia\Kernel\app;

class Response extends PhResponse implements ResponseContract
{
    const OK                    = 200;
    const CREATED               = 201;
    const ACCEPTED              = 202;
    const MOVED_PERMANENTLY     = 301;
    const FOUND                 = 302;
    const TEMPORARY_REDIRECT    = 307;
    const PERMANENTLY_REDIRECT  = 308;
    const BAD_REQUEST           = 400;
    const UNAUTHORIZED          = 401;
    const FORBIDDEN             = 403;
    const NOT_FOUND             = 404;
    const UNPROCESSABLE_ENTITY  = 422;
    const INTERNAL_SERVER_ERROR = 500;
    const NOT_IMPLEMENTED       = 501;
    const BAD_GATEWAY           = 502;
    const UNAVAILABLE           = 503;

    protected $codes = [
        200 => 'OK',
        301 => 'Moved Permanently',
        302 => 'Found',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found',
        422 => 'Unprocessable Entity',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
    ];

    /**
     * Returns the http code description or if not found the code itself
     * @param int $code
     *
     * @return int|string
     */
    public function getHttpCodeDescription(int $code)
    {
        if (true === isset($this->codes[$code])) {
            return sprintf('%d (%s)', $code, $this->codes[$code]);
        }

        return $code;
    }

    /**
     * {@inheritdoc}
     */
    public function send(): ResponseInterface
    {
        $this->setContentType('text/html', 'UTF-8');

        return parent::send();
    }

    /**
     * {@inheritdoc}
     */
    public function setPayloadError(string $detail = '', int $appStatusCode = self::INTERNAL_SERVER_ERROR): Response
    {
        /** @var Simple $view */
        $view = $this->getDI()->getShared('view');

        $view->setViewsDir(realpath(__DIR__ . '/../Foundation/Exceptions'));

        $this->setContent($view->render('/views/500.phtml', [
            'code' => $appStatusCode,
            'message' => app('env') === 'development' ? $detail : '',
            'mess' => array_key_exists($appStatusCode, $this->codes) ? $this->codes[$appStatusCode] : 'Error',
        ]));

        $this->setStatusCode(array_key_exists($appStatusCode, $this->codes)
            ? $appStatusCode
            : self::INTERNAL_SERVER_ERROR);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setPayloadSuccess($content = []): Response
    {
        $data = (true === is_array($content)) ? $content : ['data' => $content];
        $data = (true === isset($data['data'])) ? $data  : ['data' => $data];

        $this->setContent('Response - setPayloadSuccess');

        return $this;
    }
}
