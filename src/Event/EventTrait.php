<?php

namespace Decmedia\Kernel\Event;

use Phalcon\Events\Event as phEvent;
use Decmedia\Kernel\Support\Facades\Event;
use function Decmedia\Kernel\broadcast;
use function Decmedia\Kernel\event;

trait EventTrait
{
    private $events = [];

    /**
     * @param mixed $event
     */
    public function registerEvent($event)
    {
        $this->events[] = $event;
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function releaseEvents()
    {
        /** @var phEvent $event */
        foreach ($this->events as $event) {
            if ($event instanceof phEvent) {
                Event::fire($event->getType(), $this, $event->getData());
            } else {
                Event::dispatch($event);
            }
        }

        $this->events = [];
    }

    /**
     * Dispatch the event with the given arguments.
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function dispatch()
    {
        return event(new static(...func_get_args()));
    }

    /**
     * Dispatch the event with the given arguments if the given truth test passes.
     *
     * @param bool $boolean
     * @return array|null
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function dispatchIf($boolean, ...$arguments)
    {
        if ($boolean) {
            return event(new static(...$arguments));
        }
    }

    /**
     * Dispatch the event with the given arguments unless the given truth test passes.
     *
     * @param bool $boolean
     * @return array|null
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function dispatchUnless($boolean, ...$arguments)
    {
        if (! $boolean) {
            return event(new static(...$arguments));
        }
    }

    /**
     * Broadcast the event with the given arguments.
     *
     * @return \Illuminate\Broadcasting\PendingBroadcast
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function broadcast()
    {
        // TODO broadcast
        return broadcast(new static(...func_get_args()));
    }
}
