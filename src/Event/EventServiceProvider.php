<?php

namespace Decmedia\Kernel\Event;

use Decmedia\Kernel\Contracts\Queue\Factory as QueueFactoryContract;
// use Illuminate\Contracts\Queue\Factory as QueueFactoryContract;
use Decmedia\Kernel\Support\ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('events', function ($app) {
            return (new Dispatcher($app))->setQueueResolver(function () use ($app) {
                return $app->make(QueueFactoryContract::class);
            });
        });
    }
}
