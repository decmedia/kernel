<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Event;

use DateTimeImmutable;
use Decmedia\Kernel\Support\Contracts\Event\ApplicationEvent;

/**
 * Class UserRenamed
 * @package Decmedia\Kernel\Event
 */
class UserRenamed implements ApplicationEvent
{
    /** @var DateTimeImmutable  */
    private $occurredOn;

    /** @var int */
    private $userID;

    /** @var string */
    private $name;

    public function __construct(int $userID, string $name)
    {
        $this->userID = $userID;
        $this->name = $name;
        $this->occurredOn = new DateTimeImmutable();
    }

    public function userId(): int
    {
        return $this->userID;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function occurredOn(): DateTimeImmutable
    {
        return $this->occurredOn;
    }
}
