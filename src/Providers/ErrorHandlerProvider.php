<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers;

use Monolog\Logger;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Decmedia\Kernel\Contracts\Foundation\Application;
use Decmedia\Kernel\Contracts\Config\Repository;
use Decmedia\Kernel\Support\ErrorHandler;
use function register_shutdown_function;
use function set_error_handler;

class ErrorHandlerProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}
     *
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        /** @var Logger $logger */
        $logger  = $container->getShared('logger');
        /** @var Repository $registry */
        $config  = $container->getShared('config');
        /** @var Application $app */
        $app  = $container->getShared('application');

        date_default_timezone_set($config->get('app.timezone'));
        ini_set('display_errors', 'Off');
        error_reporting(E_ALL);

        $handler = new ErrorHandler($app, $logger, $config);

        set_error_handler([$handler, 'handle']);

        set_exception_handler([$handler, 'handleException']);

        register_shutdown_function([$handler, 'shutdown']);
    }
}
