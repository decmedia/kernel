<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers;

use League\Tactician\CommandBus;
use Decmedia\Kernel\CommandBus\CommandHandlerMiddleware;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Decmedia\Kernel\CommandBus\EventCommandMapping;

/**
 * A small, flexible command bus. The term is mostly used when we combine the Command pattern with a service layer.
 * Its job is to take a Command object
 * (which describes what the user wants to do) and match it to a Handler (which executes it).
 * This can help structure your code neatly.
 *
 * Class CommandBusProvider
 * @package Decmedia\Kernel\Providers
 * @see https://github.com/thephpleague/tactician
 *
 * @example $dto = new MyCommand();
 * @example Command::handle($dto)
 * Command bus найдет в DI класс, имя которого будет совпадать с MyCommandHandler
 * Префикс задается в настройках шины CommandBus
 *
 * Что может:
 * Откладывать задачи по времени (https://github.com/ConnectHolland/tactician-scheduler-plugin)
 * Блокировать другие задачи пока текущая выполняется
 * Проксировать задачи на другой сервис (http)
 * Помещать задачи в очередь (https://bernard.readthedocs.io/index.html)
 */
class CommandBusProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        $container->setShared(
            'command',
            function () use ($container) {
                return new CommandBus(
                    new CommandHandlerMiddleware(
                        $container,
                        new EventCommandMapping()
                    )
                );
            }
        );
    }
}
