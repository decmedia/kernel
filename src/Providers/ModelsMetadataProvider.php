<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon API.
 *
 * (c) Phalcon Team <team@phalcon.io>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Decmedia\Kernel\Providers;

use Phalcon\Cache\AdapterFactory;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Storage\SerializerFactory;

class ModelsMetadataProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        $container->setShared(
            'modelsMetadata',
            function () use ($container) {

                /** @var \Decmedia\Kernel\Contracts\Config\Repository $config */
                $config = $container->getShared('config');

                $driver = $config->get('cache.default');
                $options = $config->get('cache.options.' . $driver);

                $serializerFactory = new SerializerFactory();
                $adapterFactory    = new AdapterFactory($serializerFactory);

                $metaDataDriverName = '\Phalcon\Mvc\Model\MetaData\\' . ucfirst(strtolower($driver));

                return new $metaDataDriverName($adapterFactory, $options);
            }
        );
    }
}
