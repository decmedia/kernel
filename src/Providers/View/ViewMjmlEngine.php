<?php

namespace Decmedia\Kernel\Providers\View;

use Decmedia\Kernel\Config\Repository;
use Decmedia\Kernel\Contracts\Cache\Store;
use Decmedia\Kernel\Contracts\Render\Renderable as RenderContract;
use Decmedia\Kernel\Providers\View\Mjml\RenderManager;
use Decmedia\Kernel\Support\Constants\Modes;
use Phalcon\Mvc\Model\ResultsetInterface;
use Phalcon\Mvc\View\Engine\AbstractEngine;
use Phalcon\Mvc\View\Engine\Volt;
use function Decmedia\Kernel\app_path;

/**
 * Class ViewMjmlEngine
 * @package Decmedia\Kernel\Providers\View
 *
 * @property Repository $config
 * @property Store $cache
 */
final class ViewMjmlEngine extends AbstractEngine
{
    /** @var RenderContract $render */
    protected $render;

    /**
     * @param string $path
     * @param array $params
     * @param bool $mustClean
     *
     * @return string
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function render(string $path, $params = array(), bool $mustClean = false)
    {
        $volt = new Volt($this->view, $this->getDI());
        $compiledPath = $this->config->get('app.env') == Modes::TEST
            ? app_path('../tests/_cache/')
            : $this->config->get('view.compiled');

        $options = [
            'path'      => $compiledPath,
            'extension' => \Decmedia\Kernel\env('VOLT_COMPILED_EXTENSION', '.php'),
            'separator' => \Decmedia\Kernel\env('VOLT_COMPILED_SEPARATOR', '_'),
            'always'    => $this->config->get('app.devMode'),
        ];

        $volt->setOptions($options);

        ob_start();
        $volt->render($path, $params, false);
        $resultRender = ob_get_clean();

        $cacheKey = sha1(sprintf('%s-%s.cache', md5_file($path), json_encode($params)));
        if ($this->config->get('app.env') !== 'testing'
            && false === $this->config->get('app.devMode')
            && true === $this->cache->has($cacheKey)
        ) {
            /** @var ResultsetInterface $data */
            $content = $this->cache->get($cacheKey);
        } else {
            $content = $this->render->parse(
                $resultRender
            );

            $this->cache->set($cacheKey, $content);
        }

        if ($mustClean) {
            $this->view->setContent($content);
        } else {
            echo $content;
        }
    }

    /**
     * @param RenderManager $render
     */
    public function setRenderer($render)
    {
        $this->render = $render;
    }
}
