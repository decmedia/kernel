<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers\View;

use Decmedia\Kernel\Providers\View\Markdown\MarkdownRender;
use Decmedia\Kernel\Providers\View\Mjml\RenderManager;
use Phalcon\Mvc\View\Engine\Volt;
use Phalcon\Mvc\View\Simple as View;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Di\DiInterface;
use Decmedia\Kernel\Support\Constants\Modes;
use Phalcon\Mvc\ViewBaseInterface;
use function Decmedia\Kernel\app_path;

class ViewSimpleProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        /** @var \Decmedia\Kernel\Contracts\Config\Repository $config */
        $config = $container->getShared('config');



        $container->setShared(
            'view',
            function () use ($config, $container) {
                $view = new View();
                $view->setViewsDir($config->get('view.path'));

                $view->registerEngines(
                    [
                        '.phtml'  => function ($view) use ($config, $container) {
                            $volt = new Volt($view, $container);
                            $path = $config->get('app.env') == Modes::TEST
                                ? app_path('../tests/_cache/')
                                : $config->get('view.compiled');

                            $options = [
                                'path'      => $path,
                                'extension' => \Decmedia\Kernel\env('VOLT_COMPILED_EXTENSION', '.php'),
                                'separator' => \Decmedia\Kernel\env('VOLT_COMPILED_SEPARATOR', '_'),
                                'always'    => $config->get('app.devMode'),
                            ];

                            $volt->setOptions($options);

                            $volt->getCompiler()
                                ->addFunction('strtotime', 'strtotime')
                                ->addFunction('sprintf', 'sprintf')
                                ->addFunction('str_replace', 'str_replace')
                                ->addFunction('count', 'count')
                                ->addFunction('is_a', 'is_a')
                            ;

                            return $volt;
                        },

                        '.md'  => function ($view) use ($container) {
                            $renderer = $container->has('markdown')
                                ? $container->getShared('markdown')
                                : new MarkdownRender()
                            ;

                            $engine = new ViewMarkdownEngine($view, $container);
                            $engine->setRenderer($renderer);

                            return $engine;
                        },

                        '.mjml'  => function ($view) use ($container) {
                            $renderer = $container->has('mjml')
                                ? $container->getShared('mjml')
                                : new RenderManager()
                            ;

                            $engine = new ViewMjmlEngine($view, $container);
                            $engine->setRenderer($renderer);

                            return $engine;
                        }
                    ]
                );

                return $view;
            }
        );
    }
}
