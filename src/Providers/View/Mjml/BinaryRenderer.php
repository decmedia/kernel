<?php

namespace Decmedia\Kernel\Providers\View\Mjml;

use Decmedia\Kernel\Contracts\Render\Renderable;
use Decmedia\Kernel\Exception\RuntimeException;
use Phalcon\Di\Injectable;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Simple as SimpleView;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Class BinaryRenderer
 * @package Decmedia\Kernel\Providers\View\Mjml
 *
 * @property View|SimpleView $view
 */
class BinaryRenderer extends Injectable implements Renderable
{
    /**
     * The MJML CLI path.
     *
     * @var string
     */
    private $bin;

    /**
     * BinaryRenderer constructor.
     *
     * @param string $bin
     */
    public function __construct(string $bin)
    {
        $this->bin = $bin;
    }

    /**
     * @param string $view
     * @param array $data
     * @return string
     * @throws RuntimeException
     */
    public function render(string $view, array $data = []): string
    {
        $content = $this->view->render($view, $data);

        return $this->parse($content);
    }

    /**
     * @param string $content
     * @return string
     * @throws RuntimeException
     */
    public function parse(string $content): string
    {
        $arguments = [
            $this->bin,
            '-i',
            '-s',
            '--config.validationLevel',
            '--config.minify'
        ];

        $process = new Process($arguments);
        $process->setInput($content);

        try {
            $process->mustRun();
        } catch (ProcessFailedException $e) {
            throw new RuntimeException('Unable to compile MJML. Stack error: '.$e->getMessage());
        }

        return $process->getOutput();
    }
}
