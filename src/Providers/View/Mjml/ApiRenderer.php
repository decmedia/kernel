<?php

namespace Decmedia\Kernel\Providers\View\Mjml;

use Decmedia\Kernel\Contracts\Render\Renderable;
use GuzzleHttp\Client;
use Phalcon\Di\Injectable;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Simple as SimpleView;

/**
 * Class ApiRenderer
 * @package Decmedia\Kernel\Providers\View\Mjml
 *
 * @property View|SimpleView $view
 */
class ApiRenderer extends Injectable implements Renderable
{
    /**
     * The client
     *
     * @var Client
     */
    private $client;

    /**
     * The application ID
     *
     * @var string
     */
    private $appId;

    /**
     * The secret key
     *
     * @var string
     */
    private $secretKey;

    /**
     * The MJML base uri.
     *
     * @var string
     */
    const BASE_URI = "https://api.mjml.io/v1/";

    /**
     * ApiRenderer constructor.
     *
     * @param string $appId
     * @param string $secretKey
     */
    public function __construct(string $appId, string $secretKey)
    {
        $this->appId = $appId;
        $this->secretKey = $secretKey;
        $this->client = new Client(['base_uri' => self::BASE_URI]);
    }

    /**
     * @param string $view
     * @param array $data
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function render(string $view, array $data = []): string
    {
        $content = $this->view->render($view, $data);

        return $this->parse($content);
    }

    /**
     * @param string $content
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function parse(string $content): string
    {
        $response = $this->client->post('render', [
            'auth' => [$this->appId, $this->secretKey],
            'body' => json_encode([
                'mjml' => $content
            ])
        ]);

        if (false === $data =  json_decode($response->getBody()->getContents(), true)) {
            throw new \RuntimeException('Unable to decode response');
        }

        return $data['html'];
    }
}
