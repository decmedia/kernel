<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers\View\Mjml;

use Decmedia\Kernel\Contracts\Render\Renderable;
use Decmedia\Kernel\Support\Manager\Manager;

class RenderManager extends Manager
{
    /**
     * Create an instance of the Bcrypt hash Driver.
     *
     * @return Renderable
     */
    public function createApiDriver()
    {
        $config = $this->config->get('services.mjml', []);

        return new ApiRenderer($config['client_id'], $config['client_secret']);
    }

    /**
     * Create an instance of the Argon2i hash Driver.
     *
     * @return Renderable
     */
    public function createBinaryDriver()
    {
        return new BinaryRenderer($this->config->get('services.mjml.binary_file'));
    }

    /**
     * Get information about the given hashed value.
     *
     * @param string $content
     * @return string
     */
    public function parse($content): string
    {
        return $this->driver()->parse($content);
    }

    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return $this->config->get('view.mjml', 'binary');
    }
}
