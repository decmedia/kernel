<?php

namespace Decmedia\Kernel\Providers\View;

use Phalcon\Mvc\View\Engine\AbstractEngine;
use Decmedia\Kernel\Contracts\Render\MarkdownRender as MarkdownRenderContract;

/**
 * Class ViewMarkdownEngine
 * @package Decmedia\Kernel\Providers\View
 */
final class ViewMarkdownEngine extends AbstractEngine
{
    /** @var MarkdownRenderContract $render */
    protected $render;

    /**
     * @param string $path
     * @param array $params
     * @param bool $mustClean
     *
     * @return string
     */
    public function render(string $path, $params = array(), bool $mustClean = false)
    {
        $content = $this->render->parse(
            file_get_contents($path)
        );

        if ($mustClean) {
            $this->view->setContent($content);
        } else {
            echo $content;
        }
    }

    /**
     * @param MarkdownRenderContract $render
     */
    public function setRenderer(MarkdownRenderContract $render)
    {
        $this->render = $render;
    }
}
