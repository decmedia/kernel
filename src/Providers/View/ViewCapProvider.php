<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers\View;

use Phalcon\Mvc\View;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Di\DiInterface;

class ViewCapProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        $container->setShared('view', new View());
    }
}
