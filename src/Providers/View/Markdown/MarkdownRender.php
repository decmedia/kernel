<?php

namespace Decmedia\Kernel\Providers\View\Markdown;

use League\CommonMark\CommonMarkConverter;
use League\CommonMark\Environment;
use League\CommonMark\Extension\Table\TableExtension;
use Phalcon\Di\Injectable;
use Phalcon\Mvc\View\Simple as SimpleView;
use Phalcon\Mvc\View;
use \Decmedia\Kernel\Contracts\Render\MarkdownRender as MarkdownRenderContract;

/**
 * Class Markdown
 * @package Decmedia\Kernel\Support
 *
 * @property View|SimpleView $view
 *
 * @link https://github.com/thephpleague/commonmark
 * @link https://commonmark.thephpleague.com/
 */
class MarkdownRender extends Injectable implements MarkdownRenderContract
{
    /**
     * Render the Markdown template into HTML.
     *
     * @param string $view
     * @param array $data
     *
     * @return string
     */
    public function render(string $view, array $data = []): string
    {
        $content = $this->view->render($view, $data);

        return $this->parse($content);
    }

    /**
     * Render the Markdown template into text.
     *
     * @param string $view
     * @param array $data
     * @return string
     */
    public function renderText(string $view, array $data = []): string
    {
        $contents = $this->view->render($view, $data);

        return html_entity_decode(
            preg_replace("/[\r\n]{2,}/", "\n\n", $contents),
            ENT_QUOTES,
            'UTF-8'
        );
    }

    /**
     * Parse the given Markdown text into HTML.
     *
     * @param string $text
     *
     * @return string
     */
    public function parse(string $text): string
    {
        $environment = Environment::createCommonMarkEnvironment();

        $environment->addExtension(new TableExtension);

        $converter = new CommonMarkConverter([
            'allow_unsafe_links' => false,
        ], $environment);

        return $converter->convertToHtml($text);
    }
}
