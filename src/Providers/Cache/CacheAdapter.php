<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers\Cache;

use DateInterval;
use DateTimeInterface;
use Phalcon\Cache;
use Decmedia\Kernel\Contracts\Event\Dispatcher;
use Decmedia\Kernel\Providers\Cache\Events\CacheHit;
use Decmedia\Kernel\Providers\Cache\Events\CacheMissed;
use Decmedia\Kernel\Contracts\Cache\Store;
use Decmedia\Kernel\Providers\Cache\Events\KeyForgotten;
use Decmedia\Kernel\Providers\Cache\Events\KeyWritten;
use Decmedia\Kernel\Support\Carbon;

class CacheAdapter implements Store
{
    /** @var Cache $cache */
    protected $cache;

    /**
     * The event dispatcher implementation.
     *
     * @var \Decmedia\Kernel\Contracts\Event\Dispatcher
     */
    protected $events;

    /**
     * The default number of seconds to store items.
     *
     * @var int|null
     */
    protected $default = 3600;

    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param null $ttl
     * @return bool
     * @throws Cache\Exception\InvalidArgumentException
     */
    public function set($key, $value, $ttl = null): bool
    {
        return $this->cache->set($this->itemKey($key), $value, $ttl);
    }

    /**
     * @param string $key
     * @param null $defaultValue
     * @return mixed|void
     * @throws Cache\Exception\InvalidArgumentException
     */
    public function get($key, $defaultValue = null)
    {
        return $this->cache->get($this->itemKey($key), $defaultValue);
    }

    /**
     * Retrieve multiple items from the cache by key.
     *
     * Items not found in the cache will have a null value.
     *
     * @param array $keys
     * @return array
     * @throws Cache\Exception\InvalidArgumentException
     */
    public function many(array $keys)
    {
        $values = $this->storeMany(collect($keys)->map(function ($value, $key) {
            return is_string($key) ? $key : $value;
        })->values()->all());

        return collect($values)->map(function ($value, $key) use ($keys) {
            return $this->handleManyResult($keys, $key, $value);
        })->all();
    }

    /**
     * Store an item in the cache.
     *
     * @param string $key
     * @param mixed $value
     * @param \DateTimeInterface|\DateInterval|int|null $ttl
     * @return bool
     * @throws Cache\Exception\InvalidArgumentException
     */
    public function put($key, $value, $ttl = null)
    {
        if (is_array($key)) {
            return $this->putMany($key, $value);
        }

        if ($ttl === null) {
            return $this->forever($key, $value);
        }

        $seconds = $this->getSeconds($ttl);

        if ($seconds <= 0) {
            return $this->forget($key);
        }

        $result = $this->set($key, $value, $seconds);

        if ($result) {
            $this->event(new KeyWritten($key, $value, $seconds));
        }

        return $result;
    }

    /**
     * Store multiple items in the cache for a given number of seconds.
     *
     * @param array $values
     * @param \DateTimeInterface|\DateInterval|int|null $ttl
     * @return bool
     * @throws Cache\Exception\InvalidArgumentException
     */
    public function putMany(array $values, $ttl = null)
    {
        if ($ttl === null) {
            return $this->putManyForever($values);
        }

        $seconds = $this->getSeconds($ttl);

        if ($seconds <= 0) {
            return $this->deleteMultiple(array_keys($values));
        }

        // TODO чистить ключи через $this->>itemKey для setMultiple
        $result = $this->setMultiple($values, $seconds);

        if ($result) {
            foreach ($values as $key => $value) {
                $this->event(new KeyWritten($key, $value, $seconds));
            }
        }

        return $result;
    }

    /**
     * @param string $key
     * @param int $value
     *
     * @return bool|\Illuminate\Support\HigherOrderTapProxy|int|mixed|\Decmedia\Kernel\Support\HigherOrderTapProxy
     *
     * @throws Cache\Exception\InvalidArgumentException
     */
    public function increment($key, $value = 1)
    {
        // TODO в phalcon баг с инкрементом, периодически проверять на его исправление (не инкрементит)
        // TODO phalcon 4.0.5 Call to undefined method Phalcon\Cache::increment() !
        // return $this->cache->increment($key, $value);

        if (! is_null($existing = $this->get($key))) {
            $value = ((int) $existing) + $value;
        }

        $this->forever($key, $value);

        return $value;
    }

    /**
     * @param string $key
     * @param int $value
     * @return bool|int
     * @throws Cache\Exception\InvalidArgumentException
     */
    public function decrement($key, $value = 1)
    {
        // return parent::decrement($key, $value);
        return $this->increment($key, $value * -1);
    }

    /**
     * Get the cache key prefix.
     *
     * @return string
     */
    public function getPrefix(): string
    {
        // TODO phalcon 4.0.5 getPrefix пропала также !
        return $this->cache->getPrefix();
    }

    /**
     * Remove an item from the cache.
     *
     * @param string $key
     * @return bool
     * @throws Cache\Exception\InvalidArgumentException
     */
    public function forget($key)
    {
        return tap($this->delete($this->itemKey($key)), function ($result) use ($key) {
            if ($result) {
                $this->event(new KeyForgotten($key));
            }
        });
    }

    /**
     * Remove all items from the cache.
     *
     * @return bool
     */
    public function flush()
    {
        return $this->clear();
    }

    /**
     * Store an item in the cache indefinitely.
     *
     * @param string $key
     * @param mixed $value
     * @return bool
     * @throws Cache\Exception\InvalidArgumentException
     */
    public function forever($key, $value)
    {
        // $result = $this->set($key, $value, 0);
        // TODO phalcon bug: ttl 0 - lifetime = 0 sec.
        $result = $this->set($key, $value, 31536000);

        if ($result) {
            $this->event(new KeyWritten($key, $value));
        }

        return $result;
    }

    /**
     * Set the event dispatcher instance.
     *
     * @param  \Decmedia\Kernel\Contracts\Event\Dispatcher  $events
     * @return void
     */
    public function setEventDispatcher(Dispatcher $events)
    {
        $this->events = $events;
    }

    /**
     * Flushes/clears the cache
     */
    public function clear(): bool
    {
        // Custom implementation
    }

    /**
     * Deletes data from the adapter
     * @param string $key
     * @return bool
     * @throws Cache\Exception\InvalidArgumentException
     */
    public function delete(string $key): bool
    {
        return $this->cache->delete($key);
    }

    /**
     * Returns the already connected adapter or connects to the backend
     * server(s)
     */
    public function getAdapter()
    {
        return $this->cache->getAdapter();
    }

    /**
     * Returns all the keys stored. If a filter has been passed the keys that
     * match the filter will be returned
     */
    public function getKeys(string $prefix = ""): array
    {
        return $this->cache->getKeys($prefix);
    }

    /**
     * Checks if an element exists in the cache
     * @throws Cache\Exception\InvalidArgumentException
     */
    public function has(string $key): bool
    {
        return $this->cache->has($key);
    }

    /**
     * Store multiple items in the cache indefinitely.
     *
     * @param array $values
     * @return bool
     * @throws Cache\Exception\InvalidArgumentException
     */
    protected function putManyForever(array $values)
    {
        $result = true;

        foreach ($values as $key => $value) {
            if (! $this->forever($key, $value)) {
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Calculate the number of seconds for the given TTL.
     *
     * @param  \DateTimeInterface|\DateInterval|int  $ttl
     * @return int
     */
    protected function getSeconds($ttl)
    {
        $duration = $this->parseDateInterval($ttl);

        if ($duration instanceof DateTimeInterface) {
            $duration = Carbon::now()->diffInRealSeconds($duration, false);
        }

        return (int) $duration > 0 ? $duration : 0;
    }

    /**
     * If the given value is an interval, convert it to a DateTime instance.
     *
     * @param  \DateTimeInterface|\DateInterval|int  $delay
     * @return \DateTimeInterface|int
     */
    protected function parseDateInterval($delay)
    {
        if ($delay instanceof DateInterval) {
            $delay = Carbon::now()->add($delay);
        }

        return $delay;
    }

    /**
     * Get the current system time as a UNIX timestamp.
     *
     * @return int
     */
    protected function currentTime()
    {
        return Carbon::now()->getTimestamp();
    }

    /**
     * Handle a result for the "many" method.
     *
     * @param  array  $keys
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    protected function handleManyResult($keys, $key, $value)
    {
        // If we could not find the cache value, we will fire the missed event and get
        // the default value for this cache value. This default could be a callback
        // so we will execute the value function which will resolve it if needed.
        if (is_null($value)) {
            $this->event(new CacheMissed($key));

            return isset($keys[$key]) ? value($keys[$key]) : null;
        }

        // If we found a valid value we will fire the "hit" event and return the value
        // back from this function. The "hit" event gives developers an opportunity
        // to listen for every possible cache "hit" throughout this applications.
        $this->event(new CacheHit($key, $value));

        return $value;
    }

    /**
     * Fire an event for this cache instance.
     *
     * @param  string  $event
     * @return void
     */
    protected function event($event)
    {
        if (isset($this->events)) {
            $this->events->dispatch($event);
        }
    }

    /**
     * Format the key for a cache item.
     * Queue laravel ставит двоеточие в имени ключей, а phalcon их не принимает
     *
     * @param  string  $key
     * @return string
     */
    protected function itemKey($key): string
    {
        return (string) str_replace([':'], '-', $key);
    }

    /**
     * Retrieve multiple items from the cache by key.
     *
     * Items not found in the cache will have a null value.
     *
     * @param array $keys
     * @return array
     * @throws Cache\Exception\InvalidArgumentException
     */
    protected function storeMany(array $keys)
    {
        $return = [];

        foreach ($keys as $key) {
            $return[$key] = $this->get($key);
        }

        return $return;
    }

    /**
     * Store multiple items in the cache for a given number of seconds.
     *
     * @param array $values
     * @param int $seconds
     * @return bool
     * @throws Cache\Exception\InvalidArgumentException
     */
    protected function storePutMany(array $values, $seconds)
    {
        $manyResult = null;

        foreach ($values as $key => $value) {
            $result = $this->set($key, $value, $seconds);

            $manyResult = is_null($manyResult) ? $result : $result && $manyResult;
        }

        return $manyResult ?: false;
    }
}
