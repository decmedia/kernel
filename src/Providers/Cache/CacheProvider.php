<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers\Cache;

use Phalcon\Cache;
use Phalcon\Cache\AdapterFactory;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Storage\SerializerFactory;
use Webmozart\Assert\Assert;
use Decmedia\Kernel\Contracts\Foundation\Application;
use Decmedia\Kernel\Contracts\Event\Dispatcher as DispatcherContract;

class CacheProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        $container->setShared(
            'cache',
            function () use ($container) {

                /** @var \Decmedia\Kernel\Contracts\Config\Repository $config */
                $config = $container->getShared('config');

                $driver = $config->get('cache.default');
                $options = $config->get('cache.options.' . $driver);

                Assert::notNull(
                    $options,
                    sprintf(
                        'Driver [%s] not configured.
                        Add settings to file /config/cache.php in section [options] [%s] provider',
                        $driver,
                        $driver
                    )
                );

                $serializerFactory = new SerializerFactory();
                $adapterFactory    = new AdapterFactory($serializerFactory);

                $adapter = $adapterFactory->newInstance($driver, $options);

                /** @var Application $app */
                $app = $container->getShared('application');

                return tap(new CacheAdapter(new Cache($adapter)), function ($repository) use ($app) {
                    if (! $app->bound(DispatcherContract::class)) {
                        return;
                    }

                    $repository->setEventDispatcher(
                        $app[DispatcherContract::class]
                    );

                    $app->instance('cache', $repository);
                    $app->instance('cache.store', $repository);
                });
            }
        );
    }
}
