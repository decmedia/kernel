<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Events\Manager;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\Collection;
use Decmedia\Kernel\App\Api\Controllers\Dev\DevController;
use Decmedia\Kernel\App\Actions\StrToLower;

class RouterProvider implements ServiceProviderInterface
{
    /**
     * The application's global HTTP middleware stack.
     * @var array
     */
    protected $middleware = [];

    /**
     * API Routes
     * @var array
     */
    protected $routes = [];

    /**
     * {@inheritdoc}
     *
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        /** @var Micro $infrastructure */
        $infrastructure = $container->getShared('infrastructure');
        /** @var Manager $eventsManager */
        $eventsManager = $container->getShared('events');

        $this->attachRoutes($infrastructure);
        $this->attachMiddleware($infrastructure, $eventsManager);

        $infrastructure->setEventsManager($eventsManager);
    }

    /**
     * Attaches the middleware to the application
     *
     * @param Micro   $application
     * @param Manager $eventsManager
     */
    private function attachMiddleware(Micro $application, Manager $eventsManager)
    {
        $middleware = $this->getMiddleware();

        /**
         * Get the events manager and attach the middleware to it
         */
        foreach ($middleware as $class => $function) {
            $eventsManager->attach('micro', new $class());
            $application->{$function}(new $class());
        }
    }

    /**
     * Attaches the routes to the application; lazy loaded
     *
     * @param Micro $application
     */
    private function attachRoutes(Micro $application)
    {
        $routes = $this->getRoutes();

        foreach ($routes as $route) {
            $collection = new Collection();
            $collection
                ->setHandler($route[0], true)
                ->setPrefix($route[1])
                ->{$route[2]}($route[3], $route[4] ?? 'callAction');

            $application->mount($collection);
        }
    }

    /**
     * Returns the array for the middleware with the action to attach
     *
     * @return array
     */
    private function getMiddleware(): array
    {
        /*
        |--------------------------------------------------------------------------
        | Kernel Middleware
        |--------------------------------------------------------------------------
        |
        | The application's global HTTP middleware stack.
        | These middleware are run during every request to your application.
        |
        */
        $middleware = [

        ];

        return array_merge(
            $middleware,
            $this->middleware
        );
    }

    /**
     * Returns the array for the routes
     *
     * @return array
     */
    private function getRoutes(): array
    {
        /*
        |--------------------------------------------------------------------------
        | API Routes
        |--------------------------------------------------------------------------
        |
        | Here is where you can register API routes for your application. These
        | routes are loaded by the RouteServiceProvider within a group which
        | is assigned the "api" middleware group.
        |
        */
        $routes = [

        ];

        return array_merge(
            $routes,
            $this->routes
        );
    }
}
