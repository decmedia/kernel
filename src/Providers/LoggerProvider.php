<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon API.
 *
 * (c) Phalcon Team <team@phalcon.io>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Decmedia\Kernel\Providers;

use Decmedia\Kernel\Log\LogManager;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Psr\Log\LoggerInterface;
use function Decmedia\Kernel\resolve;

class LoggerProvider implements ServiceProviderInterface
{
    /**
     * Registers the logger component
     *
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        $container->setShared(
            'logger',
            function () use ($container) {
                return resolve(LogManager::class);
            }
        );
    }
}
