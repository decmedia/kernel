<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers;

use Illuminate\Container\Container;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Di\DiInterface;

/**
 * Class ResolverProvider
 * @package Decmedia\Kernel\Providers
 *
 * Автозагрузчик классов и зависмостей от Laravel
 * @link https://laravel.com/docs/8.x/container
 */
class ResolverProvider implements ServiceProviderInterface
{
    public function register(DiInterface $di): void
    {
        $di->setShared(
            'resolver',
            function () use ($di) {
                $container = new Container;

                // set container Bindings, Tagging, Resolving

                return $container;
            }
        );
    }
}
