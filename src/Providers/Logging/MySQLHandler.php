<?php

namespace Decmedia\Kernel\Providers\Logging;

use Monolog\Logger;
use Monolog\Handler\AbstractProcessingHandler;
use Decmedia\Kernel\Exception\ModelException;
use Decmedia\Kernel\Mvc\Model\AbstractModel;

/**
 * This class is a handler for Monolog, which can be used
 * to write records in a MySQL table
 *
 * Class MySQLHandler
 */
class MySQLHandler extends AbstractProcessingHandler
{
    /** @var string */
    private $table;

    /**
     * MySQLHandler constructor.
     * @param AbstractModel $table
     * @param int $level
     * @param bool $bubble
     */
    public function __construct(
        AbstractModel $table,
        $level = Logger::DEBUG,
        $bubble = true
    ) {
        $this->table = $table;
        parent::__construct($level, $bubble);
    }

    /**
     * Writes the record down to the log of the implementing handler
     *
     * @param  $record []
     * @return void
     * @throws ModelException
     */
    protected function write(array $record): void
    {
        $context = $record['context'];
        $result = $this->table
            ->set('message', $record['message'])
            ->set('userID', $context['userID'] ?? 0)
            ->set('type', $context['type'] ?? '')
            ->set('typeID', $context['typeID'] ?? '')
            ->save();

        if (false === $result) {
            throw new ModelException($this->table->getModelMessages());
        }
    }
}
