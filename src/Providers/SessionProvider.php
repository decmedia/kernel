<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Session\Manager;
use Phalcon\Session\Adapter\Stream;
use function Decmedia\Kernel\app_path;

class SessionProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        $container->setShared(
            'session',
            function () {
                $session = new Manager();
                $files = new Stream(
                    [
                        'savePath' => app_path('storage/session'),
                    ]
                );

                $session
                    ->setAdapter($files)
                    ->start();

                return $session;
            }
        );
    }
}
