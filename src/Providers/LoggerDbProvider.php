<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers;

use Monolog\Logger;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Decmedia\Kernel\Models\Log;
use Decmedia\Kernel\Providers\Logging\MySQLHandler;

/**
 * Class LoggerDbProvider
 * @package Decmedia\Kernel\Providers
 */
class LoggerDbProvider implements ServiceProviderInterface
{

    /**
     * Registers the logger component
     *
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        $container->setShared(
            'loggerDb',
            function () {
                $handler = new MySQLHandler(new Log(), Logger::DEBUG);
                $logger = new Logger('db-logger');
                $logger->pushHandler($handler);

                return $logger;
            }
        );
    }
}
