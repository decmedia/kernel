<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Di\DiInterface;
use Phalcon\Translate\InterpolatorFactory;
use Phalcon\Translate\TranslateFactory;
use function Decmedia\Kernel\app_path;

class LocaleProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        /** @var \Decmedia\Kernel\Contracts\Config\Repository $config */
        $config = $container->getShared('config');
        $container->set('translator', function () use ($config, $container) {

            $locale = $config->get('app.locale');
            $fallbackLocale = $config->get('app.fallback_locale');

            $translationFile = app_path('resource/message/' . $locale . '.php');

            if (true !== file_exists($translationFile)) {
                $translationFile = app_path('resource/message/' . $fallbackLocale . '.php');
            }

            $messages = require $translationFile;

            $interpolator = new InterpolatorFactory();
            $factory      = new TranslateFactory($interpolator);

            return $factory->newInstance(
                'array',
                [
                    'content' => $messages,
                ]
            );
        });
    }
}
