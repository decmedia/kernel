<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers;

use Phalcon\Registry;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Di\DiInterface;

class RegistryProvider implements ServiceProviderInterface
{
    public function register(DiInterface $container)
    {
        $container->setShared(
            'registry',
            function () {
                return new Registry();
            }
        );
    }
}
