<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon API.
 *
 * (c) Phalcon Team <team@phalcon.io>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Decmedia\Kernel\Providers;

use Decmedia\Kernel\Http\JsonResponse;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Di\DiInterface;

class JsonResponseProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        $container->setShared(
            'response',
            function () use ($container) {
                $response = new JsonResponse();

                /**
                 * Assume success. We will work with the edge cases in the code
                 */
                $response
                    ->setStatusCode(200)
                    ->setContentType('application/vnd.api+json', 'UTF-8')
                ;

                return $response;
            }
        );
    }
}
