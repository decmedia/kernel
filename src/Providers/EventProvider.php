<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Decmedia\Kernel\Contracts\Event\Dispatcher;
use function Decmedia\Kernel\resolve;

/**
 * The purpose of this component is to intercept the execution of components in the framework by creating hooks.
 * These hooks allow developers to obtain status information, manipulate data or change the flow of execution during
 * the process of a component.
 * The component consists of a Phalcon\Events\Manager that handles event propagation and execution of events.
 * The manager contains various Phalcon\Events\Event objects, which contain information about each hook/event.
 *
 * Class EventProvider
 * @package Decmedia\Kernel\Providers
 * @see https://docs.phalcon.io/4.0/ru-ru/events
 */
class EventProvider implements ServiceProviderInterface
{
    /**
     * API Routes
     * @var array
     */
    protected $subscribe = [];

    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        $container->setShared(
            'events',
            function () {
                return resolve(Dispatcher::class);
            }
        );
    }
}
