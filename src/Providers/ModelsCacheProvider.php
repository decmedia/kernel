<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon API.
 *
 * (c) Phalcon Team <team@phalcon.io>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Decmedia\Kernel\Providers;

use Phalcon\Cache;
use Phalcon\Cache\AdapterFactory;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Storage\SerializerFactory;
use Webmozart\Assert\Assert;

class ModelsCacheProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        $container->setShared(
            'modelsCache',
            function () use ($container) {

                /** @var \Decmedia\Kernel\Contracts\Config\Repository $config */
                $config = $container->getShared('config');

                $driver = $config->get('cache.default');
                $options = $config->get('cache.options.' . $driver);

                Assert::notNull(
                    $options,
                    sprintf(
                        'Driver [%s] not configured.
                        Add settings to file /config/cache.php in section [options] [%s] provider',
                        $driver,
                        $driver
                    )
                );

                $serializerFactory = new SerializerFactory();
                $adapterFactory    = new AdapterFactory($serializerFactory);

                $adapter = $adapterFactory->newInstance($driver, $options);

                return new Cache($adapter);
            }
        );
    }


}
