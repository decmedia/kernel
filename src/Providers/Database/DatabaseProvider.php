<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers\Database;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Decmedia\Kernel\Contracts\Foundation\Application;
use Decmedia\Kernel\Providers\Config\Repository;

class DatabaseProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        $container->setShared(
            'db',
            function () use ($container) {
                /** @var Application $app */
                $app = $container->getShared('application');

                /** @var Repository $config */
                $config = $container->getShared('config');

                $adapter = $config->get('database.default');
                $options = $config->get('database.connections.' . $adapter);


                // The database manager is used to resolve various connections, since multiple
                // connections might be managed. It also implements the connection resolver
                // interface which may be used by other components requiring connections.
                $app->instance('db', $connection = new DatabaseAdapter($options));

                $connection->execute('SET NAMES utf8mb4', []);

                return $connection;
            }
        );
    }
}
