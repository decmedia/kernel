<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers\Database;

use Closure;
use Phalcon\Db\Adapter\Pdo\Mysql;
use Throwable;

class DatabaseAdapter extends Mysql
{
    /**
     * Execute a Closure within a transaction.
     *
     * @param  Closure  $callback
     * @param  int  $attempts
     * @return mixed
     *
     * @throws Throwable
     */
    public function transaction(Closure $callback, $attempts = 1)
    {
        for ($currentAttempt = 1; $currentAttempt <= $attempts; $currentAttempt++) {
            $this->begin();

            // We'll simply execute the given callback within a try / catch block and if we
            // catch any exception we can rollback this transaction so that none of this
            // gets actually persisted to a database or stored in a permanent fashion.
            try {
                $callbackResult = $callback($this);
            }

            // If we catch an exception we'll rollback this transaction and try again if we
            // are not out of attempts. If we are out of attempts we will just throw the
            // exception back out and let the developer handle an uncaught exceptions.
            catch (Throwable $e) {
                print_r('transaction:: handleTransactionException');
                print_r($e);
                $this->rollback();
                continue;
            }

            try {
                print_r('transaction:: commit ');
                $this->commit();
            } catch (Throwable $e) {
                print_r('transaction:: handleCommitTransactionException');

                continue;
            }

            return $callbackResult;
        }
    }
}
