<?php

namespace Decmedia\Kernel\Providers;

use Illuminate\Contracts\Support\DeferrableProvider;
use Decmedia\Kernel\Support\ServiceProvider;

class ConfigServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('config', function ($app) {
            print_r($app);
            die;
            return '';
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'config'
        ];
    }
}
