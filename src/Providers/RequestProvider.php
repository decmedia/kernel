<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon API.
 *
 * (c) Phalcon Team <team@phalcon.io>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Decmedia\Kernel\Providers;

use Decmedia\Kernel\Contracts\Foundation\Application;
use Decmedia\Kernel\Http\Request;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Di\DiInterface;

class RequestProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        $container->setShared('request', function () use ($container) {

            /** @var Application $app */
            $app = $container->getShared('application');

            $app->instance('request', $request = new Request());

            return $request;
        });
    }
}
