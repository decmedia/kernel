<?php

namespace Decmedia\Kernel\Providers\Config;

use Exception;
use Decmedia\Kernel\Contracts\Foundation\Application;
use SplFileInfo;
use Symfony\Component\Finder\Finder;

class LoadConfiguration
{
    /** @var \Decmedia\Kernel\Contracts\Foundation\Application $app */
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Bootstrap the given application.
     *
     * @return array
     * @throws Exception
     */
    public function load(): array
    {
        $items = [];

        // First we will see if we have a cache configuration file. If we do, we'll load
        // the configuration items from that file so that it is very quick. Otherwise
        // we will need to spin through every configuration file and load them all.
        if (is_file($cached = $this->app->getCachedConfigPath())) {
            $items = require $cached;

            $loadedFromCache = true;
        }

        if (! isset($loadedFromCache)) {
            $items = $this->loadConfigurationFiles($this->app);
        }

        return $items;
    }

    /**
     * Load the configuration items from all of the files.
     *
     * @param  \Decmedia\Kernel\Contracts\Foundation\Application  $app
     * @return array
     *
     * @throws \Exception
     */
    protected function loadConfigurationFiles(Application $app): array
    {
        $data = [];

        $files = $this->getConfigurationFiles($app);

        if (! isset($files['app'])) {
            throw new Exception('Unable to load the "app" configuration file.');
        }

        foreach ($files as $key => $path) {
            $data[$key] = require $path;
        }

        return $data;
    }

    /**
     * Get all of the configuration files for the application.
     *
     * @param  \Decmedia\Kernel\Contracts\Foundation\Application  $app
     * @return array
     */
    protected function getConfigurationFiles(Application $app): array
    {
        $files = [];

        $configPath = realpath($app->configPath());

        foreach (Finder::create()->files()->name('*.php')->in($configPath) as $file) {
            $directory = $this->getNestedDirectory($file, $configPath);

            $files[$directory.basename($file->getRealPath(), '.php')] = $file->getRealPath();
        }

        ksort($files, SORT_NATURAL);

        return $files;
    }

    /**
     * Get the configuration file nesting path.
     *
     * @param \SplFileInfo $file
     * @param string $configPath
     * @return string
     */
    protected function getNestedDirectory(SplFileInfo $file, string $configPath)
    {
        $directory = $file->getPath();

        if ($nested = trim(str_replace($configPath, '', $directory), DIRECTORY_SEPARATOR)) {
            $nested = str_replace(DIRECTORY_SEPARATOR, '.', $nested).'.';
        }

        return $nested;
    }
}
