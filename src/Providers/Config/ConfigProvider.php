<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon API.
 *
 * (c) Phalcon Team <team@phalcon.io>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Decmedia\Kernel\Providers\Config;

use Decmedia\Kernel\Config\Repository;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Decmedia\Kernel\Contracts\Foundation\Application;

class ConfigProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        $container->setShared(
            'config',
            function () use ($container) {

                /** @var Application $app */
                $app = $container->getShared('application');

                $loadProvider = new LoadConfiguration($app);

                // Next we will spin through all of the configuration files in the configuration
                // directory and load each one into the repository. This will make all of the
                // options available to the developer for use in various parts of this app.
                $app->instance('config', $config = new Repository($loadProvider->load()));

                // Finally, we will set the application's environment based on the configuration
                // values that were loaded. We will pass a callback which will be used to get
                // the environment in a web context where an "--env" switch is not present.
                $app->detectEnvironment(function () use ($config) {
                    return $config->get('app.env', 'production');
                });

                date_default_timezone_set($config->get('app.timezone', 'UTC'));

                mb_internal_encoding('UTF-8');

                return $config;
            }
        );
    }
}
