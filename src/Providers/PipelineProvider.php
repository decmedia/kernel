<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Providers;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Di\DiInterface;
use Decmedia\Kernel\Pipeline\Pipeline;

class PipelineProvider implements ServiceProviderInterface
{
    public function register(DiInterface $container): void
    {
        $container->setShared(
            'pipeline',
            function () use ($container) {
                return new Pipeline($container);
            }
        );
    }
}
