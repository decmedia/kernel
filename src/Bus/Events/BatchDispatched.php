<?php

namespace Decmedia\Kernel\Bus\Events;

use Decmedia\Kernel\Bus\Batch;

class BatchDispatched
{
    /**
     * The batch instance.
     *
     * @var \Decmedia\Kernel\Bus\Batch
     */
    public $batch;

    /**
     * Create a new event instance.
     *
     * @param  \Decmedia\Kernel\Bus\Batch  $batch
     * @return void
     */
    public function __construct(Batch $batch)
    {
        $this->batch = $batch;
    }
}
