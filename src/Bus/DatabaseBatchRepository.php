<?php

namespace Decmedia\Kernel\Bus;

use Carbon\CarbonImmutable;
use Closure;
use \Phalcon\Db\Adapter\Pdo\AbstractPdo as Connection;
use Illuminate\Support\Str;
use Phalcon\Mvc\Model\Query\Builder;
use Decmedia\Kernel\Exception\ModelException;
use Decmedia\Kernel\Models\AbstractModel;

class DatabaseBatchRepository implements BatchRepository
{
    /**
     * The batch factory instance.
     *
     * @var \Decmedia\Kernel\Bus\BatchFactory
     */
    protected $factory;

    /**
     * The database connection instance.
     *
     * @var \Phalcon\Db\Adapter\Pdo\AbstractPdo
     */
    protected $connection;

    /**
     * The database table to use to store batch information.
     *
     * @var string
     */
    protected $table;

    /**
     * Create a new batch repository instance.
     *
     * @param  \Decmedia\Kernel\Bus\BatchFactory  $factory
     * @param  \Phalcon\Db\Adapter\Pdo\AbstractPdo $connection
     * @param  string  $table
     */
    public function __construct(BatchFactory $factory, Connection $connection, string $table)
    {
        $this->factory = $factory;
        $this->connection = $connection;
        $this->table = $table;
    }

    /**
     * Retrieve a list of batches.
     *
     * @param  int  $limit
     * @param  mixed  $before
     * @return \Decmedia\Kernel\Bus\Batch[]
     */
    public function get($limit = 50, $before = null)
    {
        return $this->connection->table($this->table)
                            ->orderByDesc('id')
                            ->take($limit)
                            ->when($before, function ($q) use ($before) {
                                return $q->where('id', '<', $before);
                            })
                            ->get()
                            ->map(function ($batch) {
                                return $this->toBatch($batch);
                            })
                            ->all();
    }

    /**
     * Retrieve information about an existing batch.
     *
     * @param  string  $batchId
     * @return \Decmedia\Kernel\Bus\Batch|null
     */
    public function find(string $batchId)
    {
        $batch = $this
            ->getBuilder()
            ->where('id = :id:', ['id' => $batchId])
            ->getQuery()
            ->getSingleResult()
        ;

        if ($batch) {
            return $this->toBatch($batch);
        }
    }

    /**
     * Store a new pending batch.
     *
     * @param \Decmedia\Kernel\Bus\PendingBatch $batch
     * @return \Decmedia\Kernel\Bus\Batch
     * @throws ModelException
     */
    public function store(PendingBatch $batch)
    {
        $id = (string) Str::orderedUuid();

        /** @var \Decmedia\Kernel\Models\AbstractModel $batchTable $logTable */
        $batchTable = new $this->table();
        $result = $batchTable
            ->set('id', $id)
            ->set('name', $batch->name)
            ->set('total_jobs', 0)
            ->set('pending_jobs', 0)
            ->set('failed_jobs', 0)
            ->set('failed_job_ids', '[]')
            ->set('options', serialize($batch->options))
            ->set('created_at', time())
            ->set('cancelled_at', null)
            ->set('finished_at', null)
            ->save()
        ;

        if (false === $result) {
            throw new ModelException($batchTable->getModelMessages());
        }

        return $this->find($id);
    }

    /**
     * Increment the total number of jobs within the batch.
     *
     * @param string $batchId
     * @param int $amount
     * @return void
     * @throws ModelException
     */
    public function incrementTotalJobs(string $batchId, int $amount)
    {
        /** @var AbstractModel $batch */
        $batch = $this->getBatchByID($batchId);

        $batch->total_jobs = $amount;
        $batch->pending_jobs = $amount;
        $batch->finished_at = null;

        if (false === $batch->update()) {
            throw new ModelException($batch->getModelMessages());
        }
    }

    /**
     * Decrement the total number of pending jobs for the batch.
     *
     * @param string $batchId
     * @param string $jobId
     * @return \Decmedia\Kernel\Bus\UpdatedBatchJobCounts
     * @throws \Throwable
     */
    public function decrementPendingJobs(string $batchId, string $jobId)
    {
        $values = $this->updateAtomicValues($batchId, function ($batch) use ($jobId) {
            return [
                'pending_jobs' => $batch->pending_jobs - 1,
                'failed_jobs' => $batch->failed_jobs,
                'failed_job_ids' => json_encode(array_values(array_diff(json_decode($batch->failed_job_ids, true), [$jobId]))),
            ];
        });

        return new UpdatedBatchJobCounts(
            $values['pending_jobs'],
            $values['failed_jobs']
        );
    }

    /**
     * Increment the total number of failed jobs for the batch.
     *
     * @param string $batchId
     * @param string $jobId
     * @return \Decmedia\Kernel\Bus\UpdatedBatchJobCounts
     * @throws \Throwable
     */
    public function incrementFailedJobs(string $batchId, string $jobId)
    {
        $values = $this->updateAtomicValues($batchId, function ($batch) use ($jobId) {
            return [
                'pending_jobs' => $batch->pending_jobs,
                'failed_jobs' => $batch->failed_jobs + 1,
                'failed_job_ids' => json_encode(array_values(array_unique(array_merge(json_decode($batch->failed_job_ids, true), [$jobId])))),
            ];
        });

        return new UpdatedBatchJobCounts(
            $values['pending_jobs'],
            $values['failed_jobs']
        );
    }

    /**
     * @param string $batchId
     * @return \Phalcon\Mvc\ModelInterface
     */
    protected function getBatchByID(string $batchId)
    {
        return $this
            ->getBuilder()
            ->where('id = :id:', ['id' => $batchId])
            ->getQuery()
            ->getSingleResult()
            ;
    }

    /**
     * Update an atomic value within the batch.
     *
     * @param string $batchId
     * @param \Closure $callback
     * @return int|null
     * @throws \Throwable
     */
    protected function updateAtomicValues(string $batchId, Closure $callback)
    {
        return $this->connection->transaction(function () use ($batchId, $callback) {
            $batch = $this->getBatchByID($batchId);

            return is_null($batch) ? [] : tap($callback($batch), function ($values) use ($batchId) {
                $batch = $this->getBatchByID($batchId);

                foreach ($values as $field => $value) {
                    $batch->$field = $value;

                    if (false === $batch->update()) {
                        throw new ModelException($batch->getModelMessages());
                    }
                }

                if (false === $batch->update()) {
                    throw new ModelException($batch->getModelMessages());
                }
            });
        });
    }

    /**
     * @return \Phalcon\Mvc\Model\Query\BuilderInterface
     */
    protected function getBuilder()
    {
        return (new Builder())->from($this->table);
    }

    /**
     * Mark the batch that has the given ID as finished.
     *
     * @param string $batchId
     * @return void
     * @throws ModelException
     */
    public function markAsFinished(string $batchId)
    {
        $batch = $this->getBatchByID($batchId);
        $batch->finished_at = time();

        if (false === $batch->update()) {
            throw new ModelException($batch->getModelMessages());
        }
    }

    /**
     * Cancel the batch that has the given ID.
     *
     * @param string $batchId
     * @return void
     * @throws ModelException
     */
    public function cancel(string $batchId)
    {
        $batch = $this->getBatchByID($batchId);
        $batch->cancelled_at = time();
        $batch->finished_at = time();

        if (false === $batch->update()) {
            throw new ModelException($batch->getModelMessages());
        }
    }

    /**
     * Delete the batch that has the given ID.
     *
     * @param  string  $batchId
     * @return void
     */
    public function delete(string $batchId)
    {
        $this->getBatchByID($batchId)->delete();
    }

    /**
     * Execute the given Closure within a storage specific transaction.
     *
     * @param \Closure $callback
     * @return mixed
     * @throws \Throwable
     */
    public function transaction(Closure $callback)
    {
        return $this->connection->transaction(function () use ($callback) {
            return $callback();
        });
    }

    /**
     * Convert the given raw batch to a Batch object.
     *
     * @param  object  $batch
     * @return \Decmedia\Kernel\Bus\Batch
     */
    protected function toBatch($batch)
    {
        return $this->factory->make(
            $this,
            $batch->id,
            $batch->name,
            (int) $batch->total_jobs,
            (int) $batch->pending_jobs,
            (int) $batch->failed_jobs,
            json_decode($batch->failed_job_ids, true),
            unserialize($batch->options),
            CarbonImmutable::createFromTimestamp($batch->created_at),
            $batch->cancelled_at ? CarbonImmutable::createFromTimestamp($batch->cancelled_at) : $batch->cancelled_at,
            $batch->finished_at ? CarbonImmutable::createFromTimestamp($batch->finished_at) : $batch->finished_at
        );
    }
}
