<?php

namespace Decmedia\Kernel\Support;

use Decmedia\Kernel\Console\Application as Artisan;

abstract class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register the package's custom Artisan commands.
     *
     * @param  array|mixed  $commands
     * @return void
     */
    public function commands($commands)
    {
        $commands = is_array($commands) ? $commands : func_get_args();

        Artisan::starting(function ($artisan) use ($commands) {
            $artisan->resolveCommands($commands);
        });
    }
}
