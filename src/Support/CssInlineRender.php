<?php

namespace Decmedia\Kernel\Support;

use League\Flysystem\FilesystemException;
use Phalcon\Di\Injectable;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;
use Webmozart\Assert\Assert;
use Decmedia\Kernel\Contracts\Support\InlineRender;
use Decmedia\Kernel\Support\Facades\Storage;

final class CssInlineRender extends Injectable implements InlineRender
{
    /**
     * @var mixed|InlineRender $renderer
     */
    protected $renderer;

    /**
     * Содержимое стилей css для применения в рендере письма
     * @var string $css
     */
    private $css = [];

    /**
     * @param string $style
     * @return $this
     */
    public function addCssRaw(string $style)
    {
        $this->css[] = $style;

        return $this;
    }

    /**
     * @param string|array $path
     *
     * @return $this
     */
    public function addCssFile($path)
    {
        $paths = is_array($path) ? $path : func_get_args();
        foreach ($paths as $path) {
            $style = file_get_contents($path);
            Assert::notFalse($style, sprintf('File %s not found', $path));

            $this->css[] = file_get_contents($path);
        }

        return $this;
    }

    /**
     * @param $path
     *
     * @return $this
     *
     * @throws FilesystemException
     */
    public function addCssFileFromStorage($path)
    {
        $storage = Storage::disk();
        $paths = is_array($path) ? $path : func_get_args();
        foreach ($paths as $path) {
            $this->css[] = $storage->read($path);
        }

        return $this;
    }

    /**
     * @param $html
     * @param null $css
     *
     * @return mixed
     */
    public function convert($html, $css = null)
    {
        return $this->getRenderer()->convert(
            $html,
            $css ?: implode(PHP_EOL, $this->css)
        );
    }

    /**
     * @return mixed|InlineRender
     */
    private function getRenderer()
    {
        return $this->getDI()->has('inliner')
            ? $this->getDI()->getShared('inliner')
            : new CssToInlineStyles
        ;
    }
}
