<?php

namespace Decmedia\Kernel\Support\Facades;

use Symfony\Component\HttpFoundation\StreamedResponse;
use Decmedia\Kernel\Filesystem\FilesystemManager;
use Decmedia\Kernel\Filesystem\FilesystemAdapter;

/**
 * @method static bool fileExists(string $path)
 * @method static void write(string $path, string $contents, array $config = [])
 * @method static void writeStream(string $path, $contents, array $config = [])
 * @method static string read(string $path)
 * @method static string path(string $path)
 * @method static resource readStream(string $path)
 * @method static bool delete(string|array $path)
 * @method static StreamedResponse download(string $path, $name = null, array $headers = [])
 * @method static void deleteDirectory(string $path)
 * @method static void createDirectory(string $path, array $config = [])
 * @method static void setVisibility(string $path, $visibility)
 * @method static int visibility(string $path)
 * @method static string mimeType(string $path)
 * @method static int lastModified(string $path)
 * @method static int fileSize(string $path)
 * @method static iterable listContents(string $path, bool $deep)
 * @method static void move(string $source, string $destination, array $config = [])
 * @method static void copy(string $source, string $destination, array $config = [])
 * @method static FilesystemAdapter disk(string $driver = null)
 *
 * @see FilesystemManager
 * @see FilesystemAdapter
 *
 * @url https://flysystem.thephpleague.com/v2/docs/
 *
 */
class Storage extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'filesystem';
    }
}
