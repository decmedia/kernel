<?php

namespace Decmedia\Kernel\Support\Facades;

use Traversable;

/**
 * @method static mixed|null path(string $path, mixed $defaultValue = null, mixed $delimiter = null)
 * @method static \Phalcon\Config setPathDelimiter(mixed $delimiter = null)
 * @method static string getPathDelimiter()
 * @method static \Phalcon\Config merge(mixed $toMerge)
 * @method static array toArray()
 * @method static mixed get(string $element, $defaultValue = null, string $cast = null)
 * @method static void clear()
 * @method static int count()
 * @method static Traversable getIterator() Returns the iterator of the class
 * @method static array getKeys(bool $insensitive = true)
 * @method static array getValues()
 * @method static bool has(string $element) Get the element from the collection
 * @method static void init(array $data = array()) Initialize internal array
 *
 * Specify data which should be serialized to JSON
 * See [jsonSerialize](https://php.net/manual/en/jsonserializable.jsonserialize.php)
 * @method static array jsonSerialize()
 *
 * Whether a offset exists
 * See [offsetExists](https://php.net/manual/en/arrayaccess.offsetexists.php)
 * @method static bool offsetExists($element)
 *
 * Offset to retrieve
 * See [offsetGet](https://php.net/manual/en/arrayaccess.offsetget.php)
 * @method static mixed offsetGet($element)
 *
 * Offset to set
 * See [offsetSet](https://php.net/manual/en/arrayaccess.offsetset.php)
 * @method static void offsetSet($element, $value)
 *
 * Offset to unset
 * See [offsetUnset](https://php.net/manual/en/arrayaccess.offsetunset.php)
 * @method static void offsetUnset($element)
 * @method static void remove(string $element) Delete the element from the collection
 * @method static void set(string $element, $value) Set an element in the collection
 *
 * String representation of object
 * See [serialize](https://php.net/manual/en/serializable.serialize.php)
 * @method static string serialize()
 *
 * Returns the object in a JSON format
 * The default string uses the following options for json_encode
 * `JSON_HEX_TAG`, `JSON_HEX_APOS`, `JSON_HEX_AMP`, `JSON_HEX_QUOT`,
 * `JSON_UNESCAPED_SLASHES`
 * See [rfc4627](https://www.ietf.org/rfc/rfc4627.txt)
 * @method static string toJson(int $options = 79)
 *
 * Constructs the object
 * See [unserialize](https://php.net/manual/en/serializable.unserialize.php)
 * @method static void unserialize($serialized)
 * @method static void setData($element, $value) Sets the collection data
 *
 * @see \Phalcon\Config
 */
class Config extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'config';
    }
}
