<?php

namespace Decmedia\Kernel\Support\Facades;

/**
 * @method static \GuzzleHttp\Promise\PromiseInterface response($body = null, $status = 200, $headers = [])
 * @method static \Decmedia\Kernel\Http\Client\Factory fake($callback = null)
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest accept(string $contentType)
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest acceptJson()
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest asForm()
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest asJson()
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest asMultipart()
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest attach(string $name, string $contents, string|null $filename = null, array $headers = [])
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest baseUrl(string $url)
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest beforeSending(callable $callback)
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest bodyFormat(string $format)
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest contentType(string $contentType)
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest retry(int $times, int $sleep = 0)
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest stub(callable $callback)
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest timeout(int $seconds)
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest withBasicAuth(string $username, string $password)
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest withBody(resource|string $content, string $contentType)
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest withCookies(array $cookies, string $domain)
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest withDigestAuth(string $username, string $password)
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest withHeaders(array $headers)
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest withOptions(array $options)
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest withToken(string $token, string $type = 'Bearer')
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest withoutRedirecting()
 * @method static \Decmedia\Kernel\Http\Client\PendingRequest withoutVerifying()
 * @method static \Decmedia\Kernel\Http\Client\Response delete(string $url, array $data = [])
 * @method static \Decmedia\Kernel\Http\Client\Response get(string $url, array $query = [])
 * @method static \Decmedia\Kernel\Http\Client\Response head(string $url, array $query = [])
 * @method static \Decmedia\Kernel\Http\Client\Response patch(string $url, array $data = [])
 * @method static \Decmedia\Kernel\Http\Client\Response post(string $url, array $data = [])
 * @method static \Decmedia\Kernel\Http\Client\Response put(string $url, array $data = [])
 * @method static \Decmedia\Kernel\Http\Client\Response send(string $method, string $url, array $options = [])
 * @method static \Decmedia\Kernel\Http\Client\ResponseSequence fakeSequence(string $urlPattern = '*')
 *
 * @see \Decmedia\Kernel\Http\Client\Factory
 * @example https://laravel.com/docs/8.x/http-client
 */
class Http extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'http';
    }
}
