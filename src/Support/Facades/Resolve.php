<?php

namespace Decmedia\Kernel\Support\Facades;

/**
 * @method static mixed thenReturn()
 *
 * Автозагрузчик классов и зависмостей от Laravel
 * @link https://laravel.com/docs/8.x/container
 */
class Resolve extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'resolver';
    }
}
