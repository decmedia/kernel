<?php

namespace Decmedia\Kernel\Support\Facades;

use Decmedia\Kernel\Throttle\Rate;
use Decmedia\Kernel\Throttle\Status;

/**
 * @method static void limit(string $identifier, Rate $rate)
 * @method static Status limitSilently(string $identifier, Rate $rate)
 */
class RateLimiter extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'rate.limiter';
    }
}
