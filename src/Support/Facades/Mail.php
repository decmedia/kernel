<?php

namespace Decmedia\Kernel\Support\Facades;

use Decmedia\Kernel\Notification\Mail\Mailer;
use Decmedia\Kernel\Notification\Mail\PendingMail;

/**
 * @method static Mailer mailer(string|null $name = null)
 * @method static PendingMail bcc($users)
 * @method static PendingMail to($users)
 * @method static \Illuminate\Support\Collection queued(string $mailable, \Closure|string $callback = null)
 * @method static \Illuminate\Support\Collection sent(string $mailable, \Closure|string $callback = null)
 * @method static array failures()
 * @method static bool hasQueued(string $mailable)
 * @method static bool hasSent(string $mailable)
 * @method static mixed later(\DateTimeInterface|\DateInterval|int $delay, \Decmedia\Kernel\Contracts\Mail\Mailable|string|array $view, string $queue = null)
 * @method static mixed queue(\Decmedia\Kernel\Contracts\Mail\Mailable|string|array $view, string $queue = null)
 * @method static void assertNotQueued(string $mailable, callable $callback = null)
 * @method static void assertNotSent(string $mailable, callable|int $callback = null)
 * @method static void assertNothingQueued()
 * @method static void assertNothingSent()
 * @method static void assertQueued(string $mailable, callable|int $callback = null)
 * @method static void assertSent(string $mailable, callable|int $callback = null)
 * @method static void raw(string $text, $callback)
 * @method static void send(\Illuminate\Contracts\Mail\Mailable|string|array $view, array $data = [], \Closure|string $callback = null)
 */
class Mail extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'mail.manager';
    }
}
