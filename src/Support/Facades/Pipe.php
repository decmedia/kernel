<?php

namespace Decmedia\Kernel\Support\Facades;

use \Decmedia\Kernel\Pipeline\Pipeline as YtPipeline;

/**
 * @method static YtPipeline send(mixed $passable)
 * @method static YtPipeline through(array|mixed $pipes)
 * @method static YtPipeline via(string $method)
 * @method static mixed then(\Closure $destination)
 * @method static mixed thenReturn()
 *
 * @see \Decmedia\Kernel\Pipeline\Pipeline
 */
class Pipe extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'pipeline';
    }
}
