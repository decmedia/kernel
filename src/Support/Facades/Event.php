<?php

namespace Decmedia\Kernel\Support\Facades;

use Phalcon\Events\ManagerInterface;

/**
 *
 * @example Event::fire('custom:custom', $this, $data);
 * Подписки
 * По имени класса. В слушателе будет вызван метод handle, если он отсутствует, то __invoke
 * По интерфейсу, который будет реализовать класс испускаемого события
 *
 * @example Event::listen(CustomEventClass::class, new CustomEventHandler(...$constructorParams));
 * @example Event::listen(CustomEventClass::class, CustomEventHandler::class);
 * @example Event::listen(CustomEventInterface::class, CustomEventHandler::class);
 * @example Event::listen(CustomEventClass::class, Closure);
 *
 * Вызов события
 * Классы команд, реализующие Decmedia\Kernel\CommandBus\Command запускаются на выполнение с CommandBus
 * обработчик будет найден самостоятельно
 *
 * @example Event::dispatch($command)
 * @example Event::dispatch(new TestCommand(...$constructorParams))
 *
 * @method static void attach(string $eventType, $handler)
 * @method static void detach(string $eventType, $handler)
 * @method static void detachAll(string $type = null)
 * @method static mixed fire(string $eventType, $source, $data = null, bool $cancelable = true)
 * @method static array getListeners(string $type)
 * @method static bool hasListeners(string $type)
 * @method static void listen($events, $listener = null, int $priority = null) Register an event listener with the dispatcher
 * @method static void subscribe($subscriber) Register an event subscriber with the dispatcher.
 * @method static array|null until($event, $payload = []) Dispatch an event until the first non-null response is returned
 * @method static array|null dispatch($event, $payload = [], $halt = false) Dispatch an event and call the listeners.
 * @method static void push($event, $payload = []) Register an event and payload to be fired later.
 * @method static void flush($event) Flush a set of pushed events.
 * @method static void forget($event) Remove a set of listeners from the dispatcher.
 * @method static void forgetPushed() Forget all of the queued listeners.
 *
 * Class Event
 * @package Decmedia\Kernel\Facades
 */
class Event extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'events';
    }
}
