<?php

namespace Decmedia\Kernel\Support\Facades;

use League\Tactician\CommandBus;

/**
 * @method static CommandBus handle(object $command)
 * @see CommandBusProvider
 */
class Command extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'command';
    }
}
