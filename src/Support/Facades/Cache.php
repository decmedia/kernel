<?php

namespace Decmedia\Kernel\Support\Facades;

/**
 * @method static mixed get($key, $default = null) Fetches a value from the cache.
 * @method static bool set($key, $value, $ttl = null) Persists data in the cache, uniquely referenced by a key with an
 * optional expiration TTL time.
 * @method static bool delete($key) Delete an item from the cache by its unique key.
 * @method static bool clear() Wipes clean the entire cache's keys.
 * @method static mixed getMultiple($keys, $default = null) Obtains multiple cache items by their unique keys.
 * @method static bool setMultiple($values, $ttl = null) Persists a set of key => value pairs in the cache
 * @method static bool deleteMultiple($keys) Deletes multiple cache items in a single operation.
 * @method static bool has($key) Determines whether an item is present in the cache.
 * @method static Cache store($name)
 * @method static mixed getAdapter() returns the connected adapter. This offers more flexibility to the developer,
 * since it can be used to execute additional methods that each adapter offers.
 *
 * Class Event
 * @package Decmedia\Kernel\Facades
 *
 * @see https://docs.phalcon.io/4.0/en/cache
 * @see CacheManager
 */
class Cache extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'cache';
    }
}
