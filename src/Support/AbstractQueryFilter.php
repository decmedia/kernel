<?php

namespace Decmedia\Kernel\Support;

use Phalcon\Di\Injectable;
use Phalcon\Mvc\Model\Query\Builder;
use Phalcon\Mvc\Model\Query\BuilderInterface;

abstract class AbstractQueryFilter extends Injectable
{
    /** @var BuilderInterface */
    protected $builder;

    /** @var array */
    protected $bindParams = [];

    /**
     * QueryFilter constructor.
     * @param string $model
     */
    public function __construct(string $model)
    {
        $this->builder = (new Builder())->addFrom($model, 'm');
    }

    /**
     * @param int $limit
     * @return BuilderInterface
     */
    public function limit(int $limit): BuilderInterface
    {
        $this->builder->limit($limit);
        return $this->builder;
    }

    /**
     * @param int $offset
     * @return BuilderInterface
     */
    public function offset(int $offset): BuilderInterface
    {
        $this->builder->offset($offset);
        return $this->builder;
    }

    /**
     * @param int $page
     * @return BuilderInterface
     */
    public function page(int $page): BuilderInterface
    {
        $this->offset($this->builder->getLimit() * $page);
        return $this->builder;
    }

    /**
     * @param array $request
     * @return BuilderInterface
     */
    public function apply(array $request)
    {
        foreach ($request as $filter => $value) {
            if (method_exists($this, $filter)) {
                $this->$filter($value);
            }
        }

        return $this->builder;
    }

    /**
     * @return BuilderInterface
     */
    public function getBuilder()
    {
        return $this->builder;
    }

    /**
     * @param string $conditions
     * @param array $bind
     * @return BuilderInterface
     */
    protected function where(string $conditions, array $bind = [])
    {
        if (!$this->builder->getWhere()) {
            $this->builder->where($conditions, $bind);
        } else {
            $this->builder->andWhere($conditions, $bind);
        }

        return $this->builder;
    }
}
