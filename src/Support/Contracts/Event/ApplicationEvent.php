<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Support\Contracts\Event;

interface ApplicationEvent
{
    public function occurredOn(): \DateTimeImmutable;
}
