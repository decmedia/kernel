<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon API.
 *
 * (c) Phalcon Team <team@phalcon.io>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Decmedia\Kernel\Support\Constants;

class Modes
{
    const DEVELOPMENT   = 'development';
    const PRODUCTION    = 'production';
    const TEST          = 'testing';
    const STAGING       = 'staging';
}
