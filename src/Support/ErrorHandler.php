<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Support;

use ErrorException;
use Exception;
use Monolog\Logger;
use Phalcon\Cli\Console;
use Phalcon\Di\Injectable;
use Phalcon\Mvc\Micro;
use Symfony\Component\Console\Output\ConsoleOutput;
use Throwable;
use Decmedia\Kernel\Contracts\Debug\ExceptionHandler;
use Decmedia\Kernel\Contracts\Foundation\Application;
use Decmedia\Kernel\Contracts\Config\Repository;
use Decmedia\Kernel\Traits\ResponseTrait;
use function memory_get_usage;
use function microtime;
use function number_format;

/**
 * Class ErrorHandler
 * @package Decmedia\Kernel\Support
 *
 * @property Micro|Console $infrastructure
 */
class ErrorHandler extends Injectable
{
    use ResponseTrait;

    /** @var \Phalcon\Di\DiInterface $container */
    protected $container;

    /** @var \Monolog\Logger $logger */
    protected $logger;

    /** @var \Decmedia\Kernel\Contracts\Config\Repository $config */
    protected $config;

    /**
     * The application instance.
     *
     * @var \Decmedia\Kernel\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * Reserved memory so that errors can be displayed properly on memory exhaustion.
     *
     * @var string
     */
    public static $reservedMemory;

    public function __construct(Application $app, Logger $logger, Repository $config)
    {
        $this->app = $app;
        $this->logger = $logger;
        $this->config = $config;

        // TODO
        // self::$reservedMemory = str_repeat('x', 10240);
    }

    /**
     * Convert PHP errors to ErrorException instances.
     *
     * @param  int  $level
     * @param  string  $message
     * @param  string  $file
     * @param  int  $line
     * @param  array  $context
     * @return void
     *
     * @throws \ErrorException
     */
    public function handleError($level, $message, $file = '', $line = 0, $context = [])
    {
        if (error_reporting() & $level) {
            throw new ErrorException($message, 0, $level, $file, $line);
        }
    }

    /**
     * Handles errors by logging them
     *
     * @param int    $number
     * @param string $message
     * @param string $file
     * @param int    $line
     */
    public function handle(int $number, string $message, string $file = '', int $line = 0)
    {
        $this
            ->logger
            ->error(
                sprintf(
                    '[#:%s]-[L: %s] %s (%s)',
                    $number,
                    $line,
                    $message,
                    $file
                )
            );
    }

    /**
     * Application shutdown - logs metrics in devMode
     * Checks for a fatal error, work around for set_error_handler not working on fatal errors.
     */
    public function shutdown()
    {
        $error = error_get_last();

        if (is_array($error)
            && array_key_exists('type', $error)
            && $error["type"] == E_ERROR
        ) {
            $this->handle(0, $error["message"], $error["file"], $error["line"]);
        }

        if (true === $this->config->get('app.devMode')) {
            $memory    = number_format(memory_get_usage() / 1000000, 2);
            $execution = number_format(
                microtime(true) - APP_START,
                4
            );

            $this
                ->logger
                ->info(
                    sprintf(
                        'Shutdown completed [%s]s - [%s]MB',
                        $execution,
                        $memory
                    )
                );
        }
    }

    /**
     * Handle an uncaught exception from the application.
     *
     * Note: Most exceptions can be handled via the try / catch block in
     * the HTTP and Console kernels. But, fatal error exceptions must
     * be handled differently since they are not normal exceptions.
     *
     * @param \Throwable $e
     * @return void
     * @throws Throwable
     */
    public function handleException(Throwable $e)
    {
        try {
            self::$reservedMemory = null;

            $this->getExceptionHandler()->report($e);
        } catch (Exception $e) {
            //
        }

        if ($this->app->runningInConsole()) {
            $this->renderForConsole($e);
        } else {
            $this->renderHttpResponse($e);
        }
    }

    /**
     * Render an exception to the console.
     *
     * @param \Throwable $e
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function renderForConsole(Throwable $e)
    {
        $this->getExceptionHandler()->renderForConsole(new ConsoleOutput, $e);
    }

    /**
     * Render an exception as an HTTP response and send it.
     *
     * @param \Throwable $e
     * @return void
     * @throws Throwable
     */
    protected function renderHttpResponse(Throwable $e)
    {
        $this->getExceptionHandler()->render($this->app['request'], $e)->send();
    }

    /**
     * Get an instance of the exception handler.
     *
     * @return \Illuminate\Contracts\Debug\ExceptionHandler
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function getExceptionHandler()
    {
        return $this->app->make(ExceptionHandler::class);
    }
}
