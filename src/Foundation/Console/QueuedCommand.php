<?php

namespace Decmedia\Kernel\Foundation\Console;

use Decmedia\Kernel\Bus\Queueable;
use Decmedia\Kernel\Contracts\Console\Kernel as KernelContract;
use Decmedia\Kernel\Contracts\Queue\ShouldQueue;
use Decmedia\Kernel\Foundation\Bus\Dispatchable;

class QueuedCommand implements ShouldQueue
{
    // TODO
    use Dispatchable, Queueable;

    /**
     * The data to pass to the Artisan command.
     *
     * @var array
     */
    protected $data;

    /**
     * Create a new job instance.
     *
     * @param  array  $data
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Handle the job.
     *
     * @param  \Decmedia\Kernel\Contracts\Console\Kernel  $kernel
     * @return void
     */
    public function handle(KernelContract $kernel)
    {
        call_user_func_array([$kernel, 'call'], $this->data);
    }
}
