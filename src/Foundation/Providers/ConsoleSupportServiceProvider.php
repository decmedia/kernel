<?php

namespace Decmedia\Kernel\Foundation\Providers;

use Decmedia\Kernel\Contracts\Support\DeferrableProvider;
use Decmedia\Kernel\Support\AggregateServiceProvider;

class ConsoleSupportServiceProvider extends AggregateServiceProvider implements DeferrableProvider
{
    /**
     * The provider class names.
     *
     * @var array
     */
    protected $providers = [
        ArtisanServiceProvider::class,
        // MigrationServiceProvider::class,
        // ComposerServiceProvider::class,
    ];
}
