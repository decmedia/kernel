<?php

namespace Decmedia\Kernel\Foundation\Exceptions;

use Decmedia\Kernel\Support\Facades\View;
use function Decmedia\Kernel\collect;
use function Decmedia\Kernel\config;

class RegisterErrorViewPaths
{
    /**
     * Register the error view paths.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __invoke()
    {
        View::replaceNamespace('errors', collect(config('view.paths'))->map(function ($path) {
            return "{$path}/errors";
        })->push(__DIR__.'/views')->all());
    }
}
