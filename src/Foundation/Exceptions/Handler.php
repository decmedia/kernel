<?php

namespace Decmedia\Kernel\Foundation\Exceptions;

use Closure;
use Decmedia\Kernel\Contracts\Support\Responsable;
use Decmedia\Kernel\Exception\AuthenticationException;
use Decmedia\Kernel\Http\Exceptions\HttpResponseException;
use Exception;
use Illuminate\Contracts\Container\Container;
use Decmedia\Kernel\Foundation\Exceptions\ReportableHandler;
use Illuminate\Support\Arr;
use InvalidArgumentException;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Simple;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Application as ConsoleApplication;
use Decmedia\Kernel\Contracts\Debug\ExceptionHandler;
use Decmedia\Kernel\Traits\ReflectsClosures;
use Decmedia\Kernel\Http\JsonResponse;
use Decmedia\Kernel\Http\Response;
use Throwable;
use function Decmedia\Kernel\app;
use function Decmedia\Kernel\config;
use function Decmedia\Kernel\tap;
use function Decmedia\Kernel\collect;

class Handler implements ExceptionHandler
{
    use ReflectsClosures;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //...
    ];

    /**
     * A list of the internal exception types that should not be reported.
     *
     * @var array
     */
    protected $internalDontReport = [
        \Decmedia\Kernel\Exception\RuntimeException::class
    ];

    /**
     * The container implementation.
     *
     * @var \Illuminate\Contracts\Container\Container
     */
    protected $container;

    /**
     * The callbacks that should be used during reporting.
     *
     * @var array
     */
    protected $reportCallbacks = [];

    /**
     * The callbacks that should be used during rendering.
     *
     * @var array
     */
    protected $renderCallbacks = [];

    /**
     * The registered exception mappings.
     *
     * @var array
     */
    protected $exceptionMap = [];

    /**
     * Create a new exception handler instance.
     *
     * @param  \Illuminate\Contracts\Container\Container  $container
     * @return void
     */
    public function __construct(Container $container)
    {
        $this->container = $container;

        $this->register();
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register a reportable callback.
     *
     * @param  callable  $reportUsing
     * @return \Decmedia\Kernel\Foundation\Exceptions\ReportableHandler
     */
    public function reportable(callable $reportUsing)
    {
        return tap(new ReportableHandler($reportUsing), function ($callback) {
            $this->reportCallbacks[] = $callback;
        });
    }

    /**
     * Register a renderable callback.
     *
     * @param  callable  $renderUsing
     * @return $this
     */
    public function renderable(callable $renderUsing)
    {
        $this->renderCallbacks[] = $renderUsing;

        return $this;
    }

    /**
     * Register a new exception mapping.
     *
     * @param  \Closure|string  $from
     * @param  \Closure|string|null  $to
     * @return $this
     */
    public function map($from, $to = null)
    {
        if (is_string($to)) {
            $to = function ($exception) use ($to) {
                return new $to('', 0, $exception);
            };
        }

        if (is_callable($from) && is_null($to)) {
            $from = $this->firstClosureParameterType($to = $from);
        }

        if (! is_string($from) || ! $to instanceof Closure) {
            throw new InvalidArgumentException('Invalid exception mapping.');
        }

        $this->exceptionMap[$from] = $to;

        return $this;
    }

    /**
     * Indicate that the given exception type should not be reported.
     *
     * @param  string  $class
     * @return $this
     */
    protected function ignore(string $class)
    {
        $this->dontReport[] = $class;

        return $this;
    }

    /**
     * Report or log an exception.
     *
     * @param  \Throwable $e
     * @return void
     *
     * @throws Exception|Throwable
     */
    public function report(Throwable $e)
    {
        $e = $this->mapException($e);

        if ($this->shouldntReport($e)) {
            return;
        }

        if (is_callable($reportCallable = [$e, 'report'])) {
            if ($this->container->call($reportCallable) !== false) {
                return;
            }
        }

        foreach ($this->reportCallbacks as $reportCallback) {
            if ($reportCallback->handles($e)) {
                if ($reportCallback($e) === false) {
                    return;
                }
            }
        }

        try {
            $logger = $this->container->make(LoggerInterface::class);
        } catch (Exception $ex) {
            throw $e;
        }

        $logger->error(
            $e->getMessage(),
            array_merge(
                $this->exceptionContext($e),
                $this->context(),
                ['exception' => $e]
            )
        );
    }

    /**
     * Determine if the exception should be reported.
     *
     * @param  \Throwable  $e
     * @return bool
     */
    public function shouldReport(Throwable $e)
    {
        return ! $this->shouldntReport($e);
    }

    /**
     * Render an exception to the console.
     *
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @param  Throwable  $e
     * @return void
     */
    public function renderForConsole($output, Throwable $e)
    {
        (new ConsoleApplication)->renderThrowable($e, $output);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Decmedia\Kernel\Http\Request $request
     * @param \Throwable $exception
     *
     * @return \Decmedia\Kernel\Http\JsonResponse|\Decmedia\Kernel\Http\Response
     * @throws \Throwable
     */
    public function render($request, Throwable $e)
    {
        if (method_exists($e, 'render') && $response = $e->render($request)) {
            // TODO: render
            die('TODO: render. [Foundation/Exceptions/Handler.php]');
            // return Router::toResponse($request, $response);
        } elseif ($e instanceof Responsable) {
            return $e->toResponse($request);
        }

        $e = $this->mapException($e);

        foreach ($this->renderCallbacks as $renderCallback) {
            if (is_a($e, $this->firstClosureParameterType($renderCallback))) {
                $response = $renderCallback($e, $request);

                if (! is_null($response)) {
                    return $response;
                }
            }
        }

        if ($e instanceof HttpResponseException) {
            return $e->getResponse();
        } elseif ($e instanceof AuthenticationException) {
            return $this->unauthenticated($request, $e);
        }

        return $request->expectsJson()
            ? $this->prepareJsonResponse($request, $e)
            : $this->prepareResponse($request, $e);
    }

    /**
     * Prepare a response for the given exception.
     *
     * @param \Decmedia\Kernel\Http\Request $request
     * @param \Throwable $e
     * @return \Decmedia\Kernel\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function prepareResponse($request, Throwable $e)
    {
        $app = app('infrastructure');

        return tap($app->getDI()->getShared('response'), function ($response) use ($e, $app) {
            $response->setStatusCode($this->isHttpException($e) ? $e->getCode() : 500);

            /** @var \Phalcon\Mvc\View|\Phalcon\Mvc\View\Simple $view */
            $view = $app->getDI()->getShared('view');

            $view->setViewsDir(__DIR__);

            $response->setContent($view->render('/views/500.phtml', [
                'code' => $e->getCode() ? $e->getCode() : 500,
                'message' => app('env') === 'development' ? $e->getMessage() : '',
                'mess' => 'Server Error',
            ]));
        });
    }

    /**
     * Prepare a JSON response for the given exception.
     *
     * @param \Decmedia\Kernel\Http\Request $request
     * @param \Throwable $e
     * @return \Decmedia\Kernel\Http\JsonResponse
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function prepareJsonResponse($request, Throwable $e)
    {
        $app = app('infrastructure');
        return tap($app->getDI()->getShared('response'), function ($response) use ($e) {
            $response->setStatusCode($this->isHttpException($e) ? $e->getCode() : 500);
            $response->setJsonContent([
                'errors' => $this->convertExceptionToArray($e),
                'code' => $e->getCode()
            ]);
        });
    }

    /**
     * Convert the given exception to an array.
     *
     * @param \Throwable $e
     * @return array
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function convertExceptionToArray(Throwable $e)
    {
        return config('app.debug') || config('app.devMode') ? [
            'message' => $e->getMessage(),
            'exception' => get_class($e),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'trace' => collect($e->getTrace())->map(function ($trace) {
                return Arr::except($trace, ['args']);
            })->all(),
        ] : [
            'message' => $this->isHttpException($e) ? $e->getMessage() : 'Server Error',
        ];
    }

    /**
     * Convert an authentication exception into a response.
     *
     * @param  \Decmedia\Kernel\Http\Request  $request
     * @param  \Decmedia\Kernel\Exception\AuthenticationException  $exception
     * @return \Decmedia\Kernel\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $request->expectsJson()
            ? die('return json')
            : die('redirect to login page');
        // TODO
        /*return $request->expectsJson()
            ? response()->json(['message' => $exception->getMessage()], 401)
            : redirect()->guest($exception->redirectTo() ?? route('login'));*/
    }

    /**
     * Map the exception using a registered mapper if possible.
     *
     * @param  \Throwable  $e
     * @return \Throwable
     */
    protected function mapException(Throwable $e)
    {
        foreach ($this->exceptionMap as $class => $mapper) {
            if (is_a($e, $class)) {
                return $mapper($e);
            }
        }

        return $e;
    }

    /**
     * Determine if the exception is in the "do not report" list.
     *
     * @param  \Throwable  $e
     * @return bool
     */
    protected function shouldntReport(Throwable $e)
    {
        $dontReport = array_merge($this->dontReport, $this->internalDontReport);

        return ! is_null(Arr::first($dontReport, function ($type) use ($e) {
            return $e instanceof $type;
        }));
    }

    /**
     * Get the default exception context variables for logging.
     *
     * @param  \Throwable  $e
     * @return array
     */
    protected function exceptionContext(Throwable $e)
    {
        return [];
    }

    /**
     * Get the default context variables for logging.
     *
     * @return array
     */
    protected function context()
    {
        // TODO: добавлять к контекст в лог (параметры что помогут в отладке. Например юзера)
        try {
            return array_filter([
                // 'userId' => Auth::id(),
                // 'email' => optional(Auth::user())->email,
            ]);
        } catch (Throwable $e) {
            return [];
        }
    }

    /**
     * Determine if the given exception is an HTTP exception.
     *
     * @param  \Throwable  $e
     * @return bool
     */
    protected function isHttpException(Throwable $e)
    {
        return $e instanceof \Phalcon\Http\Request\Exception;
    }
}
