<?php

namespace Decmedia\Kernel\Foundation\Bootstrap;

use Decmedia\Kernel\Contracts\Foundation\Application;
use Decmedia\Kernel\Support\Facades\Facade;
use Decmedia\Kernel\Support\Facades\AliasLoader;

class RegisterFacades
{
    /**
     * Bootstrap the given application.
     *
     * @param \Decmedia\Kernel\Contracts\Foundation\Application $app
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function bootstrap(Application $app)
    {
        Facade::clearResolvedInstances();

        Facade::setFacadeApplication($app);

        /** @var array $aliases */
        $aliases = $app->make('config')->path('app.aliases', []);

        AliasLoader::getInstance($aliases)->register();
    }
}
