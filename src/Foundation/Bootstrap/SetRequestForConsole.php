<?php

namespace Decmedia\Kernel\Foundation\Bootstrap;

use Decmedia\Kernel\Contracts\Foundation\Application;
use Decmedia\Kernel\Http\Request;

class SetRequestForConsole
{
    /**
     * Bootstrap the given application.
     *
     * @param \Decmedia\Kernel\Contracts\Foundation\Application $app
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function bootstrap(Application $app)
    {
        $uri = $app->make('config')->get('app.url', 'http://localhost');

        $components = parse_url($uri);

        $server = $_SERVER;

        if (isset($components['path'])) {
            $server = array_merge($server, [
                'SCRIPT_FILENAME' => $components['path'],
                'SCRIPT_NAME' => $components['path'],
            ]);
        }

        /*$app->instance('request', Request::create(
            $uri,
            'GET',
            [],
            [],
            [],
            $server
        ));*/
    }
}
