<?php

namespace Decmedia\Kernel\Foundation\Bootstrap;

use Decmedia\Kernel\Contracts\Foundation\Application;

class RegisterProviders
{
    /**
     * Bootstrap the given application.
     *
     * @param  \Decmedia\Kernel\Contracts\Foundation\Application  $app
     * @return void
     */
    public function bootstrap(Application $app)
    {
        $app->registerConfiguredProviders();
    }
}
