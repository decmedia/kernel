<?php

namespace Decmedia\Kernel\Foundation\Events;

trait Dispatchable
{
    /**
     * Dispatch the event with the given arguments.
     *
     * @return array
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function dispatch()
    {
        return \Decmedia\Kernel\event(new static(...func_get_args()));
    }

    /**
     * Dispatch the event with the given arguments if the given truth test passes.
     *
     * @param bool $boolean
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function dispatchIf($boolean, ...$arguments)
    {
        if ($boolean) {
            return \Decmedia\Kernel\event(new static(...$arguments));
        }
    }

    /**
     * Dispatch the event with the given arguments unless the given truth test passes.
     *
     * @param bool $boolean
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function dispatchUnless($boolean, ...$arguments)
    {
        if (! $boolean) {
            return \Decmedia\Kernel\event(new static(...$arguments));
        }
    }

    /**
     * Broadcast the event with the given arguments.
     *
     * @return \Illuminate\Broadcasting\PendingBroadcast
     */
    public static function broadcast()
    {
        return \Decmedia\Kernel\event(new static(...func_get_args()));
    }
}
