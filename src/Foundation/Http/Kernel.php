<?php

namespace Decmedia\Kernel\Foundation\Http;

use Phalcon\Cli\Console;
use Phalcon\Di\FactoryDefault;
use Phalcon\Di\FactoryDefault\Cli as PhCli;
use Phalcon\Mvc\Micro;
use Decmedia\Kernel\Contracts\Foundation\Application;
use Decmedia\Kernel\Contracts\Http\Kernel as HttpKernelContract;

class Kernel implements HttpKernelContract
{
    /**
     * The application's global providers stack.
     * @var string[]
     */
    protected $infrastructureProvider;

    /** @var string $eventManager */
    protected $eventManager;

    /** @var Micro|Console */
    protected $infrastructure;

    /** @var FactoryDefault|PhCli */
    protected $di;

    /**
     * The bootstrap classes for the application.
     *
     * @var array
     */
    protected $bootstrapper = [
        \Decmedia\Kernel\Foundation\Bootstrap\HandleExceptions::class,
        \Decmedia\Kernel\Foundation\Bootstrap\RegisterFacades::class,
        \Decmedia\Kernel\Foundation\Bootstrap\RegisterProviders::class,
        \Decmedia\Kernel\Foundation\Bootstrap\BootProviders::class,
    ];

    /**
     * @var \Decmedia\Kernel\Contracts\Foundation\Application $application
     */
    protected $application;

    public function __construct(Application $application)
    {
        $this->application = $application;

        $this->di = new FactoryDefault();
        $this->di->setShared('dispatcher', new \Phalcon\Mvc\Dispatcher());

        $this->infrastructure = new Micro($this->di);
    }

    /**
     * @param string $request
     * @return mixed
     */
    public function run(string $request)
    {
        return $this->infrastructure->handle($request);
    }

    /**
     * Bootstrap the application for HTTP requests.
     *
     * @return void
     */
    public function bootstrap()
    {
        $this->di->setShared('infrastructure', $this->infrastructure);
        $this->di->setShared('application', $this->application);

        $this->application->instance('infrastructure', $this->infrastructure);

        if (! $this->application->hasBeenBootstrapped()) {
            $this->application->bootstrapWith([
                \Decmedia\Kernel\Foundation\Bootstrap\LoadEnvironmentVariables::class
            ]);
            $this->registerInfrastructureProviders();

            $this->application->bootstrapWith($this->bootstrapper());
        }
    }

    /**
     * @return mixed
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Get the bootstrap classes for the application.
     *
     * @return array
     */
    protected function bootstrapper()
    {
        return $this->bootstrapper;
    }

    /**
     * Registers available providers
     *
     * @return void
     */
    private function registerInfrastructureProviders()
    {
        foreach ($this->infrastructureProvider as $provider) {
            (new $provider())->register($this->infrastructure->getDI());
        }
    }
}
