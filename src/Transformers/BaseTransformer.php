<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Transformers;

use Decmedia\Kernel\Model\AbstractModel;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;
use Decmedia\Kernel\Exception\ModelException;
use function array_intersect;
use function array_keys;

/**
 * Class BaseTransformer
 */
class BaseTransformer extends TransformerAbstract
{
    /** @var array */
    private $fields = [];

    /** @var string */
    private $resource = '';

    /**
     * BaseTransformer constructor.
     *
     * @param array  $fields
     * @param string $resource
     */
    public function __construct(array $fields = [], string $resource = '')
    {
        $this->fields   = $fields;
        $this->resource = $resource;
    }

    /**
     * @param AbstractModel $model
     *
     * @return array
     * @throws ModelException
     */
    public function transform($model): array
    {
        $modelFields     = array_keys($model->getModelFilters());
        $requestedFields = $this->getArrayDepth($this->fields) == 0
            ? $this->fields
            : $this->fields[$this->resource] ?? $modelFields;
        $fields          = array_intersect($modelFields, $requestedFields);
        $data            = [];
        foreach ($fields as $field) {
            $data[$field] = $model->get($field);
        }

        return $data;
    }

    /**
     * @param string        $method
     * @param AbstractModel $model
     * @param string        $transformer
     * @param string        $resource
     *
     * @return Collection|Item
     */
    protected function getRelatedData(string $method, $model, string $transformer, string $resource)
    {
        /** @var AbstractModel $data */
        $data = $model->getRelated($resource);

        return $this->$method($data, new $transformer($this->fields, $resource), $resource);
    }

    /**
     * @param $array
     * @return int
     */
    protected function getArrayDepth($array): int
    {
        $depth = 0;
        $iteIte = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($array));

        foreach ($iteIte as $ite) {
            $d = $iteIte->getDepth();
            $depth = $d > $depth ? $d : $depth;
        }

        return $depth;
    }

    /**
     * @param mixed $data
     * @return array
     */
    protected function transformWithFieldFilter($data): array
    {
        if (is_null($this->fields) || (is_array($this->fields) && count($this->fields) < 1)) {
            return $data;
        }

        return array_intersect_key($data, array_flip((array)$this->fields));
    }
}
