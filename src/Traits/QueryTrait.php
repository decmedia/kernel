<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Traits;

use Lcobucci\JWT\Token;
use Decmedia\Kernel\Contracts\Cache\Store;
use Decmedia\Kernel\Contracts\Config\Repository;
use Decmedia\Kernel\Support\Constants\Flags;
use Decmedia\Kernel\Support\Constants\JWTClaims;
use Decmedia\Kernel\Models\User;
use Phalcon\Mvc\Model\Query\Builder;
use Phalcon\Mvc\Model\ResultsetInterface;
use function json_encode;
use function sha1;

/**
 * Trait QueryTrait
 */
trait QueryTrait
{
    /**
     * Runs a query using the builder
     *
     * @param \Decmedia\Kernel\Contracts\Config\Repository $config
     * @param Store $cache
     * @param string $class
     * @param array $where
     * @param string $orderBy
     *
     * @return ResultsetInterface
     */
    protected function getRecords(
        Repository $config,
        Store $cache,
        string $class,
        array $where = [],
        string $orderBy = ''
    ): ResultsetInterface {
        $builder = new Builder();
        $builder->addFrom($class, 't1');

        foreach ($where as $field => $value) {
            $builder->andWhere(
                sprintf('%s = :%s:', $field, $field),
                [$field => $value]
            );
        }

        if (true !== empty($orderBy)) {
            $builder->orderBy($orderBy);
        }

        return $this->getResults($config, $cache, $builder, $where);
    }

    /**
     * Runs the builder query if there is no cached data
     *
     * @param \Decmedia\Kernel\Contracts\Config\Repository $config
     * @param Store $cache
     * @param Builder $builder
     * @param array $where
     *
     * @return ResultsetInterface
     */
    protected function getResults(
        Repository $config,
        Store $cache,
        Builder $builder,
        array $where = []
    ): ResultsetInterface {
        /**
         * Calculate the cache key
         */
        $phql     = $builder->getPhql();
        $params   = json_encode($where);
        $cacheKey = sha1(sprintf('%s-%s.cache', $phql, $params));
        if ($config->get('app.env') !== 'testing'
            && true !== $config->get('app.devMode')
            && true === $cache->has($cacheKey)
        ) {
            /** @var ResultsetInterface $data */
            $data = $cache->get($cacheKey);
        } else {
            $data = $builder->getQuery()->execute();
            $cache->set($cacheKey, $data);
        }

        return $data;
    }
}
