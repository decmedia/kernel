<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Traits;

use Closure;
use Decmedia\Kernel\Support\Facades\Config;

trait Localizable
{
    /**
     * Run the callback with the given locale.
     *
     * @param string $locale
     * @param Closure $callback
     * @return mixed
     */
    public function withLocale($locale, Closure $callback)
    {
        if (! $locale) {
            return $callback();
        }

        $original = Config::path('app.locale');
        print_r($original);
        die;

        try {
            Config::set('app.locale', $locale);

            return $callback();
        } finally {
            Config::set('app.locale', $original);
        }
    }
}
