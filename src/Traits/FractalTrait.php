<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Traits;

use League\Fractal\Manager;
use League\Fractal\Serializer\JsonApiSerializer;

use function sprintf;
use function ucfirst;

/**
 * Trait FractalTrait
 */
trait FractalTrait
{
    /**
     * Format results based on a transformer
     *
     * @param string  $method
     * @param mixed   $results
     * @param string  $transformer
     * @param string  $resource
     * @param array   $relationships
     * @param array   $fields
     *
     * @return array
     */
    protected function format(
        string $method,
        $results,
        string $transformer,
        string $resource,
        array $relationships = [],
        array $fields = []
    ): array {
        $url      = \Decmedia\Kernel\env('APP_BASE_URI', 'http://localhost');
        $manager  = new Manager();
        $manager->setSerializer(new JsonApiSerializer($url));

        /**
         * Process relationships
         */
        if (count($relationships) > 0) {
            $manager->parseIncludes($relationships);
        }

        $class    = sprintf('League\Fractal\Resource\%s', ucfirst($method));
        $resource = new $class($results, new $transformer($fields, $resource), $resource);
        $results  = $manager->createData($resource)->toArray();

        return $results;
    }
}
