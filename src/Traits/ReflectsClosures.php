<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Traits;

use Closure;
use Illuminate\Support\Reflector;
use ReflectionException;
use ReflectionFunction;
use Decmedia\Kernel\Exception\RuntimeException;

trait ReflectsClosures
{
    /**
     * Get the class names / types of the parameters of the given Closure.
     *
     * @param Closure $closure
     * @return array
     *
     * @throws ReflectionException
     */
    protected function closureParameterTypes(Closure $closure)
    {
        $reflection = new ReflectionFunction($closure);

        return collect($reflection->getParameters())->mapWithKeys(function ($parameter) {
            if ($parameter->isVariadic()) {
                return [$parameter->getName() => null];
            }

            return [$parameter->getName() => Reflector::getParameterClassName($parameter)];
        })->all();
    }

    /**
     * Get the class name of the first parameter of the given Closure.
     *
     * @param Closure $closure
     * @return string
     *
     * @throws RuntimeException|ReflectionException
     */
    protected function firstClosureParameterType(Closure $closure)
    {
        $types = array_values($this->closureParameterTypes($closure));

        if (! $types) {
            throw new RuntimeException('The given Closure has no parameters.');
        }

        if ($types[0] === null) {
            throw new RuntimeException('The first parameter of the given Closure is missing a type hint.');
        }

        return $types[0];
    }
}
