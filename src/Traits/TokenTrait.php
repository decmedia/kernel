<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Traits;

use Decmedia\Kernel\Exception\RuntimeException;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Token;

use function time;
use function Decmedia\Kernel\app;

/**
 * Trait TokenTrait
 */
trait TokenTrait
{
    use InteractsWithTime;

    /**
     * Returns the JWT token object
     *
     * @param string $token
     *
     * @return Token
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function getToken(string $token): Token
    {
        return $this->getConfiguration()->parser()->parse($token);
    }

    /**
     * @return Configuration
     * @throws \Illuminate\Contracts\Container\BindingResolutionException|\Decmedia\Kernel\Exception\RuntimeException
     */
    protected function getConfiguration()
    {
        $key = app('config')->get('app.key');
        if (!$key) {
            throw new RuntimeException('The application key is not installed. Run: php cli key:generate');
        }

        return Configuration::forSymmetricSigner(
            new Sha256(),
            InMemory::plainText($key)
        );
    }

    /**
     * Returns the default audience for the tokens
     *
     * @param string $uri
     * @return string
     */
    protected function getTokenAudience(string $uri): string
    {
        return $uri;
    }

    /**
     * Returns the time the token is issued at
     *
     * @return int
     */
    protected function getTokenTimeIssuedAt(): int
    {
        return time();
    }

    /**
     * Returns the time drift i.e. token will be valid not before
     *
     * @param \DateTimeInterface|\DateInterval|int $second
     * @return int
     */
    protected function getTokenTimeNotBefore($delay): int
    {
        return (time() + $this->secondsUntil($delay));
    }

    /**
     * Returns the expiry time for the token
     *
     * @param \DateTimeInterface|\DateInterval|int $second
     * @return int
     */
    protected function getTokenTimeExpiration($delay): int
    {
        return (time() + $this->secondsUntil($delay));
    }
}
