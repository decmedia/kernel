<?php
declare(strict_types=1);

namespace Decmedia\Kernel\Traits;

use League\Fractal\Manager;
use League\Fractal\Serializer\ArraySerializer;

use function sprintf;
use function ucfirst;

/**
 * Trait FractalArrayTrait
 * @package Decmedia\Kernel\Traits
 */
trait FractalArrayTrait
{
    /**
     * Format results based on a transformer
     *
     * @param string  $method
     * @param mixed   $results
     * @param string  $transformer
     * @param string  $resource
     * @param array   $relationships
     * @param array   $fields
     *
     * @return array
     */
    protected function format(
        string $method,
        $results,
        string $transformer,
        string $resource,
        array $relationships = [],
        array $fields = []
    ): array {
        $manager = new Manager();
        $manager->setSerializer(new ArraySerializer());

        /**
         * Process relationships
         */
        if (count($relationships) > 0) {
            $manager->parseIncludes($relationships);
        }

        $class    = sprintf('League\Fractal\Resource\%s', ucfirst($method));
        $resource = new $class($results, new $transformer($fields, $resource), $resource);
        $results  = $manager->createData($resource)->toArray();

        return $results;
    }
}
