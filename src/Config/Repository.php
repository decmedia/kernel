<?php

namespace Decmedia\Kernel\Config;

use Illuminate\Support\Arr;
use Phalcon\Config;
use Decmedia\Kernel\Contracts\Config\Repository as RepositoryContract;

class Repository extends Config implements RepositoryContract
{
    /**
     * Set an element in the collection
     *
     * @param string $element
     * @param mixed $value
     * @return void
     */
    public function set(string $element, $value = null): void
    {
        parent::set($element, $value);
    }

    /**
     * Determine if the given configuration value exists.
     *
     * @param  string  $key
     * @return bool
     */
    public function has($key): bool
    {
        return Arr::has($this->getItems(), $key);
    }

    /**
     * Get the specified configuration value.
     *
     * @param array|string $key
     * @param mixed $default
     * @param string|null $cast
     * @return mixed
     */
    public function get($key, $default = null, string $cast = null)
    {
        if (is_array($key)) {
            return $this->getMany($key);
        }

        return Arr::get($this->getItems(), $key, $default);
    }

    /**
     * Get many configuration values.
     *
     * @param  array  $keys
     * @return array
     */
    public function getMany($keys)
    {
        $config = [];

        foreach ($keys as $key => $default) {
            if (is_numeric($key)) {
                [$key, $default] = [$default, null];
            }

            $config[$key] = Arr::get($this->getItems(), $key, $default);
        }

        return $config;
    }

    /**
     * Get all of the configuration items for the application.
     *
     * @return array
     */
    public function all()
    {
        return $this->getItems();
    }

    private function getItems()
    {
        return $this->toArray();
    }

    /**
     * Prepend a value onto an array configuration value.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function prepend($key, $value)
    {
        $array = $this->get($key);

        array_unshift($array, $value);

        $this->set($key, $array);
    }

    /**
     * Push a value onto an array configuration value.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function push($key, $value)
    {
        $array = $this->get($key);

        $array[] = $value;

        $this->set($key, $array);
    }
}
