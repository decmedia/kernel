<?php

namespace Decmedia\Kernel\Log;

use Decmedia\Kernel\Support\ServiceProvider;

class LogServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('logger', function ($app) {
            return new LogManager($app);
        });
    }
}
