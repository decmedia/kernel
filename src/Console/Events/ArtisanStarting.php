<?php

namespace Decmedia\Kernel\Console\Events;

class ArtisanStarting
{
    /**
     * The Artisan application instance.
     *
     * @var \Decmedia\Kernel\Console\Application
     */
    public $artisan;

    /**
     * Create a new event instance.
     *
     * @param  \Decmedia\Kernel\Console\Application  $artisan
     * @return void
     */
    public function __construct($artisan)
    {
        $this->artisan = $artisan;
    }
}
