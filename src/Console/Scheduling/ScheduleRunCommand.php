<?php

namespace Decmedia\Kernel\Console\Scheduling;

use Decmedia\Kernel\Console\Command;
use Decmedia\Kernel\Console\Events\ScheduledTaskFailed;
use Decmedia\Kernel\Console\Events\ScheduledTaskFinished;
use Decmedia\Kernel\Console\Events\ScheduledTaskSkipped;
use Decmedia\Kernel\Console\Events\ScheduledTaskStarting;
use Decmedia\Kernel\Contracts\Debug\ExceptionHandler;
use Decmedia\Kernel\Contracts\Event\Dispatcher;
use Decmedia\Kernel\Support\Facades\Date;
use Throwable;

class ScheduleRunCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'schedule:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the scheduled commands';

    /**
     * The schedule instance.
     *
     * @var \Decmedia\Kernel\Console\Scheduling\Schedule
     */
    protected $schedule;

    /**
     * The 24 hour timestamp this scheduler command started running.
     *
     * @var \Decmedia\Kernel\Support\Carbon
     */
    protected $startedAt;

    /**
     * Check if any events ran.
     *
     * @var bool
     */
    protected $eventsRan = false;

    /**
     * The event dispatcher.
     *
     * @var \Decmedia\Kernel\Contracts\Event\Dispatcher
     */
    protected $dispatcher;

    /**
     * The exception handler.
     *
     * @var \Decmedia\Kernel\Contracts\Debug\ExceptionHandler
     */
    protected $handler;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->startedAt = Date::now();

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param  \Decmedia\Kernel\Console\Scheduling\Schedule  $schedule
     * @param  \Decmedia\Kernel\Contracts\Event\Dispatcher  $dispatcher
     * @param  \Decmedia\Kernel\Contracts\Debug\ExceptionHandler  $handler
     * @return void
     */
    public function handle(Schedule $schedule, Dispatcher $dispatcher, ExceptionHandler $handler)
    {
        $this->schedule = $schedule;
        $this->dispatcher = $dispatcher;
        $this->handler = $handler;

        foreach ($this->schedule->dueEvents($this->container) as $event) {
            if (! $event->filtersPass($this->container)) {
                $this->dispatcher->dispatch(new ScheduledTaskSkipped($event));

                continue;
            }

            if ($event->onOneServer) {
                $this->runSingleServerEvent($event);
            } else {
                $this->runEvent($event);
            }

            $this->eventsRan = true;
        }

        if (! $this->eventsRan) {
            $this->info('No scheduled commands are ready to run.');
        }
    }

    /**
     * Run the given single server event.
     *
     * @param \Decmedia\Kernel\Console\Scheduling\Event $event
     * @return void
     * @throws Throwable
     */
    protected function runSingleServerEvent($event)
    {
        if ($this->schedule->serverShouldRun($event, $this->startedAt)) {
            $this->runEvent($event);
        } else {
            $this->line('<info>Skipping command (has already run on another server):</info> '.$event->getSummaryForDisplay());
        }
    }

    /**
     * Run the given event.
     *
     * @param \Decmedia\Kernel\Console\Scheduling\Event $event
     * @return void
     * @throws Throwable
     */
    protected function runEvent($event)
    {
        $this->line('<info>Running scheduled command:</info> '.$event->getSummaryForDisplay());

        $this->dispatcher->dispatch(new ScheduledTaskStarting($event));

        $start = microtime(true);

        try {
            $event->run($this->container);

            $this->dispatcher->dispatch(new ScheduledTaskFinished(
                $event,
                round(microtime(true) - $start, 2)
            ));

            $this->eventsRan = true;
        } catch (Throwable $e) {
            $this->dispatcher->dispatch(new ScheduledTaskFailed($event, $e));

            $this->handler->report($e);
        }
    }
}
