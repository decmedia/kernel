<?php

namespace Decmedia\Kernel\Queue;

use Phalcon\Mvc\Model\Query\Builder;
use Decmedia\Kernel\Contracts\Queue\Queue as QueueContract;
use Decmedia\Kernel\Exception\ModelException;
use Decmedia\Kernel\Providers\Database\DatabaseAdapter as Connection;
use Decmedia\Kernel\Queue\Jobs\DatabaseJob;
use Decmedia\Kernel\Queue\Jobs\DatabaseJobRecord;
use Decmedia\Kernel\Support\Carbon;
use PDO;

class DatabaseQueue extends Queue implements QueueContract
{
    /**
     * The database connection instance.
     *
     * @var \Decmedia\Kernel\Providers\Database\DatabaseAdapter
     */
    protected $connection;

    /**
     * The database table that holds the jobs.
     *
     * @var string
     */
    protected $table;

    /**
     * The name of the default queue.
     *
     * @var string
     */
    protected $default;

    /**
     * The expiration time of a job.
     *
     * @var int|null
     */
    protected $retryAfter = 60;

    /**
     * Create a new database queue instance.
     *
     * @param   \Phalcon\Db\Adapter\AbstractAdapter $connection
     * @param  string  $table
     * @param  string  $default
     * @param  int  $retryAfter
     * @return void
     */
    public function __construct(Connection $connection, $table, $default = 'default', $retryAfter = 60)
    {
        $this->table = $table;
        $this->default = $default;
        $this->connection = $connection;
        $this->retryAfter = $retryAfter;
    }

    /**
     * Get the size of the queue.
     *
     * @param  string|null  $queue
     * @return int
     */
    public function size($queue = null)
    {
        return count(
            $this
                ->getBuilder()
                ->where('queue = :queue:', ['queue' => $this->getQueue($queue)])
                ->getQuery()
                ->execute()
        );
    }

    /**
     * Push a new job onto the queue.
     *
     * @param  string  $job
     * @param  mixed  $data
     * @param  string|null  $queue
     * @return mixed
     */
    public function push($job, $data = '', $queue = null)
    {
        return $this->pushToDatabase($queue, $this->createPayload(
            $job,
            $this->getQueue($queue),
            $data
        ));
    }

    /**
     * Push a raw payload onto the queue.
     *
     * @param  string  $payload
     * @param  string|null  $queue
     * @param  array  $options
     * @return mixed
     */
    public function pushRaw($payload, $queue = null, array $options = [])
    {
        return $this->pushToDatabase($queue, $payload);
    }

    /**
     * Push a new job onto the queue after a delay.
     *
     * @param  \DateTimeInterface|\DateInterval|int  $delay
     * @param  string  $job
     * @param  mixed  $data
     * @param  string|null  $queue
     * @return void
     */
    public function later($delay, $job, $data = '', $queue = null)
    {
        return $this->pushToDatabase($queue, $this->createPayload(
            $job,
            $this->getQueue($queue),
            $data
        ), $delay);
    }

    /**
     * Push an array of jobs onto the queue.
     *
     * @param  array  $jobs
     * @param  mixed  $data
     * @param  string|null  $queue
     * @return mixed
     */
    public function bulk($jobs, $data = '', $queue = null)
    {
        $queue = $this->getQueue($queue);

        $availableAt = $this->availableAt();

        return $this->connection->table($this->table)->insert(collect((array) $jobs)->map(
            function ($job) use ($queue, $data, $availableAt) {
                return $this->buildDatabaseRecord($queue, $this->createPayload($job, $this->getQueue($queue), $data), $availableAt);
            }
        )->all());
    }

    /**
     * Release a reserved job back onto the queue.
     *
     * @param  string  $queue
     * @param  \Decmedia\Kernel\Queue\Jobs\DatabaseJobRecord  $job
     * @param  int  $delay
     * @return mixed
     */
    public function release($queue, $job, $delay)
    {
        return $this->pushToDatabase($queue, $job->payload, $delay, $job->attempts);
    }

    /**
     * Push a raw payload to the database with a given delay.
     *
     * @param string|null $queue
     * @param string $payload
     * @param \DateTimeInterface|\DateInterval|int $delay
     * @param int $attempts
     * @return mixed
     * @throws \Decmedia\Kernel\Exception\ModelException
     */
    protected function pushToDatabase($queue, $payload, $delay = 0, $attempts = 0)
    {
        /** @var \Decmedia\Kernel\Models\AbstractModel $logTable $logTable */
        $logTable = new $this->table();

        $record = $this->buildDatabaseRecord(
            $this->getQueue($queue),
            $payload,
            $this->availableAt($delay),
            $attempts
        );
        foreach ($record as $field => $value) {
            $logTable->set($field, $value);
        }

        if (false === $logTable->save()) {
            throw new ModelException($logTable->getModelMessages());
        }

        return $logTable->get('id');
    }

    /**
     * Create an array to insert for the given job.
     *
     * @param  string|null  $queue
     * @param  string  $payload
     * @param  int  $availableAt
     * @param  int  $attempts
     * @return array
     */
    protected function buildDatabaseRecord($queue, $payload, $availableAt, $attempts = 0)
    {
        return [
            'queue' => $queue,
            'attempts' => $attempts,
            'reserved_at' => null,
            'available_at' => $availableAt,
            'created_at' => $this->currentTime(),
            'payload' => $payload,
        ];
    }

    /**
     * Pop the next job off of the queue.
     *
     * @param  string|null  $queue
     * @return \Decmedia\Kernel\Contracts\Queue\Job|null
     *
     * @throws \Throwable
     */
    public function pop($queue = null)
    {
        $queue = $this->getQueue($queue);

        return $this->connection->transaction(function () use ($queue) {
            if ($job = $this->getNextAvailableJob($queue)) {
                return $this->marshalJob($queue, $job);
            }
        });
    }

    /**
     * Get the next available job for the queue.
     *
     * @param  string|null  $queue
     * @return \Decmedia\Kernel\Queue\Jobs\DatabaseJobRecord|null
     */
    protected function getNextAvailableJob($queue)
    {
        $expiration = Carbon::now()->subSeconds($this->retryAfter)->getTimestamp();
        $job = $this
            ->getBuilder()
            ->where(
                'queue = :queue: AND reserved_at IS NULL AND available_at <= :available_at: OR reserved_at <= :reserved_at:',
                [
                    'queue' => $this->getQueue($queue),
                    'available_at' => $this->currentTime(),
                    'reserved_at' => $expiration
                ]
            )
            ->orderBy('id ASC')
            ->getQuery()
            ->getSingleResult()
        ;

        return $job ? new DatabaseJobRecord((object) $job) : null;
    }

    /**
     * Get the lock required for popping the next job.
     *
     * @return string|bool
     */
    protected function getLockForPopping()
    {
        $databaseEngine = $this->connection->getPdo()->getAttribute(PDO::ATTR_DRIVER_NAME);
        $databaseVersion = $this->connection->getPdo()->getAttribute(PDO::ATTR_SERVER_VERSION);

        if ($databaseEngine == 'mysql' && ! strpos($databaseVersion, 'MariaDB') && version_compare($databaseVersion, '8.0.1', '>=') ||
            $databaseEngine == 'pgsql' && version_compare($databaseVersion, '9.5', '>=')) {
            return 'FOR UPDATE SKIP LOCKED';
        }

        return true;
    }

    /**
     * Modify the query to check for available jobs.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @return void
     */
    /*protected function isAvailable($query)
    {
        $query->where(function ($query) {
            $query->whereNull('reserved_at')
                  ->where('available_at', '<=', $this->currentTime());
        });
    }*/

    /**
     * Modify the query to check for jobs that are reserved but have expired.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @return void
     */
   /* protected function isReservedButExpired($query)
    {
        $expiration = Carbon::now()->subSeconds($this->retryAfter)->getTimestamp();

        $query->orWhere(function ($query) use ($expiration) {
            $query->where('reserved_at', '<=', $expiration);
        });
    }*/

    /**
     * Marshal the reserved job into a DatabaseJob instance.
     *
     * @param  string  $queue
     * @param  \Decmedia\Kernel\Queue\Jobs\DatabaseJobRecord  $job
     * @return \Decmedia\Kernel\Queue\Jobs\DatabaseJob
     */
    protected function marshalJob($queue, $job)
    {
        $job = $this->markJobAsReserved($job);

        return new DatabaseJob(
            $this->container,
            $this,
            $job,
            $this->connectionName,
            $queue
        );
    }

    /**
     * Mark the given job ID as reserved.
     *
     * @param \Decmedia\Kernel\Queue\Jobs\DatabaseJobRecord $job
     * @return \Decmedia\Kernel\Queue\Jobs\DatabaseJobRecord
     * @throws ModelException
     */
    protected function markJobAsReserved($job)
    {
        $jobRow = $this->getJobById($job->id);
        if ($jobRow) {
            $jobRow
                ->set('reserved_at', $job->touch())
                ->set('attempts', $job->increment())
            ;

            if (false === $jobRow->update()) {
                throw new ModelException($jobRow->getModelMessages());
            }
        }

        return $job;
    }

    /**
     * @param int $jobID
     * @return \Phalcon\Mvc\ModelInterface
     */
    protected function getJobById(int $jobID)
    {
        return $this
            ->getBuilder()
            ->where('id = :id:', ['id' => $jobID])
            ->getQuery()
            ->getSingleResult()
            ;
    }

    /**
     * Delete a reserved job from the queue.
     *
     * @param  string  $queue
     * @param  string  $id
     * @return void
     *
     * @throws \Throwable
     */
    public function deleteReserved($queue, $id)
    {
        $this->connection->transaction(function () use ($id) {
            $jobRow = $this->getJobById($id);
            if ($jobRow) {
                if (false === $jobRow->delete()) {
                    throw new ModelException($jobRow->getModelMessages());
                }
            }
        });
    }

    /**
     * Delete a reserved job from the reserved queue and release it.
     *
     * @param string $queue
     * @param \Decmedia\Kernel\Queue\Jobs\DatabaseJob $job
     * @param int $delay
     * @return void
     * @throws \Throwable
     */
    public function deleteAndRelease($queue, $job, $delay)
    {
        $this->connection->transaction(function () use ($queue, $job, $delay) {
            $jobRow = $this->getJobById($job->getJobId());
            if ($jobRow) {
                if (false === $jobRow->delete()) {
                    throw new ModelException($jobRow->getModelMessages());
                }
            }

            $this->release($queue, $job->getJobRecord(), $delay);
        });
    }

    /**
     * Get the queue or return the default.
     *
     * @param  string|null  $queue
     * @return string
     */
    public function getQueue($queue)
    {
        return $queue ?: $this->default;
    }

    /**
     * Get the underlying database instance.
     *
     * @return \Illuminate\Database\Connection
     */
    /*public function getDatabase()
    {
        return $this->database;
    }*/

    /**
     * @return \Phalcon\Mvc\Model\Query\BuilderInterface
     */
    protected function getBuilder()
    {
        return (new Builder())->from($this->table);
    }
}
