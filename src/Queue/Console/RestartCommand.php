<?php

namespace Decmedia\Kernel\Queue\Console;

use Decmedia\Kernel\Console\Command;
use Decmedia\Kernel\Contracts\Cache\Store;
use Decmedia\Kernel\Traits\InteractsWithTime;

class RestartCommand extends Command
{
    use InteractsWithTime;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'queue:restart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restart queue worker daemons after their current job';

    /**
     * The cache store implementation.
     *
     * @var \Decmedia\Kernel\Contracts\Cache\Store
     */
    protected $cache;

    /**
     * Create a new queue restart command.
     *
     * @param  \Decmedia\Kernel\Contracts\Cache\Store  $cache
     * @return void
     */
    public function __construct(Store $cache)
    {
        parent::__construct();

        $this->cache = $cache;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->cache->forever('illuminate:queue:restart', $this->currentTime());

        $this->info('Broadcasting queue restart signal.');
    }
}
