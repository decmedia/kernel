<?php

namespace Decmedia\Kernel\Queue\Connectors;

use Phalcon\Db\Adapter\AbstractAdapter as ConnectionResolverInterface;
use Decmedia\Kernel\Queue\DatabaseQueue;

class DatabaseConnector implements ConnectorInterface
{
    /**
     * Database connections.
     *
     * @var \Phalcon\Db\Adapter\AbstractAdapter
     */
    protected $connection;

    /**
     * Create a new connector instance.
     *
     * @param  \Phalcon\Db\Adapter\AbstractAdapter $connections
     * @return void
     */
    public function __construct(ConnectionResolverInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Establish a queue connection.
     *
     * @param  array  $config
     * @return \Decmedia\Kernel\Contracts\Queue\Queue
     */
    public function connect(array $config)
    {
        return new DatabaseQueue(
            $this->connection,
            $config['table'],
            $config['queue'],
            $config['retry_after'] ?? 60
        );
    }
}
