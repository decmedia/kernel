<?php

namespace Decmedia\Kernel\Queue\Connectors;

interface ConnectorInterface
{
    /**
     * Establish a queue connection.
     *
     * @param  array  $config
     * @return \Decmedia\Kernel\Contracts\Queue\Queue
     */
    public function connect(array $config);
}
