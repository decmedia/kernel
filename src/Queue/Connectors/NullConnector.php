<?php

namespace Decmedia\Kernel\Queue\Connectors;

use Decmedia\Kernel\Queue\NullQueue;

class NullConnector implements ConnectorInterface
{
    /**
     * Establish a queue connection.
     *
     * @param  array  $config
     * @return \Decmedia\Kernel\Contracts\Queue\Queue
     */
    public function connect(array $config)
    {
        return new NullQueue;
    }
}
