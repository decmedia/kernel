<?php

namespace Decmedia\Kernel\Queue;

use RuntimeException;

class ManuallyFailedException extends RuntimeException
{
    //
}
