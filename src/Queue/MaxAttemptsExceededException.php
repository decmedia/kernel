<?php

namespace Decmedia\Kernel\Queue;

use RuntimeException;

class MaxAttemptsExceededException extends RuntimeException
{
    //
}
