<?php

namespace Decmedia\Kernel\Queue\Failed;
use Flow\JSONPath\JSONPath;
use Phalcon\Db\Adapter\Pdo\AbstractPdo;
use Phalcon\Db\Adapter\AdapterInterface;
use Illuminate\Database\ConnectionResolverInterface;
use Decmedia\Kernel\Exception\ModelException;
use Decmedia\Kernel\Models\AbstractModel;
use Decmedia\Kernel\Support\Facades\Date;
use Phalcon\Mvc\Model\Query\Builder;

class DatabaseFailedJobProvider implements FailedJobProviderInterface
{
    /**
     * The connection resolver implementation.
     *
     * @var \Phalcon\Db\Adapter\AdapterInterface
     */
    protected $resolver;

    /**
     * The database table.
     *
     * @var string
     */
    protected $table;

    /**
     * Create a new database failed job provider.
     *
     * @param  \Phalcon\Db\Adapter\AdapterInterface  $resolver
     * @param  string  $table
     * @return void
     */
    public function __construct(AdapterInterface $resolver, string $table)
    {
        $this->table = $table;
        $this->resolver = $resolver;
    }

    /**
     * Log a failed job into storage.
     *
     * @param string $connection
     * @param string $queue
     * @param string $payload
     * @param \Throwable $exception
     * @return int|null
     * @throws \Decmedia\Kernel\Exception\ModelException
     * @throws \Exception
     */
    public function log($connection, $queue, $payload, $exception)
    {
        $uuid = (new JSONPath(json_decode($payload)))->find('$.uuid')->first();

        $failed_at = Date::now();
        $exception = (string) $exception;

        /** @var \Decmedia\Kernel\Models\AbstractModel $logTable $logTable */
        $logTable = new $this->table();
        $result = $logTable
            ->set('uuid', $uuid)
            ->set('connection', $connection)
            ->set('queue', $queue)
            ->set('payload', $payload)
            ->set('exception', $exception)
            ->set('failed_at', $failed_at)
            ->save()
        ;

        if (false === $result) {
            throw new ModelException($logTable->getModelMessages());
        }

        return $logTable->get('id');
    }

    /**
     * Get a list of all of the failed jobs.
     *
     * @return array<\Phalcon\Mvc\Model>
     */
    public function all()
    {
        return $this
            ->getBuilder()
            ->orderBy('id')
            ->getQuery()
            ->execute()
        ;
    }

    /**
     * Get a single failed job.
     *
     * @param  mixed  $id
     * @return object|null
     */
    public function find($id)
    {
        $result = $this
            ->getBuilder()
            ->where('id = :id: or uuid = :id:', ['id' => $id])
            ->getQuery()
            ->execute()
        ;

        return count($result) > 0 ? $result[0] : null;
    }

    /**
     * Delete a single failed job from storage.
     *
     * @param  mixed  $id
     * @return bool
     */
    public function forget($id)
    {
        return $this->find($id)->delete() > 0;
    }

    /**
     * Flush all of the failed jobs from storage.
     *
     * @return void
     */
    public function flush()
    {
        $this->all()->delete();
    }

    /**
     * @return \Phalcon\Mvc\Model\Query\BuilderInterface
     */
    protected function getBuilder()
    {
        return (new Builder())->from($this->table);
    }
}
