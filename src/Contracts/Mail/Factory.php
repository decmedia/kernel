<?php

namespace Decmedia\Kernel\Contracts\Mail;

interface Factory
{
    /**
     * Get a mailer instance by name.
     *
     * @param  string|null  $name
     * @return \Decmedia\Kernel\Notification\Mail\Mailer
     */
    public function mailer($name = null);
}
