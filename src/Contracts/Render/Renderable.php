<?php

namespace Decmedia\Kernel\Contracts\Render;

/**
 * Interface RendererInterface
 */
interface Renderable
{
    /**
     * Get the evaluated contents of the object.
     *
     * @param string $view
     * @param array $data
     * @return string
     */
    public function render(string $view, array $data = []): string;
}
