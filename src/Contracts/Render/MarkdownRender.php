<?php

namespace Decmedia\Kernel\Contracts\Render;

interface MarkdownRender extends Renderable
{
    /**
     * Parse the given Markdown text into HTML.
     *
     * @param string $text
     * @return mixed
     */
    public function parse(string $text);

    /**
     * Render the Markdown template into text.
     *
     * @param string $view
     * @param array $data
     *
     * @return string
     */
    public function renderText(string $view, array $data = []): string;
}
