<?php

namespace Decmedia\Kernel\Contracts\Support;

interface MessageProvider
{
    /**
     * Get the messages for the instance.
     *
     * @return \Decmedia\Kernel\Contracts\Support\MessageBag
     */
    public function getMessageBag();
}
