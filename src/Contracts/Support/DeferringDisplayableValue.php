<?php

namespace Decmedia\Kernel\Contracts\Support;

interface DeferringDisplayableValue
{
    /**
     * Resolve the displayable value that the class is deferring.
     *
     * @return \Decmedia\Kernel\Contracts\Support\Htmlable|string
     */
    public function resolveDisplayableValue();
}
