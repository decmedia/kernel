<?php

namespace Decmedia\Kernel\Contracts\Support;

interface Responsable
{
    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Decmedia\Kernel\Http\Request  $request
     * @return \Decmedia\Kernel\Http\Response
     */
    public function toResponse($request);
}
