<?php

namespace Decmedia\Kernel\Contracts\Support;

interface InlineRender
{
    /**
     * @param $html
     * @param null $css
     * @return mixed
     */
    public function convert($html, $css = null);
}
