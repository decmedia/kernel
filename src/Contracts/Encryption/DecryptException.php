<?php

namespace Decmedia\Kernel\Contracts\Encryption;

use RuntimeException;

class DecryptException extends RuntimeException
{
    //
}
