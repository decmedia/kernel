<?php

namespace Decmedia\Kernel\Contracts\Encryption;

use RuntimeException;

class EncryptException extends RuntimeException
{
    //
}
