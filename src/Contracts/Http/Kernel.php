<?php

namespace Decmedia\Kernel\Contracts\Http;

interface Kernel
{
    /**
     * Run application for HTTP request
     *
     * @param string $request
     * @return mixed
     */
    public function run(string $request);

    /**
     * Bootstrap the application for HTTP requests.
     *
     * @return void
     */
    public function bootstrap();


    /**
     * Get the application instance.
     *
     * @return \Decmedia\Kernel\Contracts\Foundation\Application
     */
    public function getApplication();
}
