<?php

namespace Decmedia\Kernel\Contracts\Http;

use Phalcon\Http\ResponseInterface;

interface Response
{
    /**
     * Send the response back
     *
     * @return ResponseInterface
     */
    public function send(): ResponseInterface;

    /**
     * Sets the payload code as Error
     *
     * @param string $detail
     * @param int $appStatusCode Код результата выполнения приложения в случае ошибки. Если код предусмотрен массивом
     * $this->codes, то HTTP Status также устанавливается на его значение
     *
     * @return \Decmedia\Kernel\Http\Response
     */
    public function setPayloadError(string $detail = '', int $appStatusCode = 500): \Decmedia\Kernel\Http\Response;


    /**
     * Sets the payload code as Success
     *
     * @param null|string|array $content The content
     *
     * @return \Decmedia\Kernel\Http\Response
     */
    public function setPayloadSuccess($content = []): Response;
}
