<?php

namespace Decmedia\Kernel\Tests\Foundation\Http;

use Decmedia\Kernel\Foundation\Application;
use Decmedia\Kernel\Foundation\Http\Kernel;
use PHPUnit\Framework\TestCase;

class KernelTest extends TestCase
{
    /**
     * @return \Decmedia\Kernel\Contracts\Foundation\Application
     */
    protected function getApplication()
    {
        return new Application;
    }

    // TODO тестировать роутеры
    // https://github.com/laravel/framework/blob/8.x/tests/Foundation/Http/KernelTest.php

    public function testDemo()
    {
        $this->assertEquals(1, 1);
    }
}
